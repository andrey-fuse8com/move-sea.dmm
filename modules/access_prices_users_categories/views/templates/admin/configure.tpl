{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
<form action="{$link->getAdminLink('AdminModules')|escape:'html':'UTF-8'}&configure=access_prices_users_categories&submitSaveData" method="post" class="form-horizontal clearfix" id="form-tabs">
	<input type="hidden" name="configure" value="access_prices_users_categories">
	<input type="hidden" name="submitSaveData" value="1">
	{*<select name="group_name">
		{foreach from=$groups_users key=id_group item=group}
			<option value="{$group.id_group}">{$group.name}</option>
		{/foreach}
	</select>*}


	{foreach from=$groups_users key=id_group item=group}
		<div>
			<ul>
		{$group.name} access:
		{function name="categories" nodes=[] depth=0}
			{strip}
				{if $nodes|count}

						{foreach from=$nodes item=node}

								{if (isset($access_prices[$group.id_group][$node.id]) && $access_prices[$group.id_group][$node.id])}
									<input checked type="checkbox" name="access_allowed[{$group.id_group}][{$node.id}]" value="true">{$node.name}
								{else}
									<input type="checkbox" name="access_allowed[{$group.id_group}][{$node.id}]" value="true">{$node.name}
								{/if}
								<div>
									{categories nodes=$node.children depth=$depth+1}
								</div>

						{/foreach}

				{/if}
			{/strip}
		{/function}
		</div>
		<div class="category-tree">


                    {if (isset($access_prices[{$group.id_group}][{$categories.id}]) && $access_prices[{$group.id_group}][{$categories.id}])}
						<div><input type="checkbox" checked name="access_allowed[{$group.id_group}][{$categories.id}]" value="true">{$categories.name}</div>
                    {else}
						<div><input type="checkbox" name="access_allowed[{$group.id_group}][{$categories.id}]" value="true">{$categories.name}</div>
                    {/if}

				{categories nodes=$categories.children}
			</ul>
		</div>
	{/foreach}
	<button type="submit" class="btn btn-default col-lg-12 col-xs-4" name="submitSaveAccess">{l s="Save" d='Admin.Global'}</button>
</form>
</div>
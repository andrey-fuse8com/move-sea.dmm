

{if {$cookie} eq false}
<div id="alert-cookies">
	<div class="cookie-banner">
		<div class="cookie-banner__container">
					<p>{l s='This website uses cookies to give you the best possible service.' mod='cookies'} <a class="btn btn--white" href="{$test}"><span>{l s='Yes, I accept' mod='cookies'}</span></a></p>
		</div>
	</div>
</div>
{/if}

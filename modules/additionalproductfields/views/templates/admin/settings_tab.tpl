{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    DMTAY <info@dmtay.ru>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript" src="../js/jquery/plugins/jquery.tablednd.js"></script>
{if !empty($message)}
	<div class="{$message[1]|escape:'html':'UTF-8'} alert" id="ERROR">
		<button type="button" class="close" data-dismiss="alert">×</button>
        {$message[0]|escape:'html':'UTF-8'}
	</div>
{/if}
<div class="panel">
<form action="{$link->getAdminLink('AdminModules')|escape:'html':'UTF-8'}&configure=additionalproductfields&updatePositionTabs=true" class="form-horizontal clearfix" id="form-tabs">

	<div class="form-wrapper">
		<table class="table fields" id="tabs">
				<thead>
				<tr class="nodrag nodrop">
					<th class="fixed-width-xs center"><span class="title_box">{l s='ID' mod='additionalproductfields'}</span></th>
					<th><span class="title_box text-left">{l s='Fields' mod='additionalproductfields'}</span></th>
					<th><span class="title_box text-left">{l s='Position' mod='additionalproductfields'}</span></th>
				</tr>
				</thead>
				<tbody>
					{foreach from=$tabs item="tab"}
						<tr id="td_0_{$tab.id_field|escape:'html':'UTF-8'}_{$tab.position|escape:'html':'UTF-8'}">
							<td>{$tab.id_field|escape:'html':'UTF-8'}</td>
							<td>{$tab.title[{$id_language}]|escape:'html':'UTF-8'}</td>
							<td id="td_{$tab.id_field|escape:'html':'UTF-8'}_{$tab.position|escape:'html':'UTF-8'}" class=" dragHandle fixed-width-md center">
								<div class="dragGroup">
									<div class="positions">{$tab.position|escape:'html':'UTF-8'}</div>
								</div>
							</td>
							<td>
								<div class="btn-group-action">
									<div class="btn-group pull-right">
										<a href="{$link->getAdminLink('AdminSettingsTabs')|escape:'html':'UTF-8'}&configure=additionalproductfields&update_tab={$tab.id_field|escape:'html':'UTF-8'}" class="btn btn-default">
											<i class="icon-pencil"></i> {l s='Edit' mod='additionalproductfields'}
										</a>
										<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
											<span class="caret"></span>&nbsp;
										</button>
										<ul class="dropdown-menu">
											<li>
												<a href="{$link->getAdminLink('AdminModules')|escape:'html':'UTF-8'}&configure=additionalproductfields&delete_tab_ID={$tab.id_field|escape:'html':'UTF-8'}"
												   onclick="return confirm('{l s='Do you really want to delete this tab?' mod='additionalproductfields'}');">
													<i class="icon-trash"></i> {l s='Delete' mod='additionalproductfields'}
												</a>
											</li>
										</ul>
									</div>
								</div>
							</td>
						</tr>
					{/foreach}
				</tbody>
		</table>
	</div>
	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminSettingsTabs')|escape:'html':'UTF-8'}&configure=additionalproductfields&addTabs" class="btn btn-default pull-right"><i class="process-icon-new"></i> Новый блок</a>
	</div>
</form>
</div>
{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    DMTAY <info@dmtay.ru>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<h3>Add your own values to custom fields</h3>
<div class="translations tabbable">

		{foreach $language as $lang}
		<div class="translationsFields tab-content">
			<div class="{if $id_lang_current == $lang.id_lang}show active{/if} tab-pane translation-field translation-label-{$lang.iso_code|escape:'html':'UTF-8'}">

                {foreach from=$fields item="field"}
					<div class="customfiellds-admin_item">
                        {if $field.type == "radio"}
							<div class="form-group">
								<label class="form-control-label col-lg-2" for="{$field.name|escape:'html':'UTF-8'}">{$field.title[$current_language]|escape:'html':'UTF-8'}</label>
								<div class="col-lg-9">
									<div class="col-lg-12">
                                        {foreach from=json_decode($field.configure) item="value"}
                                            {$key_value = explode(':',$value)}
											<div class="input-group-radio">
                                                {$key_value[1]|escape:'html':'UTF-8'}: <input {if $field.value[{$lang.id_lang}] == $key_value[0]}checked{/if} type="radio" name="{$field.name|escape:'html':'UTF-8'}[{$lang.id_lang}]" value="{$key_value[0]|escape:'html':'UTF-8'}">
											</div>
                                        {/foreach}
									</div>
								</div>
							</div>
                        {/if}

                        {*if $field.type == "checkbox"}
							<div class="form-group">
								<label class="form-control-label col-lg-2" for="{$field.name|escape:'html':'UTF-8'}">{$field.title[$current_language]|escape:'html':'UTF-8'}</label>
								<div class="col-lg-9">
									<div class="col-lg-12">
                                        {foreach from=json_decode($field.configure) item="value"}
                                            {$key_value = explode(':',$value)}
											<div class="input-group-checkbox">
                                                {$key_value[1]|escape:'html':'UTF-8'}: <input {if in_array($key_value[0], $field.value[{$lang.id_lang}])}checked{/if} type="checkbox" name="{$field.name}[{$lang.id_lang}][]" value="{$key_value[0]|escape:'html':'UTF-8'}">
											</div>
                                        {/foreach}
									</div>
								</div>
							</div>
                        {/if*}

                        {if $field.type == "input"}
							<div class="form-group">
								<label class="form-control-label col-lg-2" for="{$field.name|escape:'html':'UTF-8'}">{$field.title[$current_language]|escape:'html':'UTF-8'}</label>
								<div class="col-lg-9">
									<div class="col-lg-12">
										<div class="input-group">
											<input class="form-control" type="input" name="{$field.name|escape:'html':'UTF-8'}[{$lang.id_lang}]" value="{$field.value[{$lang.id_lang}]|escape:'html':'UTF-8'}">
										</div>
									</div>
								</div>
							</div>
                        {/if}
                        {if $field.type == "checkbox"}
							<div class="form-group">

								<label class="form-control-label col-lg-2" for="{$field.name|escape:'html':'UTF-8'}">{$field.title[$current_language]|escape:'html':'UTF-8'}</label>
								<div class="col-lg-9">
									<div class="col-lg-12">
										<div class="input-group">
											<input class="form-control" type="input" name="{$field.name|escape:'html':'UTF-8'}[{$lang.id_lang}]" value="{$field.value[{$lang.id_lang}]|escape:'html':'UTF-8'}">
										</div>
									</div>
								</div>
							</div>
                        {/if}
                        {if $field.type == "text"}
							<div class="form-group">
								<label class="form-control-label col-lg-2" for="{$field.name|escape:'html':'UTF-8'}">{$field.title[$current_language]|escape:'html':'UTF-8'}</label>
								<div class="col-lg-9">
									<div class="col-lg-12">
										<textarea class="autoload_rte textarea-autosize" rows="1" cols="45" name="{$field.name|escape:'html':'UTF-8'}[{$lang.id_lang}]">{$field.value[{$lang.id_lang}]|escape:'htmlall':'UTF-8'}</textarea>
									</div>
								</div>
							</div>
                        {/if}
                        {*{if $field.type == "images"}*}
							{*<label class="form-control-label col-lg-2"*}
								   {*for="{$field.name|escape:'html':'UTF-8'}">{$field.title[$current_language]|escape:'html':'UTF-8'}</label>*}
							{*<ul class="images" rows="1" cols="45"*}
								{*name="{$field.name|escape:'html':'UTF-8'}[{$lang.id_lang}]">*}
								{*<input type="text" class="{$field.name|escape:'html':'UTF-8'}_images"><br>*}
							{*</ul>*}
							{*<h1>Перетащите картинки сюда</h1>*}
							{*<div id="drop_zone" ondrop="drag_drop(event)" ondragover="return false"></div>*}
							{*<div id="uploaded"></div>*}

                        {*{/if}*}
					</div>
                {/foreach}
			</div>
		</div>
		{/foreach}
</div>



<script>
    $(".{$field.name|escape:'html':'UTF-8'}_images").append("<li class='item'>Тест</li>");
    var i=1;
    $(document).ready(function(){

    });
    function drag_drop(event) {
        event.preventDefault();
        //alert(event.dataTransfer.files[0]);
        alert(event.dataTransfer.files[0].name);
        //alert(event.dataTransfer.files[0].size+" bytes");
        alert(event.dataTransfer.files[0].tmp_name);

        $("#uploaded").append('<div class="image_pars"><label name="img_name">'+event.dataTransfer.files[0].name+'</label><input type="text" name="img_title"><br><a href="#" class="remove" id="'+i+++'">Удалить</a></div>');

        $(".remove").click(function(){
            $('#uploaded .image_pars').remove();
        });
    }



</script>
<style>
	#drop_zone {
		background-color: #EEE;
		border: #999 5px dashed;
		width: 290px;
		height: 200px;
		padding: 8px;
		font-size: 18px;
	}
</style>
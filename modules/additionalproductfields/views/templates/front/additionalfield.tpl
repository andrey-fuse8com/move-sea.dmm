{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    DMTAY <info@dmtay.ru>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

{if $tabs}
    <div class="row" id="customfiellds">
        <div class="col-md-12">
            {foreach $tabs as $tab}
                <ul>
                {foreach $tab.fields as $field}
                    {if $field.value}
                    {if $field.is_show_title}
                        <summary
                                class="customfiellds-item_title">{$field.title[$current_language]|escape:'html':'UTF-8'}</summary>
                    {/if}
                        {if $field.type == "checkbox"}
                            <ul>
                                {foreach $field.value as $value}
                                    <li>{$value|escape:'htmlall':'UTF-8'}</li>
                                {/foreach}
                            </ul>
                        {else}
                            <li style="list-style: square outside">{$field.value|escape:'htmlall':'UTF-8'}</li>
                        {/if}
                    {/if}
                {/foreach}
                </ul>
            {/foreach}

        </div>
    </div>
{/if}


<script>
    var flag,content,i;
    flag='';
    {foreach $tabs as $tab}
     i=1;
    {if $tab.content_type=='tabs'}
    $(document).ready(function(){
        content='';
        $(".tabs .nav-tabs ").append('<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#extra-'+i+'" role="tab" aria-controls="extra-'+i+'" aria-selected="true" aria-expanded="false">{$tab.title[$current_language]|escape:'html':'UTF-8'}</a></li>');

        {foreach $tab.fields as $field}

        {if $field.is_show_title}
        content= content + "<summary class='customfiellds-tabs-title'>{$field.title[$current_language]|escape:'html':'UTF-8'} </summary>";
                {/if}
        content= content + "<div class='customfiellds-item_content'>";
        content= content + '{$field.value nofilter}';
        content= content +"</div>";
        {/foreach}
        $(".tabs .tab-content ").append('<div class="tab-pane fade in '+ flag +'" id="extra-'+i+++'" role="tabpanel">'+content+'</div>');
    });

    {/if}

    {/foreach}
    //
</script>

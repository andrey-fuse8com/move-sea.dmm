<?php
/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    DMTAY <info@dmtay.ru>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class AdminSettingsTabsController extends ModuleAdminController
{
    private $lang_id;
    private $name;
    private $template_dir;

    public function __construct()
    {
        $this->name = 'additionalproductfields';
        $this->tab = 'content_management';
        $this->version = '1.0.0';
        $this->author = 'DMTAY';
        $this->title = 'DMTAY';
        $this->active = 1;
        $this->need_instance = 1;
        $this->module_key = 'e9c187aa5c1135f4a84fe259ab11f74a';
        $this->template_dir = '../../../../modules/'.$this->name.'/views/templates/admin/';
        $this->bootstrap = true;

        parent::__construct();


        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);


        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $this->lang_id = $lang->id;
    }

    public function initContent()
    {
        $this->content .= $this->renderForm(Tools::getValue('update_tab'));

        $this->context->smarty->assign(array(
            'content' => $this->content,
        ));
    }

    public function processSave()
    {
        if (Tools::isSubmit('submitReturn')) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').
                "&configure=additionalproductfields&settings_tab");
        }

        if (Tools::isSubmit('submitSettingsTabs')) {
            $language = Language::getLanguages(true, $this->context->shop->id);
            $items = array(
                "ids_field" => addslashes(json_encode(Tools::getValue('items'))),
            );
            $id_tab = Tools::getValue('update_tab');
            $redirect = false;

            if ($id_tab) {
                $where = "ID = " . Tools::getValue('update_tab');

                if (!Db::getInstance()->update('additionalproductfields_settings_tab', $items, $where)) {
                    return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 52'),
                        'module_error alert-danger');
                }
            } else {
                $redirect = true;
                $apClass = new Additionalproductfields();
                $items['position'] = $apClass->getNextPosition('additionalproductfields_settings_tab');
                if (!Db::getInstance()->insert('additionalproductfields_settings_tab', $items)) {
                    return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 52'),
                        'module_error alert-danger');
                }
                $id_tab = (int)Db::getInstance()->Insert_ID();
            }
            foreach ($language as $lang) {
                $where = "id_field = ".pSQL($id_tab)." AND id_lang_title = "
                    .pSQL($lang['id_lang']).
                    " AND group_name = 'tabs'";

                $data = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ .
                    'additionalproductfields_lang` WHERE ' . $where);
                if (!empty($data)) {
                    $data_lang = array();
                    $data_lang['title'] = Tools::getValue('Title_' . $lang['id_lang']);
                    $data_lang['content_type'] = Tools::getValue('field_type');
                    if (!Db::getInstance()->update('additionalproductfields_lang', $data_lang, $where)) {
                        return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 16'),
                            'module_error alert-danger');
                    }
                } else {
                    $data_lang = array();
                    $data_lang['id_field'] = $id_tab;
                    $data_lang['title'] = Tools::getValue('Title_'.$lang['id_lang']);
                    $data_lang['content_type'] = Tools::getValue('field_type');
                    $data_lang['id_lang_title'] = $lang['id_lang'];
                    $data_lang['group_name'] = 'tabs';

                    if (!Db::getInstance()->insert('additionalproductfields_lang', $data_lang)) {
                        return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 16'),
                            'module_error alert-danger');
                    }
                }
            }
            if ($redirect) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminSettingsTabs').
                    "&configure=additionalproductfields&update_tab=".$id_tab);
            }
            return array($this->l('The tab is update successfully'), 'module_confirmation conf confirm alert-success');
        }
        return parent::processSave();
    }
    public function renderForm($tab_id = null)
    {

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Setting Tab', array(), 'Modules.Additionalproductfields.Admin'),
                    'icon' => 'icon-link'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Title', array(), 'Modules.Additionalproductfields.Admin'),
                        'name' => 'Title',
                        'lang' => true,
                    ),
                    array(
                        'type' => 'select',
                        'required' => true,
                        'options' => array(
                            'query' => array(
                                array('id' => 'footerproduct', 'name' => $this->l('Product footer')),
                                array('id' => 'afterproductthumbs', 'name' => $this->l('After product')),
                                array('id' => 'tabs', 'name' => $this->l('Product tabs')),
                                array('id' => 'bodyproductfields', 'name' => $this->l('Body Product')),
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        ),
                        'name' => 'field_type',
                        'label' => $this->l('Field type'),
                    ),

                    array(
                        'type' => 'link_choice',
                        'label' => '',
                        'name' => 'link',
                        'lang' => true,
                    ),
                ),
                'submit' => array(
                    'name' => 'submitSettingsTabs',
                    'title' => $this->trans('Save', array(), 'Admin.Actions')
                ),
                'buttons' => array(
                    'return' => array(
                        'title' => $this->trans('Back to list', array(), 'Modules.Additionalproductfields.Admin'),
                        'name' => 'submitReturn',
                        'type' => 'submit',
                        'class' => 'btn btn-default pull-right',
                        'icon' => 'process-icon-back'
                    )
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->override_folder = 'settingstabs/';
        $helper->table = $this->table;
        $helper->default_form_language = $this->lang_id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminSettingsTabs').
            '&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.
            $this->name."&update_tab=".Tools::getValue('update_tab');

        $helper->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'choices' => $this->renderChoicesSelect(),
            'selected_links' => $this->makeMenuOption(),
        );
        $id_tab = Tools::getValue('update_tab');
        if (!empty($id_tab)){
            $result =Db::getInstance()->getRow("SELECT content_type FROM "._DB_PREFIX_."additionalproductfields_lang WHERE group_name='tabs' AND id_field=".$id_tab)['content_type'];
            if (!empty($result)){
                $helper->fields_value['field_type'] = $result;
            }else{
                $helper->fields_value['field_type'] = 'footerproduct';
            }
        }else{
            $helper->fields_value['field_type'] = 'footerproduct';
        }


        $language = Language::getLanguages(true, $this->context->shop->id);
        if ($tab_id) {
            $tab = $this->getTab($tab_id);

            foreach ($language as $lang) {
                if (isset($tab['title'][$lang['id_lang']])) {
                    $helper->fields_value['Title'][$lang['id_lang']] = $tab['title'][$lang['id_lang']];
                }
            }
        } else {
            foreach ($language as $lang) {
                $helper->fields_value['Title'][$lang['id_lang']] = '';
            }
        }

        return $helper->generateForm(array($fields_form));
    }

    protected function renderChoicesSelect()
    {
        $apClass = new Additionalproductfields();
        $fields = $apClass->getAllFields();
        $html = "<select multiple=\"multiple\" id=\"availableItems\" style=\"width: 300px; height: 160px;\">
                    <optgroup label=\"".$this->l('Additional fields')."\">";
        foreach ($fields as $field) {
            $html .= "<option value=\"{$field['id_field']}\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".
                "{$field['title'][$this->lang_id]}</option>";
        }

        $html .= "</optgroup></select>";
        return $html;
    }

    protected function makeMenuOption()
    {
        $fields = $this->getTabsFields(Tools::getValue('update_tab'));

        $html = "<select multiple=\"multiple\" name=\"items[]\" id=\"items\" style=\"width: 300px; height: 160px;\">";
        if (!empty($fields)) {
            foreach ($fields as $field) {
                if (!empty($field['id_field'])) {
                    $html .= "<option selected=\"selected\" value=\"{$field['id_field']}\">".
                        "{$field['title'][$this->lang_id]}</option>";
                }
            }
        }
        $html .= "</select>";
        return $html;
    }

    protected function getTabsFields($updateTab)
    {
        $query = new DbQuery();
        $query->select('*');
        $query->from('additionalproductfields_settings_tab', 'apst');
        $query->where('apst.ID = '.(int)$updateTab);

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        $fields = array();
        $apClass = new Additionalproductfields();

        if (!empty($result[0]['ids_field']) && ($result[0]['ids_field']!='false')) {    //Добавил исключение при пустом значении таба
            $ids_field = json_decode($result[0]['ids_field']);
            foreach ($ids_field as $id_field) {
                $fields[] = $apClass->getField($id_field);
            }
        } else {
            return '';
        }
        return $fields;
    }

    protected function getTab($idTab)
    {
        $query = new DbQuery();
        $query->select('*');
        $query->from('additionalproductfields_settings_tab', 'apf_st');
        $query->join('LEFT JOIN `'._DB_PREFIX_.'additionalproductfields_lang` apf_l ON apf_st.`ID` = apf_l.`id_field`  AND apf_l.group_name = \'tabs\'');
        $query->where('apf_st.ID = '.(int)$idTab);
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

        $tab = array();

        foreach ($result as $item) {
            if (isset($tab['title'])) {
                $tab['title'][$item['id_lang_title']] = $item['title'];
            } else {
                $tab = $item;
                unset($tab['title']);
                $tab['title'][$item['id_lang_title']] = $item['title'];
            }
        }

        return $tab;
    }
}

<?php
/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    DMTAY <info@dmtay.ru>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
if (!defined('_PS_VERSION_')) {
    exit;
}

class Additionalproductfields extends Module
{

    public function __construct()
    {
        $this->name = 'additionalproductfields';
        $this->tab = 'content_management';
        $this->version = '1.0.0';
        $this->author = 'DMTAY';
        $this->need_instance = 1;
        $this->module_key = 'e9c187aa5c1135f4a84fe259ab11f74a';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Additional product fields');
        $this->description = $this->l('Additional product fields');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        $tab = new Tab();
        $tab->active = 1;
        $languages = Language::getLanguages(false);
        if (is_array($languages)) {
            foreach ($languages as $language) {
                $tab->name[$language['id_lang']] = 'Settings Tabs';
            }
        }
        $tab->class_name = 'AdminSettingsTabsController';
        $tab->module = $this->name;
        $tab->id_parent = - 1;

        return parent::install() &&
            $tab->add() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionProductSave') &&
            $this->registerHook('actionCartSave') &&
            $this->registerHook('actionBeforeCartUpdateQty') &&
            $this->registerHook('displayFooterProduct') &&
            $this->registerHook('displayAdminProductsExtra') &&
            $this->registerHook('displayProductPriceBlock') &&
            $this->registerHook('displayLeftColumnProduct') &&
            $this->registerHook('displayRightColumnProduct') &&
            $this->registerHook('displayProductTab') &&
            $this->registerHook('displayProductTabContent') &&
            $this->registerHook('displayReassurance') &&
            $this->registerHook('displayProductAdditionalInfo') &&
            $this->registerHook('displayAfterProductThumbs') &&
            $this->registerHook('displayBodyProductFields') &&
            $this->registerHook('displayProductListReviews') &&
            $this->registerHook('displayRightColumn') &&
            $this->registerHook('displayPaymentTop') &&
            $this->registerHook('displayProductExtraContent') &&
            $this->registerHook('displayProductListFunctionalButtons') &&


            $this->registerHook('displayProductButtons');
    }
    public function hookdisplayProductAdditionalInfo($params)
    {

        if (!empty(Tools::getValue('id_product'))){
            // $query->join('LEFT JOIN `'._DB_PREFIX_.'additionalproductfields_lang` apf_l ON apf.`ID` = apf_l.`id_field` AND apf_l.group_name = \'fields\'');
            $q = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'additionalproductfields_value` apfv
        LEFT JOIN ' . _DB_PREFIX_ . 'additionalproductfields apf  ON apfv.`name` = apf.`name`
        WHERE id_product='.$id_product = Tools::getValue('id_product')
                    .' AND id_lang='.$this->context->language->id.' AND type="checkbox"');
            if (!empty($q)) {
                foreach ($q as $fieldkey => $field) {
                    $values = explode(';', $field['value']);
                    $content[$fieldkey]['name'] = $field['name'];
                    foreach ($values as $cbkey => $value) {
                        if (!empty($value)) {

                            //$content[$fieldkey]['cb'][$cbkey]['value'] = (explode('$', $value))[0];
                            //$content[$fieldkey]['cb'][$cbkey]['price'] = (explode('$', $value))[1];
                        }
                    }
                }
                $this->smarty->assign(array());
                $this->context->smarty->assign(
                    array(
                        'themePath' => _PS_THEME_DIR_,
                        'content' => $content,
                        'currency' => $this->context->currency->sign,
                        //'data' => $q,

                    )
                );
                $items = $this->fetch('module:additionalproductfields/views/templates/front/product-parameters.tpl');
                return $items;
            }
        }

        return '';

    }
    public function hookdisplayBodyProductFields($params)
    {
        $id_product = Tools::getValue('id_product');
        $this->smarty->assign(
            $this->getWidgetVariables($id_product)
        );
        $items=$this->fetch('module:additionalproductfields/views/templates/front/additionalfield.tpl');



        return $items;

    }
    public function hookactionBeforeCartUpdateQty($data){
$data['quantity']=55;

    }

    public function hookactionCartSave($params)
    {

        if (!empty(Tools::getValue('id_product'))){
        $check=false;
        $q = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'additionalproductfields_value` apfv
        LEFT JOIN ' . _DB_PREFIX_ . 'additionalproductfields apf  ON apfv.`name` = apf.`name`
        WHERE id_product=' . $id_product = Tools::getValue('id_product')
                . ' AND id_lang=' . $this->context->language->id . ' AND type="checkbox"');
        if (!empty($q)) {
            foreach ($q as $fieldkey => $field) {
                $values = explode(';', $field['value']);
                $content[$fieldkey]['name'] = $field['name'];
                foreach ($values as $cbkey => $value) {
                    if (!empty($value)) {
                        //$content[$fieldkey]['cb'][$cbkey]['value'] = (explode('$', $value))[0];
                        //$content[$fieldkey]['cb'][$cbkey]['price'] = (explode('$', $value))[1];
                        if (!empty($_POST[$content[$fieldkey]['name'] . $cbkey])) {
                            $content[$fieldkey]['cb'][$cbkey]['chosen'] = true;
                            $check = true;
                        } else {
                            $content[$fieldkey]['cb'][$cbkey]['chosen'] = false;
                        }
                    }
                }
            }
        }
        if ($check==true){

        //SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME='id_product_attribute'
        //ищет все таблицы в базе с таким полем

        $product = new Product(Tools::getValue('id_product'));
        $price=$product->price;
        $id_product_attribute = $id_product_base_attribute = $this->getBaseAttribute();
        $id_product=Tools::getValue('id_product');
        $id_product_attribute=$this->addProductAttribute($id_product,$id_product_attribute,$id_product_base_attribute);
            //ИЗМЕНЕНИЕ ПАРАМЕТРОВ ПРИ ДОБАВЛЕНИИ ТОВАРА В КОРЗИНУ

            $additional_price = 0;
            foreach ($content as $item) {
                $attrText='';
                foreach ($item['cb'] as $cb) {

                    if ($cb['chosen'] == true) {
                        $additional_price += $cb['price'];
                        $attrText.=$cb['value'].'(+'.$cb['price'].' '.$this->context->currency->sign.') ';
                    }
                }
                $this->addAttribute($id_product_attribute,$attrText, $item['name']);
            }
            $price=$price+$additional_price;
            $this->addPrice($id_product_attribute,$price);
        }

        }
        return '';
    }
    public function addPrice($id_product_attribute,$price){



        $specific_price = new SpecificPrice();
        $specific_price->id_product = Tools::getValue('id_product'); // choosen product id
        $specific_price->id_product_attribute = $id_product_attribute;
        $specific_price->id_cart = $this->context->cart->id;
        $specific_price->id_shop = $this->context->shop->id;
        $specific_price->id_currency = 0;
        $specific_price->id_country = 0;
        $specific_price->id_group = 0;
        $specific_price->id_customer = 0;
        $specific_price->from_quantity = 1;
        $specific_price->price = $price;       //CHANGE PRICE
        $specific_price->reduction_type = 'amount';
        $specific_price->reduction_tax = 1;
        $specific_price->reduction = 0;
        $specific_price->from = date("Y-m-d").' 00:00:00';
        $specific_price->to = date("Y-m-d").' 23:59:59'; // or set date x days from now
        $specific_price->id_specific_price_rule = 0;
        $specific_price->add();


    }


    public function addAttribute($id_product_attribute,$name, $group_name)
    {
        $atts = new AttributeGroup();
        $atts->position=0;
        $atts->group_type=' ';
        $atts->name[$this->context->language->id]=$group_name;
        $atts->public_name[$this->context->language->id]=$group_name;
        $atts->add();
        $AGid=$atts->id;

        $atts = new Attribute();
        $atts->required=false;
        $atts->id_attribute_group = $AGid;
        $atts->color='';
        $atts->name[$this->context->language->id]= $name;
        $atts->add();
        $Aid=$atts->id;
        $attribute_list[] = array(
            'id_product_attribute' => (int) $id_product_attribute,
            'id_attribute' => (int) $Aid,
        );
        Db::getInstance()->insert('product_attribute_combination', $attribute_list);


    }

    public function uninstall()
    {
        $id_tab = (int)Tab::getIdFromClassName('AdminSettingsTabsController');
        if ($id_tab) {
            $tab = new Tab($id_tab);
            $tab->delete();
        }
        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        $message = "";

        if (((bool)Tools::isSubmit('submitAdditionalproductfieldsModule')) == true) {
            $message = $this->postProcess();
        }

        if (Tools::isSubmit('update_field')) {
            return $this->renderForm(Tools::getValue('update_field'));
        }

        if (Tools::isSubmit('update_tab')) {
            return $this->contentUpdateTab(Tools::getValue('update_field'));
        }

        if (Tools::isSubmit('delete_id_field')) {
            $message = $this->deleteField(Tools::getValue('delete_id_field'));
        }

        if (Tools::isSubmit('delete_tab_ID')) {
            $message = $this->deleteTab(Tools::getValue('delete_tab_ID'));

            return $this->settingsTab($message);
        }

        if (Tools::isSubmit('updatePositions')) {
            $this->ajaxPositions();
        }


        if (Tools::isSubmit('settings_tab')) {
            return $this->settingsTab();
        }

        $this->context->smarty->assign(
            array(
                'link' => $this->context->link,
                'module_dir' => $this->_path,
                'fields' => $this->getAllFields(),
                'message' => $message,
                'id_language' => $this->context->language->id,
            )
        );

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    public function getAllFields()
    {
        $query = new DbQuery();
        $query->select('*');
        $query->from('additionalproductfields', 'apf');
        $query->join('LEFT JOIN `'._DB_PREFIX_.'additionalproductfields_lang` apf_l ON apf.`ID` = apf_l.`id_field` AND apf_l.group_name = \'fields\'');
        $query->orderBy('position');

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

        $fields = array();
        foreach ($result as $item) {
            if (isset($fields[$item['id_field']])) {
                $fields[$item['id_field']]['title'][$item['id_lang_title']] = $item['title'];
            } else {
                $fields[$item['id_field']] = $item;
                unset($fields[$item['id_field']]['title']);
                $fields[$item['id_field']]['title'][$item['id_lang_title']] = $item['title'];
            }
        }

        return $fields;
    }

    public function getField($field_id)
    {
        $query = new DbQuery();
        $query->select('*');
        $query->from('additionalproductfields', 'apf');
        $query->join('LEFT JOIN `'._DB_PREFIX_.'additionalproductfields_lang` apf_l ON apf.`ID` = apf_l.`id_field`  AND apf_l.group_name = \'fields\'');
        $query->where('apf.ID = '.(int)$field_id);

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

        $fields = array();

        foreach ($result as $item) {
            if (isset($fields['title'])) {
                $fields['title'][$item['id_lang_title']] = $item['title'];
            } else {
                $fields = $item;
                unset($fields['title']);
                $fields['title'][$item['id_lang_title']] = $item['title'];
            }
        }

        return $fields;
    }

    private function deleteField($field_id)
    {

        if (!empty($field_id)) {
            $field = $this->getField($field_id);
            if (Db::getInstance()->delete('additionalproductfields_value', 'name = \''.pSQL($field['name']).'\'')) {
                if (Db::getInstance()->delete('additionalproductfields_lang', 'id_field = \'' . (int)$field_id . '\' AND group_name = \'fields\'')) {
                    if (Db::getInstance()->delete('additionalproductfields', 'ID = \'' . (int)$field_id . '\'')) {
                        return array($this->l('Field successfully deleted'), 'module_confirmation conf confirm alert-success');
                    }
                }
            }
        }

        return array($this->l('Cannot delete this field'), 'module_error alert-danger');
    }

    private function deleteTab($tab_id)
    {
        if (!empty($tab_id)) {
            if (Db::getInstance()->delete('additionalproductfields_settings_tab', 'ID = \''.(int)$tab_id.'\'')) {
                if (Db::getInstance()->delete('additionalproductfields_lang', 'id_field = \'' . (int)$tab_id . '\' AND group_name = \'tabs\'')) {
                    return array($this->l('Tab successfully deleted'), 'module_confirmation conf confirm alert-success');
                }
            }
        }

        return array($this->l('Cannot delete this tab'), 'module_error alert-danger');
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm($field_id = null)
    {
        $language = $this->context->controller->getLanguages();

        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'insertTab';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            //'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $language,
            'id_language' => $this->context->language->id,
        );



        if ($field_id) {
            $field = $this->getField($field_id);

            foreach ($language as $lang) {
                if (isset($field['title'][$lang['id_lang']])) {
                    $helper->fields_value['ADDITIONALPRODUCTFIELDS_TITLE_FIELD'][$lang['id_lang']] = $field['title'][$lang['id_lang']];
                }
            }

            $helper->fields_value['ADDITIONALPRODUCTFIELDS_NAME_FIELD'] = $field['name'];
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_ID_FIELD'] = $field['id_field'];
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_TYPE_FIELD'] = $field['type'];
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_CLASS_FIELD'] = $field['class'];
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_IS_SHOW_TITLE_FIELD'] = $field['is_show_title'];
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_CONFIGURE_FIELD'] = implode("\r\n", json_decode($field['configure']));
        } else {
            foreach ($language as $lang) {
                $helper->fields_value['ADDITIONALPRODUCTFIELDS_TITLE_FIELD'][$lang['id_lang']] = '';
            }
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_NAME_FIELD'] = '';
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_ID_FIELD'] = '';
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_TYPE_FIELD'] = 'input';
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_CLASS_FIELD'] = '';
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_IS_SHOW_TITLE_FIELD'] = '1';
            $helper->fields_value['ADDITIONALPRODUCTFIELDS_CONFIGURE_FIELD'] = '';
        }

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'name' => 'ADDITIONALPRODUCTFIELDS_TITLE_FIELD',
                        'label' => $this->l('Title'),
                        'lang' => true,
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'required' => true,
                        'name' => 'ADDITIONALPRODUCTFIELDS_NAME_FIELD',
                        'label' => $this->l('Name'),
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'ADDITIONALPRODUCTFIELDS_ID_FIELD',
                    ),
                    array(
                        'type' => 'select',
                        'required' => true,
                        'options' => array(
                            'query' => array(
                                array('id' => 'input', 'name' => $this->l('Input')),
                                array('id' => 'text', 'name' => $this->l('Editor RTE')),
                                array('id' => 'radio', 'name' => $this->l('Radio Button')),
                                array('id' => 'checkbox', 'name' => $this->l('Checkbox')),
                                array('id' => 'images', 'name' => $this->l('Images')),
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        ),
                        'name' => 'ADDITIONALPRODUCTFIELDS_TYPE_FIELD',
                        'label' => $this->l('Type'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'name' => 'ADDITIONALPRODUCTFIELDS_CLASS_FIELD',
                        'label' => $this->l('Class'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'radio',
                        'desc' => $this->l('Show title'),
                        'name' => 'ADDITIONALPRODUCTFIELDS_IS_SHOW_TITLE_FIELD',
                        'label' => $this->l('Show title'),
                        'values' => array(
                            array(
                                'id' => 'true',
                                'value' => '1',
                                'label' => $this->l('Show')
                            ),
                            array(
                                'id' => 'false',
                                'value' => '0',
                                'label' => $this->l('Hide')
                            ),
                        )
                    ),
                    array(
                        'col' => 3,
                        'id' => 'js-configure',
                        'type' => 'textarea',
                        'desc' => $this->l('Configure'),
                        'name' => 'ADDITIONALPRODUCTFIELDS_CONFIGURE_FIELD',
                        'label' => $this->l('Configure'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'name' => 'submitAdditionalproductfieldsModule',
                ),
            ),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        if (empty(Tools::getValue('ADDITIONALPRODUCTFIELDS_NAME_FIELD'))) {
            return array($this->l('ERROR. Field \'name\' cannot be empty'), 'module_error alert-danger');
        }
        if (empty(Tools::getValue('ADDITIONALPRODUCTFIELDS_TYPE_FIELD'))) {
            return array($this->l('ERROR. Field \'type\' cannot be empty'), 'module_error alert-danger');
        }
        $form_values = array();
        $form_values['ID'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_ID_FIELD');
        $language = Language::getLanguages(true, $this->context->shop->id);
        if ($form_values['ID']) {
            $form_values['name'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_NAME_FIELD');
            $data = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'additionalproductfields` WHERE name=\'' . pSQL($form_values['name']) . '\' AND ID <> '.pSQL($form_values['ID']));

            if (!empty($data)) {
                return array($this->l('Field with this \'name\' already exists'), 'module_error alert-danger');
            }

            $form_values['type'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_TYPE_FIELD');
            $form_values['class'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_CLASS_FIELD');
            $form_values['is_show_title'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_IS_SHOW_TITLE_FIELD');

            $form_values['configure'] = addslashes(json_encode(explode("\r\n", Tools::getValue('ADDITIONALPRODUCTFIELDS_CONFIGURE_FIELD'))));
            $data = array(
                'name'=>$form_values['name'],
                'type'=>$form_values['type'],
                'class'=>$form_values['class'],
                'is_show_title'=>$form_values['is_show_title'],
                'configure'=>$form_values['configure'],
            );

            foreach ($language as $lang) {
                $where = "id_field = ".pSQL($form_values['ID'])." AND id_lang_title = ".pSQL($lang['id_lang'])." AND group_name = 'fields'";

                $data = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'additionalproductfields_lang` WHERE ' . $where);

                if (!empty($data)) {
                    $data_lang = array();
                    $data_lang['title'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_TITLE_FIELD_' . $lang['id_lang']);


                    if (!Db::getInstance()->update('additionalproductfields_lang', $data_lang, $where)) {
                        return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 16'), 'module_error alert-danger');
                    }
                } else {
                    $data_lang = array();
                    $data_lang['id_field'] = $form_values['ID'];
                    $data_lang['title'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_TITLE_FIELD_'.$lang['id_lang']);
                    $data_lang['id_lang_title'] = $lang['id_lang'];

                    if (!Db::getInstance()->insert('additionalproductfields_lang', $data_lang)) {
                        return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 16'), 'module_error alert-danger');
                    }
                }
            }

            $where = "ID = ".$form_values['ID'];


            if (Db::getInstance()->update('additionalproductfields', $data, $where)) {
                return array($this->l('The field is update successfully'), 'module_confirmation conf confirm alert-success');
            }

            return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 12'), 'module_error alert-danger');
        } else {
            $form_values['name'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_NAME_FIELD');

            $data = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'additionalproductfields` WHERE name=\'' . pSQL($form_values['name']) . '\'');

            if (!empty($data)) {
                return array($this->l('Field with this \'name\' already exists'), 'module_error alert-danger');
            }

            $form_values['title'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_TITLE_FIELD');
            $form_values['type'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_TYPE_FIELD');
            $form_values['configure'] = addslashes(json_encode(explode("\r\n", Tools::getValue('ADDITIONALPRODUCTFIELDS_CONFIGURE_FIELD'))));
            $form_values['class'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_CLASS_FIELD');
            $form_values['is_show_title'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_IS_SHOW_TITLE_FIELD');
            $position = $this->getNextPosition('additionalproductfields');

            $data = array(
                'name' => $form_values['name'],
                'type' => $form_values['type'],
                'configure' => $form_values['configure'],
                'position' => $position,
                'class' => $form_values['class'],
                'is_show_title' => $form_values['is_show_title'],
            );

            if (!Db::getInstance()->insert('additionalproductfields', $data)) {
                return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 14'), 'module_error alert-danger');
            }

            $id_field = (int)Db::getInstance()->Insert_ID();

            foreach ($language as $lang) {
                $data_lang = array();
                $data_lang['id_field'] = $id_field;
                $data_lang['title'] = Tools::getValue('ADDITIONALPRODUCTFIELDS_TITLE_FIELD_'.$lang['id_lang']);
                $data_lang['id_lang_title'] = $lang['id_lang'];
                $data_lang['group_name'] = 'fields';

                if (!Db::getInstance()->insert('additionalproductfields_lang', $data_lang)) {
                    return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 16'), 'module_error alert-danger');
                }
            }

            return array($this->l('The field is added successfully'), 'module_confirmation conf confirm alert-success');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addJS($this->_path.'views/js/position.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayAdminProductsExtra($params)
    {

        $language = Language::getLanguages(true, $this->context->shop->id);
        $id_product = $params['id_product'];
        $fields = $this->getAllFields();

        foreach ($fields as $key => $field) {
            foreach ($language as $lang) {
                $fields[$key]['value'][$lang['id_lang']] = $this->loadProductData($id_product, $lang['id_lang'], $field['name'], $field['type']);
            }
        }

        $this->context->smarty->assign(
            array(
                'id_lang_current' => $this->context->language->id,
                'fields' => $fields,
                'language' => $language,
                'current_language' => $this->context->language->id,
            )
        );

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/product_additional.tpl');

        return $output;
    }

    public function hookActionProductSave()
    {
        $fields = $this->getAllFields();
        $id_product = Tools::getValue('id_product');

        print_r('test');
        foreach ($fields as $field) {
            $field_value = Tools::getValue($field['name']);
            if (!empty($field_value)) {
                foreach ($field_value as $id_lang => $value) {
                    if ($field['type'] == 'images') {
                        $value = json_encode($value);
                    }
                    $this->saveProductOverviewData($id_product, $id_lang, $field['name'], $value);
                }
            }
        }
    }

    public function loadProductData($id_product, $id_lang, $name, $type)
    {
        $data = Db::getInstance()->getValue('SELECT `value` FROM `'._DB_PREFIX_.'additionalproductfields_value` WHERE id_product='.(int)$id_product.' and `name`=\''.pSQL($name).'\' and `id_lang`=\''.pSQL($id_lang).'\'');
        if (empty($data)) {
            return "";
        }

//        if ($type == 'checkbox') {
//            $data = json_decode($data);
//        }

        return $data;
    }

    public function saveProductOverviewData($id_product, $id_lang, $name, $value)
    {
        $data = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'additionalproductfields_value` WHERE id_product='.(int)$id_product.' and `name` =\''.pSQL($name).'\' and `id_lang` =\''.pSQL($id_lang).'\'');
        if (empty($data)) {
            $data = array(
                'id_product' => (int)$id_product,
                'name' => pSQL($name),
                'id_lang' => pSQL($id_lang),
                'value' => pSQL($value, true),
            );
            Db::getInstance()->insert('additionalproductfields_value', $data);
        } else {
            $data =array(
                'value' => pSQL($value, true),
            );

            $where = '`id_product`='.(int)$id_product.' and `id_lang`='.(int)$id_lang.' and `name` =\''.pSQL($name).'\'';

            Db::getInstance()->update('additionalproductfields_value', $data, $where);
        }
    }

    protected function getProducts()
    {

        $customer = $this->context->customer->id;

        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'product` WHERE  id_product = id_product';

        $products = Db::getInstance()->executeS($sql);

        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );

        $products_for_template = [];

        foreach ($products as $rawProduct) {
            $products_for_template[] = $presenter->present(
                $presentationSettings,
                $assembler->assembleProduct($rawProduct),
                $this->context->language
            );
        }

        return $products_for_template;
    }

    public function getWidgetVariables($product_id)
    {
        $tabs = $this->getAllTabs();
        foreach ($tabs as $key => $tab) {
            $id_fields = json_decode($tab['ids_field']);
            foreach ($id_fields as $id_field) {
                $field = $this->getField($id_field);
                $tabs[$key]['fields'][$id_field] = $field;
                $tabs[$key]['fields'][$id_field]['value'] = $this->loadProductData($product_id, $this->context->language->id, $field['name'], $field['type']);
            }
        }

        return array(
            'current_language' => $this->context->language->id,
            'tabs' => $tabs,
            'product_id' => $product_id
        );
    }

    protected function ajaxPositions()
    {
        $positions = Tools::getValue('fields');
        if (!empty($positions)) {
            $table = "additionalproductfields";
        } else {
            $positions = Tools::getValue('tabs');
            if (!empty($positions)) {
                $table = "additionalproductfields_settings_tab";
            }
        }

        if (empty($positions)) {
            return;
        }

        foreach ($positions as $position => $value) {
            $pos = explode('_', $value);

            if (isset($pos[2])) {
                $this->updatePositionFields($pos[2], $position + 1, $table);
            }
        }

        $this->_clearCache('*');
    }

    public function updatePositionFields($id_field, $position, $table)
    {
        $data = array('position' => (int)$position);
        $where = 'ID = '.(int)$id_field;

        Db::getInstance()->update($table, $data, $where);
    }

    public static function getNextPosition($table)
    {
        $sql = 'SELECT MAX(`position`)
				FROM `'._DB_PREFIX_.$table.'`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? ++$position : 1;
    }

    //Functions for Tabs
    protected function settingsTab($message = null)
    {
        $this->context->smarty->assign(
            array(
                'id_language' => $this->context->language->id,
                'link' => $this->context->link,
                'tabs' => $this->getAllTabs(),
                'message' => $message,
            )
        );
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/settings_tab.tpl');
        return $output;
    }

    protected function getAllTabs()
    {
        $query = new DbQuery();
        $query->select('*');
        $query->from('additionalproductfields_settings_tab', 'apf_st');
        $query->join('LEFT JOIN `'._DB_PREFIX_.'additionalproductfields_lang` apf_l ON apf_st.`ID` = apf_l.`id_field` AND apf_l.group_name = \'tabs\'');
        $query->orderBy('position');

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        $tabs = array();
        foreach ($result as $item) {
            if (isset($tabs[$item['id_field']])) {
                $tabs[$item['id_field']]['title'][$item['id_lang_title']] = $item['title'];
            } else {
                $tabs[$item['id_field']] = $item;
                unset($tabs[$item['id_field']]['title']);
                $tabs[$item['id_field']]['title'][$item['id_lang_title']] = $item['title'];
            }
        }

        return $tabs;
    }

    public function addCustomization($price,$id_product,$id_product_attribute,$id_cart)
    {
        $data = array(
            'id_product_attribute' => $id_product_attribute,
            'id_address_delivery' => '1',
            'id_cart' => $id_cart,
            'id_product' => $id_product,
            'quantity' => '0',
            'quantity_refunded' => '0',
            'quantity_returned' => '0',
            'in_cart' => '1',
        );
        $q=Db::getInstance()->insert('customization', $data);
        if (!$q) {
            return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 14'), 'module_error alert-danger');
        }
        $id_customization=Db::getInstance()->Insert_ID();
        $data = array(
            'id_customization' => $id_customization,
            'type' => 0,
            'index' => 0,
            'value' => 0,
            'id_module' => 0,
            'price' => $price,
            'weight' => 0,
        );
        if (!Db::getInstance()->insert('customized_data', $data)) {
            return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 14'), 'module_error alert-danger');
        }
        $data = array(
            'id_product' => $id_product,
            'type' => 0,
            'required' => 0,
            'is_module' => 0,
            'is_deleted' => 0,
        );
        if (!Db::getInstance()->insert('customization_field', $data)) {
            return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 14'), 'module_error alert-danger');
        }

        //$where = "id_cart = ".pSQL($id_cart)." AND id_product = ".pSQL($id_product)." AND id_address_delivery = `0` AND id_shop = `1` AND id_product_attribute = ".pSQL($id_product_attribute)." AND id_customization = `0`";
        $where = "id_cart = ".pSQL($id_cart)." AND id_product = 1";
        $sql = 'UPDATE '._DB_PREFIX_.'cart_product SET
        id_cart='.$id_cart.',
        id_product='.$id_product.',
        id_address_delivery=1,
        id_shop=1,
        id_product_attribute='.$id_product_attribute.',
        id_customization='.$id_customization.',
        quantity=1
        WHERE '.$where;

        if (!$result=Db::getInstance()->execute($sql))
            die('error!');

        return $result;
    }

    public function removeCustomization($id_product, $id_product_attribute,$id_cart){
        $data = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'cart_product` WHERE id_product=\'' . pSQL($id_product) . '\' AND  id_product_attribute ='.pSQL($id_product_attribute));
        foreach($data as $field){
            if (!empty($field['id_customization'])){
                $where = "id_cart = ".pSQL($id_cart)." AND id_product = 1";
                $sql = 'UPDATE '._DB_PREFIX_.'cart_product SET
        id_cart='.$id_cart.',
        id_product='.$id_product.',
        id_address_delivery=1,
        id_shop=1,
        id_product_attribute='.$id_product_attribute.',
        id_customization=0,
        quantity=1
        WHERE '.$where;

                if (!Db::getInstance()->execute($sql))
                    die('error!');
            }
        }
        return $data;
    }

    public function addProductAttribute($id_product,$id_product_attribute,$id_product_base_attribute){
        $where = "id_product_attribute = ".pSQL($id_product_attribute);
        $data = Db::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'product_attribute` WHERE ' . $where);
        unset($data['id_product_attribute']);
        unset($data['default_on']);
//        $data_out = array(
//            'id_product' => $id_product,
//            'wholesale_price' => '0',
//            'price' => '0',
//            'ecotax' => '0',
//            'quantity' => '1',
//            'unit_price_impact' => '1',
//            'minimal_quantity' => '1',
//            'low_stock_alert' => '1',
//        );
        $q=Db::getInstance()->insert('product_attribute', $data);
        if (!$q) {
            return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 14'), 'module_error alert-danger');
        }
        $id_product_attribute=Db::getInstance()->Insert_ID();

        $data = array(
            'id_product' => $id_product,
            'id_product_attribute' => $id_product_attribute,
            'id_shop' => $this->context->shop->id,
            'available_date' => '0000-00-00'
        );
        $q=Db::getInstance()->insert('product_attribute_shop', $data);
        if (!$q) {
            return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 14'), 'module_error alert-danger');
        }

        $stock = new StockAvailable();
        $stock->id_product = $id_product;
        $stock->id_product_attribute = $id_product_attribute;
        $stock->id_shop = $this->context->shop->id;
        $stock->id_shop_group = 0;
        $stock->quantity = 50;
        $stock->out_of_stock = 2;
        $stock->add();
        $cart_product = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart='.$this->context->cart->id.' ORDER BY date_add DESC LIMIT 0,1');

        $where = "id_cart = ".pSQL($cart_product[0]['id_cart'])." AND id_product = ".pSQL($cart_product[0]['id_product'])." AND date_add = '".pSQL($cart_product[0]['date_add'])."'";
        $sql = 'UPDATE '._DB_PREFIX_.'cart_product SET
        id_product_attribute='.$id_product_attribute.'
        WHERE '.$where;
        if (!$result=Db::getInstance()->execute($sql))
            die('error!');

        $base_attributes = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'product_attribute_combination WHERE id_product_attribute='.$id_product_base_attribute);
        foreach($base_attributes as $base_attribute){
            $data = array(
                'id_attribute' => $base_attribute['id_attribute'],
                'id_product_attribute' => $id_product_attribute,
            );
            $q=Db::getInstance()->insert('product_attribute_combination', $data);
            if (!$q) {
                return array($this->l('ERROR. Please contact the module developers. ERROR CODE: 14'), 'module_error alert-danger');
            }
        }

        return $id_product_attribute;
    }
    public function getBaseAttribute(){
        $cart_product = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart='.$this->context->cart->id.' ORDER BY date_add DESC LIMIT 0,1');
        return $cart_product[0]['id_product_attribute'];
    }

}

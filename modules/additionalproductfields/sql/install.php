<?php
/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    DMTAY <info@dmtay.ru>
 *  @copyright 2007-2019 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */
/*
$engine = _MYSQL_ENGINE_;

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'additionalproductfields_value` (
                                        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                                        `id_product` INT(10) UNSIGNED NOT NULL,
                                        `id_lang` INT(10) UNSIGNED NOT NULL,
                                        `name` VARCHAR(100) NOT NULL,
                                        `value` TEXT,
                                        PRIMARY KEY (`id`)
                                        ) ENGINE='.$engine.' DEFAULT CHARSET=utf8;';
$sql[] = 'CREATE TABLE `'._DB_PREFIX_.'additionalproductfields` 
                ( 
                `ID` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(100) NOT NULL ,
                `type` VARCHAR(100) NOT NULL ,
                `position` INT NOT NULL,
                `is_show_title` INT NOT NULL,
                `class` VARCHAR(512) ,
                `configure` TEXT,
                `meta` TEXT,
                PRIMARY KEY  (`ID`)) ENGINE = '.$engine.';';

$sql[] = 'CREATE TABLE `'._DB_PREFIX_.'additionalproductfields_lang` (
          `ID` int(11) NOT NULL AUTO_INCREMENT,
          `id_field` int(11) NOT NULL,
          `id_lang_title` int(11) NOT NULL,
          `title` varchar(512) NOT NULL,
          `content_type` varchar(25) NOT NULL,
          `group_name` varchar(100) NOT NULL,
          PRIMARY KEY (`ID`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE `ps_additionalproductfields_settings_tab` (
          `ID` int(11) NOT NULL AUTO_INCREMENT,
          `ids_field` text NOT NULL,
          `position` int(11) NOT NULL,
          PRIMARY KEY (`ID`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}*/
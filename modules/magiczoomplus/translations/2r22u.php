<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_7a6a7c07bef6c56d5af1fddd93d5b8ab'] = 'Настройки по умолчанию увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_3776a0c6ecac9d1059b422de73b217bb'] = 'Настройки по умолчанию увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_79d4f5ec62eb98182598ccb7e931f65e'] = 'Настройки по умолчанию увеличить текст подсказки для мобильных устройств (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_aa00cf55ecbd961a54bd3a6895a80a7c'] = 'Настройки по умолчанию увеличить текст подсказки для мобильных устройств (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d78779b683a2501f171c17cf3e2f5005'] = 'Параметры по умолчанию развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_c38cc61d6ca436af08242e61ef1763c4'] = 'Параметры по умолчанию развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_b2079f0d037519ea18143482fc2abdd6'] = 'Параметры по умолчанию кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_24d383e3b206f3b002f55425334c2775'] = 'Параметры по умолчанию кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_842b6faabe92883cebebcf3f909adb95'] = 'Параметры по умолчанию кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_c267a22eded06c37c0712ca984213247'] = 'Параметры по умолчанию сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_51d86e31800f9da1f2391665e5402b31'] = 'Страница товара увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_66f2f9b244180ba1e594f1d28f157ef0'] = 'Страница товара увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_e6803515982cb1c6bb80d04f03add39e'] = 'Страница товара развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5753760389892b4cb143fca46709c007'] = 'Страница товара развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_c4b840ffed55a8290a3d55cfb7f741e8'] = 'Страница товара кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_b307c47ef98d30f6262faaf52806dfb3'] = 'Страница товара кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_1e1093ec74320115f84dab3cfee68963'] = 'Страница товара кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_289712780436b27f81ef5dca008ddd05'] = 'Страница товара сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d7a735e8e62ef760fb317abff89cf09a'] = 'Страница категории увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_b7b23c9f6e74102626ab8da54dd8c374'] = 'Страница категории увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_316db827700d5f1a323d63c0dae807d2'] = 'Страница категории увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8c9fe0135dfc7b7bce446f7a2abd119e'] = 'Страница категории увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_040a6ff6c2ccd54edc92c96362cf699e'] = 'Страница категории развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_87a888220e423a6124e28df4d62c9117'] = 'Страница категории развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_a67835b8df3307dca8830d6433cb8c8b'] = 'Страница категории кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_55a102c2fa5d9df3e8d156e187fe0067'] = 'Страница категории кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8221fe50eb6f82845d79f24db8b7b157'] = 'Страница категории кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_a14e8e3307231943672ce54285666da8'] = 'Страница категории сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_7eb55abec832d8666601ff80b06ca7a8'] = 'Страница производителей увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_bb4dbc0293552bd488daf48d73a748ed'] = 'Страница производителей увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_e7e611539f8864cfd27a691d1d26def4'] = 'Страница производителей увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_903f3a197ca0320b72b9ee51b6f47ac2'] = 'Страница производителей увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8f93562157eadc55bba26deb5ebf53ae'] = 'Страница производителей развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_849b3ef07408d27f80ac41acdbc0e70f'] = 'Страница производителей развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_58ea124101dec1f7962c512d1d5a0fdb'] = 'Страница производителей кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_55c64cccf7da5d44c1b1608fcfef719f'] = 'Страница производителей кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5b90804d99b1214211a58946b430bad6'] = 'Страница производителей кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_2e53b761eae2e10de74c3d519bca261f'] = 'Страница производителей сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_7f86bccc85a77cf48366d5837301b21f'] = 'Страница новые товары увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_78b5aca05ace112cbf3bb1cbf9d604d8'] = 'Страница новые товары увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_2fe2b39fef83a73f252c414170e3ceb1'] = 'Страница новые товары увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_1cc8ee1d00fad2ee96b9a3c7a9a0b871'] = 'Страница новые товары увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_9eaf7d0e183136982c7768436e76ba54'] = 'Страница новые товары развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_3927e48dca02c76f0a8ae5c1e0935b3e'] = 'Страница новые товары развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5d5237a4c72f130414447d1103ed6226'] = 'Страница новые товары кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_e6b5006295e182ab22aaf8bb7a1444f0'] = 'Страница новые товары кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_350197ae0a403c08e0bec9a41cf3d229'] = 'Страница новые товары кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_40ca389310963079c79f88aeba272b3f'] = 'Страница новые товары сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5571be510cffface822598d1837ccfd6'] = 'Боковое меню новые товары увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_fc3b3d182f2d39712170357eca7e92e0'] = 'Боковое меню новые товары увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_24e9f771dffd6320a0dcba77d4b3ff08'] = 'Боковое меню новые товары увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5b16b9110c0d11d9aade9bac2701de3a'] = 'Боковое меню новые товары увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_63226a6e45f4723d5861b1f66aafda1b'] = 'Боковое меню новые товары развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_9d96187a1d50baa8b03f80fc18c7392e'] = 'Боковое меню новые товары развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_a7773d79acb8961f74a3bfe72ae146e2'] = 'Боковое меню новые товары кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5e8612a3e588b22a76eaf8da4ebee3a2'] = 'Боковое меню новые товары кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_b96f41441d83c25cd720dce63be13212'] = 'Боковое меню новые товары кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_faeb7b2e426772dab5616cf9bac0a2e6'] = 'Боковое меню новые товары сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_abb6513e6934b2f1b73ba1c0fc4976b2'] = 'Блок новые товары увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_08e9911e14b8d8eaeebed586e0423ab1'] = 'Блок новые товары увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_22224db547fec0e1fc4ba8c4a3892845'] = 'Блок новые товары увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d0e80a412c6819406574e640a84f2cf2'] = 'Блок новые товары увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_b78d6febb2bf49789f9592ef768e0e48'] = 'Блок новые товары развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_1c5a4f71a98677c45352dd54d215f772'] = 'Блок новые товары развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_aa91a464242d6938fe1d03839d17e18b'] = 'Блок новые товары кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_95d3523fd4d5e8d3288d68065a7ee903'] = 'Блок новые товары кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_54b0986853aef30951c0e7e2607acb53'] = 'Блок новые товары кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_e8813d449871ffa82c0e3822dc9f0e84'] = 'Блок новые товары сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8694bb5237eba78ed0c429c8fb32bf26'] = 'Страница бестселлеры увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5b0f19ef902d0e36f220c26988594b4f'] = 'Страница бестселлеры увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_62a100f3137b2d9f33e7defb0634373b'] = 'Страница бестселлеры увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_775e0bc9be9c3b69a1963283a019b646'] = 'Страница бестселлеры увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_c4a11b1dd7446c5f1bec5d128438eba6'] = 'Страница бестселлеры развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_3af859e140982f5f53021a44e5c12305'] = 'Страница бестселлеры развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_0031d5cb4bfc7809bccbc48ba44749d8'] = 'Страница бестселлеры кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_28e911dcd4355f80dc0f384b616d0252'] = 'Страница бестселлеры кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_62cb72743ec55bf28da592db3dcf8ba3'] = 'Страница бестселлеры кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_9d400de263ebc4a7b5371e74ba4bfb43'] = 'Страница бестселлеры сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_eb6f41416f00a86ea277331c0d4e8b3d'] = 'Боковое меню бестселлеры увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_aa9c105ac08843031ee7bced65079ed7'] = 'Боковое меню бестселлеры увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d1301ec81cec84cb49f9febc012322ca'] = 'Боковое меню бестселлеры увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_fb9c0de4b789551be16d3cf53d04d9b3'] = 'Боковое меню бестселлеры увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_bce77504b1ae3c438f1b1bc00ff28d38'] = 'Боковое меню бестселлеры развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_87119f647c44d200b390d9e62054b174'] = 'Боковое меню бестселлеры развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_7eb86bba4ed8e52f9895c4af82b58eda'] = 'Боковое меню бестселлеры кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_cb15c0a6246c2ae03cec31acf62581a0'] = 'Боковое меню бестселлеры кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_c247aa85d54e95d65c42c5ecdb10c52c'] = 'Боковое меню бестселлеры кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_39b59c1f39739c0a67d901d6208fe336'] = 'Боковое меню бестселлеры сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_203931b07d74fb06894ddd0c06d38c28'] = 'Блок бестселлеры увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_f97b00bc5365ac2e47862c92b2c155de'] = 'Блок бестселлеры увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_e60c2294800ef3a28fcc940d3ce53b62'] = 'Блок бестселлеры увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d6a583e7a175352909aa65ef2d7482c9'] = 'Блок бестселлеры увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_80f66180fbf6307fac5f25288c72c972'] = 'Блок бестселлеры развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_4c6558ff949a25b463f09038ed9833ff'] = 'Блок бестселлеры развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_9262e9476881c5177424daaabc8b6fc9'] = 'Блок бестселлеры кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_30dee42abcd4ecbb119386acce615d3f'] = 'Блок бестселлеры кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_dba7dc20cfbd496134d33fe4e925343c'] = 'Блок бестселлеры кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5aeb0e6348f8b7410770548364e11f74'] = 'Блок бестселлеры сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_66ed9d97a954bc309c358aac34ca7671'] = 'Специальная страница увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_2fe86e16ee5af9db1d51b0cd394432be'] = 'Специальная страница увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_431bc62f854b6d077e560cdb94fffca2'] = 'Специальная страница увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_23a239898b3d5c24aeba6c04349476e3'] = 'Специальная страница увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_20b2189c51127764f4e5b6da2b06d1cb'] = 'Специальная страница развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5df31068de0a2a3f779f11c8a1e98c2d'] = 'Специальная страница развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8fd9aa3b40af5dbe9beeb6cd9f3f2e92'] = 'Специальная страница кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_e7c62da5d4c703a5e7dcb5e3c77f3586'] = 'Специальная страница кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_cc842d77289ce4e3efd2fa7a6b67d02a'] = 'Специальная страница кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_813b4e9aa9ee1e7d028389f096a87f43'] = 'Специальная страница сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_33160e605b9a9251b7eb594ac63e667c'] = 'Специальная боковая панель увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8d11e4fc2be5efa70570d1cb9ce096bc'] = 'Специальная боковая панель увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_9d740783c9267bacd8fd5ac1226f4d94'] = 'Специальная боковая панель увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8df09d63dff7de672d038d6ca1aaf10f'] = 'Специальная боковая панель увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_afb10c6399f0578453593942ab98a8f7'] = 'Специальная боковая панель развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_36368ff08ca562973f2a5b7503805764'] = 'Специальная боковая панель развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_fad6c10cba818c5b085737f09f7028e3'] = 'Специальная боковая панель кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d528c965d9da41e7f1abe68dceff0053'] = 'Специальная боковая панель кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_6e9fc5c0270aa8f566654d7d942d4d21'] = 'Специальная боковая панель кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_bb7feb988717203f06f10b6d8f522994'] = 'Специальная боковая панель сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_39ecb1488873e08314a12362fb3da2e9'] = 'Специальный блок увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_33eb76c36ad4475b2daa7dad5ab12292'] = 'Специальный блок увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_44c4315f83f07a777a5fbae4b9c5cf4c'] = 'Специальный блок увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_1ff98b6ce6745cb5a2f20d2901db0452'] = 'Специальный блок увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_a53076ad3df50085ef6f7a76b48c85ae'] = 'Специальный блок развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_82eb6e51ef984fcd718e760ae53787da'] = 'Специальный блок развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_74b25556b62fc55494a068c4a2a07c5a'] = 'Специальный блок кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_511f0a83fcefa35b352471160bc3b222'] = 'Специальный блок кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_442c40e7a4b3f5ba3da3c85a2cf85aee'] = 'Специальный блок кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_0212064aa531a862b401241223dbdf80'] = 'Специальный блок сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_0e14147d2ce0725b776916d46ed51935'] = 'Видимая боковая панель увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_9637cf97b8701d9421edd8c466fad154'] = 'Видимая боковая панель увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_3299c7acb0ce58c479679b986a2b07ad'] = 'Видимая боковая панель увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_db8fa3dbfa818b04babfd82ad9928a58'] = 'Видимая боковая панель увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_dbade5deb4a1cbe3582745f56a853689'] = 'Видимая боковая панель развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_20c8d062128ed360ef169e9ca102c4d0'] = 'Видимая боковая панель развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_69cb0a0ae8a4890b04ebf0978eed5c06'] = 'Видимая боковая панель кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_a89e63f4addaa96b911611a1002a51f6'] = 'Видимая боковая панель кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_eec7cde962ca13f7116c0f45c1963a1f'] = 'Видимая боковая панель кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_f016363ea6c61c9446c5e016c9d10c12'] = 'Видимая боковая панель сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d395cbdddde10d94bf25c3d5bae6d239'] = 'Отображаемый блок увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d6fd27434c45b9a1cd5c7c9c9572fa81'] = 'Отображаемый блок увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_090b774dae16ee39899c71f0b4fd41cc'] = 'Отображаемый блок увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_5222bf8985536e30539fc222ded6a387'] = 'Отображаемый блок увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8d24a1b66f78b577a5cc90db62ed2063'] = 'Отображаемый блок развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_e9185c6610fc0a39f21b80dca834a27f'] = 'Отображаемый блок развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_8e5c87a9e18be43275a5e59936e3252e'] = 'Отображаемый блок кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_7b10eb99bc69dd30be50e8a35887bd09'] = 'Отображаемый блок кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_47e4496b07a2a6dab639025a43993081'] = 'Отображаемый блок кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_623fa9d844f47100b71fbf8b77a6cf60'] = 'Отображаемый блок сообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_d23efd362a9b6fef89c4cfc4e566684e'] = 'Страница поиска увеличить текст подсказки (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_a7c317812254e53b4a209f31754d7106'] = 'Страница поиска увеличить текст подсказки (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_60cab2f37f450d4d9c09a655727a8ef7'] = 'Страница поиска увеличить текст подсказки для мобильных устройств  (при наведении)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_81f58ae0fa93aa9f16f601782867862f'] = 'Страница поиска увеличить текст подсказки для мобильных устройств  (при нажатии)';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_1d358465600b405442b9a9a9ce4be97c'] = 'Страница поиска развернуть текст подсказки';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_0b565584bcba0aca8a5305eb1bc30fd7'] = 'Страница поиска развернуть текст подсказки для мобильных устройств';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_b9f09e3ffd6449e3ec3bb34987889e40'] = 'Страница поиска кнопка закрыть текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_f2857b6d4f5aee16ad36991373866696'] = 'Страница поиска кнопка следующий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_3e27c0537f26203adf4a5452a8d665d4'] = 'Страница поиска кнопка предыдущий текст';
$_MODULE['<{magiczoomplus}prestashop>magiczoomplus_4eb4c4b82845a89ddba0dc25fea00a28'] = 'Страница поиска ообщение (под Magic Zoom Plus)';
$_MODULE['<{magiczoomplus}prestashop>product_videos_ps17_aec6d21bda803fda4a5d419df2b9cf46'] = 'Видео товара';
$_MODULE['<{magiczoomplus}prestashop>product_videos_ps17_d6dafee2e1332d6c1091b38ccf5c5bfd'] = 'Укажите ссылки на видео, разделенные пробелом или новой строкой.';
$_MODULE['<{magiczoomplus}prestashop>product_videos_ps17_51598eb73a1185a4bddca5452178e7cd'] = '\"Видео товара\" содержит ошибочные urls адреса:';
$_MODULE['<{magiczoomplus}prestashop>product_videos_51598eb73a1185a4bddca5452178e7cd'] = '\"Видео продукт\" содержит ошибочные адреса:';
$_MODULE['<{magiczoomplus}prestashop>product_videos_3b3194791495ad4fe950fb318f2b36f4'] = '\"Видео продукт\" содержит ошибочные адреса:';
$_MODULE['<{magiczoomplus}prestashop>product_videos_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{magiczoomplus}prestashop>product_videos_9ea67be453eaccf020697b4654fc021a'] = 'Сохранить и остаться';
$_MODULE['<{magiczoomplus}prestashop>product_videos_aec6d21bda803fda4a5d419df2b9cf46'] = 'Видео товара';

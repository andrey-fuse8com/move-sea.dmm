{**
* 2005-2017 Magic Toolbox
*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Magic Toolbox <support@magictoolbox.com>
*  @copyright Copyright (c) 2017 Magic Toolbox <support@magictoolbox.com>. All rights reserved
*  @license   https://www.magictoolbox.com/license/
*}

<!-- MagicThumb -->
{if $legacy_template}
<style>
#magicthumb-template {
    display: none;
}
.magicthumb-settings {
    width: 100%;
}
.magicthumb-settings td {
    text-align: center;
}
.magicthumb-settings .error {
    text-align: left;
}
.magicthumb-settings fieldset {
    border: none;
}
.magicthumb-settings textarea {
    margin: 0;
    width: 99% !important;
}
.magicthumb-settings p {
    text-align: left;
}
</style>
<div id="magicthumb-template">
    <table class="magicthumb-settings" cellpadding="5">
        {if isset($magicthumb_invalid_urls)}
        <tr>
            <td>
                <div class="error">
                    <img src="../img/admin/error2.png" />
                    {l s='"Product Videos" value contains incorrect urls:' mod='magicthumb'}
                    <ol>
                        {foreach from=$magicthumb_invalid_urls item=url}
                            <li>{$url|escape:'html':'UTF-8'}</li>
                        {/foreach}
                    </ol>
                </div>
            </td>
        </tr>
        {/if}
        <tr>
            <td>
                <fieldset>
                    <legend>Product Videos</legend>
                    <textarea name="magicthumb_video" rows="10" cols="45">{if isset($magicthumb_textarea)}{$magicthumb_textarea|escape:'html':'UTF-8'}{/if}</textarea>
                    <p>{l s='Provide links to video separated by a space or new line' mod='magicthumb'}</p>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="{l s='Save' mod='magicthumb'}" name="submitAddproduct" class="button" />&nbsp;
                <input type="submit" value="{l s='Save and stay' mod='magicthumb'}" name="submitAddproductAndStay" class="button" />
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
//<![CDATA[
    var mtTabNumber = $('.tab-page').length + 1;
    $('.tab-page:last').after(
        '<div class="tab-page" id="step'+mtTabNumber+'"><h4 class="tab">'+mtTabNumber+'. MagicThumb</h4>'+
        $('#magicthumb-template').html()+
        '</div>'
    );
//]]>
</script>
{else}
<style>
.magicthumb-settings .error {
    overflow: hidden;
}
.magicthumb-settings fieldset {
    border: none;
}
.magicthumb-settings textarea {
    margin: 0;
    width: 99% !important;
    resize: vertical;
}
</style>
<div class="magicthumb-settings panel product-tab">
    <input type="hidden" name="submitted_tabs[]" value="magicthumb" />
    {if $p16x_template}
    <h3>{l s='Product Videos' mod='magicthumb'}</h3>
    {else}
    <h4>{l s='Product Videos' mod='magicthumb'}</h4>
    {/if}
    <div class="separation"></div>
    {if isset($magicthumb_invalid_urls)}
    {if $p16x_template}
    <div class="bootstrap">
        <div class="module_error alert alert-danger" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {l s='"Product Videos" value contains incorrect urls:' mod='magicthumb'}
            <ul>
                {foreach from=$magicthumb_invalid_urls item=url}
                    <li>{$url|escape:'html':'UTF-8'}</li>
                {/foreach}
            </ul>
        </div>
    </div>
    {else}
    <div class="error">
        <span style="float:right">
            <a id="hideError" href="#"><img alt="X" src="../img/admin/close.png" /></a>
        </span>
        {l s='"Product Videos" value contains incorrect urls:' mod='magicthumb'}
        <br/>
        <ol>
            {foreach from=$magicthumb_invalid_urls item=url}
                <li>{$url|escape:'html':'UTF-8'}</li>
            {/foreach}
        </ol>
    </div>
    {/if}
    {/if}
    <fieldset>
        <textarea name="magicthumb_video" rows="10">{if isset($magicthumb_textarea)}{$magicthumb_textarea|escape:'html':'UTF-8'}{/if}</textarea>
        <p class="{if $p16x_template}help-block{else}preference_description{/if}">{l s='Provide links to video separated by a space or new line' mod='magicthumb'}</p>
    </fieldset>
    {if $p16x_template}
    <div class="panel-footer">
        <button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save' mod='magicthumb'}</button>
        <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save and stay' mod='magicthumb'}</button>
    </div>
    {/if}
</div>
<div class="clear">&nbsp;</div>
{/if}
<!-- /MagicThumb -->

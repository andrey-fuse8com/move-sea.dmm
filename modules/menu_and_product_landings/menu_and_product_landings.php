<?php
/**
 * 2007-2019 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Menu_and_product_landings extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'menu_and_product_landings';
        $this->tab = 'content_management';
        $this->version = '0.0.1';
        $this->author = 'dmtay';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('menu_and_product_landings');
        $this->description = $this->l('Allows to make landing pages for each product and display it in category page menus');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__) . '/sql/install.php');
        Configuration::updateValue('MENU_AND_PRODUCT_LANDINGS_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayNavFullWidth') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        include(dirname(__FILE__) . '/sql/uninstall.php');
        Configuration::deleteByName('MENU_AND_PRODUCT_LANDINGS_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $message = '';
        /**
         * If values have been submitted in the form, process.
         */
        if (Tools::isSubmit('update_field')) {
            return $this->renderForm(Tools::getValue('update_field'));
        }
        if (Tools::isSubmit('delete_id_field')) {
            $message = $this->deleteField(Tools::getValue('delete_id_field'));
        }
        if (((bool)Tools::isSubmit('submitMenu_and_product_landingsModule')) == true) {
            $this->postProcess();
        }

        $data = $this->get_fields();
        $this->context->smarty->assign(
            array(
                'fields' => $data,
                'module_dir' => $this->_path,
                'link' => $this->context->link,
                'message' => $message,
            )
        );

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output . $this->renderForm();
    }

    public function hookdisplayNavFullWidth()
    {
        $product_id = 0;
        if (!empty(Tools::getValue('id_category'))) {
            $cat = Tools::getValue('id_category');
        } else if (!empty($this->get_field_by_page(Tools::getValue('id_cms')))) {
            $field = $this->get_field_by_page(Tools::getValue('id_cms'));

            $cat = $field['product_id'];
            $product_id = $field['product_id'];
        }

        if (!empty($cat)) {
            $data = $this->get_fields();
            $child = ($this->get_field_by_id_product($cat))['product_id'];
            $parent = new Category((new Category($child))->id_parent);
            /*var_dump($this->get_field_by_page($cat));
            var_dump($cat);
            var_dump($child);
            exit();*/
            $category = new Category($cat);
            if ($category->id_parent == 2) {
                return "";
            }
            $nb = 10000;
            $products = $parent->getSubCategories((int)Context::getContext()->language->id, 1);
            foreach ($products as $key => $product) {
                foreach ($data as $field) {
                    if ($product['id_category'] == $field['product_id']) {
                        $products[$key]['lp'] = new CMS($field['page_id'], $this->context->language->id, $this->context->shop->id);
                        $products_out[] = $products[$key];
                    }
                }
            }

            //$test_LP = new CMS(12, $this->context->language->id, $this->context->shop->id);

            $this->context->smarty->assign(
                array(
                    'products' => $products_out,
                    'product_id' => $product_id,
                    'request' => $_REQUEST,
                    'base_url' => Context::getContext()->shop->getBaseURL(true) . $this->context->language->iso_code . '/',
                )
            );

            $output = $this->context->smarty->fetch($this->local_path . 'views/templates/front/cat_menu.tpl');

            return $output;
        }
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm($product_id = null)
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMenu_and_product_landingsModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;

        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        if ($product_id) {
            $data = $this->get_field($product_id);
            $helper->fields_value['PRODUCT_ID'] = $data['product_id'];
            $helper->fields_value['PAGE_ID_CONFIGURATION'] = $data['page_id_configuration'];
            $helper->fields_value['PAGE_ID'] = $data['page_id'];
        } else {
            $helper->fields_value['PRODUCT_ID'] = '';
            $helper->fields_value['PAGE_ID_CONFIGURATION'] = '';
            $helper->fields_value['PAGE_ID'] = '';
        }

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 1,
                        'type' => 'text',
                        'name' => 'PRODUCT_ID',
                        'label' => $this->l('Category ID'),
                    ),
                    array(
                        'col' => 1,
                        'type' => 'text',
                        'name' => 'PAGE_ID',
                        'label' => $this->l('Page ID'),
                    ),
                    array(
                        'col' => 1,
                        'type' => 'text',
                        'name' => 'PAGE_ID_CONFIGURATION',
                        'label' => $this->l('Page Configuration ID'),
                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        if (Tools::isSubmit('update_field')) {
            $field = $this->get_field(Tools::getValue('update_field'));
            $out['page_id'] = $field['page_id'];
            $out['page_id_configuration'] = $field['page_id_configuration'];
            $out['product_id'] = $field['product_id'];
        } else {
            $out['page_id'] = '';
            $out['product_id'] = '';
        }
        return array(
            'MENU_AND_PRODUCT_LANDINGS_LIVE_MODE' => Configuration::get('MENU_AND_PRODUCT_LANDINGS_LIVE_MODE', true),
            'MENU_AND_PRODUCT_LANDINGS_ACCOUNT_EMAIL' => Configuration::get('MENU_AND_PRODUCT_LANDINGS_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'MENU_AND_PRODUCT_LANDINGS_ACCOUNT_PASSWORD' => Configuration::get('MENU_AND_PRODUCT_LANDINGS_ACCOUNT_PASSWORD', null),
            'PAGE_ID' => $out['page_id'],
            'PAGE_ID_CONFIGURATION' => $out['page_id_configuration'],
            'PRODUCT_ID' => $out['product_id'],
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();
        $data = [];
        foreach (array_keys($form_values) as $key) {
            //Configuration::updateValue($key, Tools::getValue($key));
            $data[$key] = Tools::getValue($key);
        }
        $this->update_field($data);

    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {

        //$this->context->controller->addCSS($this->_path . 'views/css/style.css');
        $this->context->controller->addCSS($this->_path . 'views/css/front.css');
        $this->context->controller->addCSS($this->_path . 'views/fifish/v6/css/main.css');
        if (Tools::getValue('id_cms')){
            $this->context->controller->addCSS($this->_path . 'views/css/swiper.min.css');
            $this->context->controller->addCSS($this->_path . 'views/css/spark.min.css');
        }

        if (in_array(Tools::getValue('id_cms'), array(12,13,14,15,16,17))){
        }
        if (in_array(Tools::getValue('id_cms'), array(20,35))) {
            //$this->context->controller->addCSS($this->_path . 'views/css/back.css');
            $this->context->controller->addCSS($this->_path . 'views/css/swiper.min.css');
            $this->context->controller->addCSS($this->_path . 'views/css/biki.css');
        }
        if(in_array(Tools::getValue('id_cms'), array(22,33))) {
            $this->context->controller->addCSS($this->_path . 'views/css/shark.css');
            $this->context->controller->addCSS($this->_path . 'views/css/swii.css');
        }
        if(in_array(Tools::getValue('id_cms'), array(23,32))) {
            $this->context->controller->addCSS($this->_path . 'views/css/swii.css');
        }
        if(in_array(Tools::getValue('id_cms'), array(24,34))) {
            $this->context->controller->addCSS($this->_path . 'views/css/seabow.css');
            $this->context->controller->addCSS($this->_path . 'views/css/swii.css');
        }
        if(in_array(Tools::getValue('id_cms'), array(18,25,26,27,28,29,61,38,69,63))) {
            $this->context->controller->addCSS($this->_path . 'views/css/trident_carusel.css');
            $this->context->controller->addCSS($this->_path . 'views/css/trident_swip.css');
            $this->context->controller->addCSS($this->_path . 'views/css/trident.css');
            $this->context->controller->addCSS($this->_path . 'views/css/titan.css');
            $this->context->controller->addCSS($this->_path . 'views/css/poseidon.css');
            $this->context->controller->addCSS($this->_path . 'views/css/trident_s2.css');

        }
        if(in_array(Tools::getValue('id_cms'), array(19,30))) {
            $this->context->controller->addCSS($this->_path . 'views/css/lefeet.css');

        }
        if(in_array(Tools::getValue('id_cms'), array(36,37))) {
            $this->context->controller->addCSS($this->_path . 'views/css/youcanrobot.css');
            $this->context->controller->addCSS($this->_path . 'views/css/trident_s2.css');
        }
        if (in_array(Tools::getValue('id_cms'), array(21))) {
            $this->context->controller->addCSS($this->_path . 'views/css/style.css');
        }
    }

    public function get_field($product_id)
    {
        $data = Db::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'mapl` WHERE product_id=\'' . pSQL($product_id) . "'");
        return $data;
    }

    public function get_field_by_page($page_id)
    {
        $data = Db::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'mapl` WHERE page_id=\'' . pSQL($page_id) . "' OR page_id_configuration = '" . pSQL($page_id) . "'");
        return $data;
    }

    public function get_fields()
    {
        $data = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'mapl`');
        return $data;
    }

    public function get_field_by_id_product($product_id)
    {
        $data = Db::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'mapl` WHERE `product_id` = ' . $product_id);
        return $data;
    }

    public function deleteField($product_id)
    {


        if ($this->get_field($product_id)) {
            if (Db::getInstance()->delete('mapl', 'product_id = \'' . pSQL($product_id) . '\'')) {
                return array($this->l('Field successfully deleted'), 'module_confirmation conf confirm alert-success');

            }
        }
        return array($this->l('Cannot delete this field'), 'module_error alert-danger');
    }

    public function update_field($data)
    {
        if (!empty($this->get_field($data['PRODUCT_ID']))) {
            $sql = 'UPDATE ' . _DB_PREFIX_ . 'mapl SET
        product_id=' . $data['PRODUCT_ID'] . ',
        page_id=' . $data['PAGE_ID'] . ',
        page_id_configuration=' . $data['PAGE_ID_CONFIGURATION'] . '
        WHERE product_id=' . $data['PRODUCT_ID'];
            if (!$result = Db::getInstance()->execute($sql))
                die('error!');

            return $sql;
        } else {
            $sql = 'INSERT ' . _DB_PREFIX_ . 'mapl SET
        product_id=' . $data['PRODUCT_ID'] . ',
        page_id=' . $data['PAGE_ID'] . ',
        page_id_configuration=' . $data['PAGE_ID_CONFIGURATION'];
            if (!$result = Db::getInstance()->execute($sql))
                die('error!');

            return $sql;
        }

    }
}

{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if count($products) > 1}
<nav class="header__second-menu">
    <ul class="top-menu" id="top-menu" data-depth="0" style="text-align:center;">
    {foreach $products as $product}


        <li class="product-{$product.id_category} {if ($product.id_category == $product_id)}lp_active{/if}" id="product-{$product.id_category}" style="display:inline-block; border:solid; "  >

            {if (isset($product.lp))}
            <a class="dropdown-item" href="{$base_url}content/{$product.lp->id}-{$product.lp->link_rewrite}" data-depth="0">
                {$product.lp->meta_title}
            </a>
            {else}
                <a class="dropdown-item" href="" data-depth="0">
                    {$product.name}
                </a>
            {/if}
        </li>
    {/foreach}
    </ul>
{if !empty($chosen_product)}
    <h1 style="text-align: center">{$chosen_product}</h1>
{/if}
</nav>
{/if}
{*{$test->content nofilter}*}
{**}

var header =  document.querySelector('.header');
        var headerHeight = header.clientHeight;
		var productBar =  document.querySelector('.pro-nav');
        var productBarHeight = productBar.clientHeight;
 		var productsAllBar =  document.querySelector('.header__menu + nav');
		if(header && productBar){
			window.onscroll = function() {
				var scrolled = window.pageYOffset || document.documentElement.scrollTop;
				if (scrolled > headerHeight + productBarHeight + 50) {
                    //header.style.height = headerHeight + productBarHeight + 'px';
					productBar.classList.add('pro-nav--fixed')
                    if ($(".header__second-menu").length === 0){
                        $(productBar).css("top","0px")
                    }
                    if (productsAllBar !== null) {
                        productsAllBar.classList.add('nav-products--fixed')
                    }
				} else {
                    //header.style.height = headerHeight + 'px';
					productBar.classList.remove('pro-nav--fixed')
                    if (productsAllBar !== null) {
                        productsAllBar.classList.remove('nav-products--fixed')
                    }
				}
			}
		}

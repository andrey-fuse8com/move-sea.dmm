var mySwiper = new Swiper ('#titan-one-swiper', {
  cssMode: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '#titan-one-swiper .swiper-pagination',
        clickable: true,
      },
      mousewheel: false,
      keyboard: true,
      autoplay: {
        dalay: 3000,
      },
      on: {
		    init: function(){		     
		      window.setTimeout(function(){
		      	$("#titan-one-swiper .swiper-pagination-top span").each(function(index){
				  $(this).html('<img src="/modules/menu_and_product_landings/views/img/titan-pc-two-t'+(index+1)+'.png"><i></i>');
			  });
		      },3000)
       			
		    }, 
		},
     
}); 

var mySwiper = new Swiper ('#titan-two-swiper', {
  pagination: {
    el: '#titan-two-swiper .swiper-pagination',
    clickable :true,
  },
  autoplay: {
    dalay: 3000,
  },
  on: {
    init: function(){		     
      window.setTimeout(function(){
        $("#titan-two-swiper .swiper-pagination-top span").each(function(index){
      $(this).html('<img src="/modules/menu_and_product_landings/views/img/titan-pc-six-f'+(index+1)+'.png"><i></i><p>'+tabsTitle[index]+'</p>');
    });
      },3000)         
    }
  }
})
var isScrolling = false;
 
window.addEventListener("scroll", throttleScroll, false);

function throttleScroll(e) {
  if (isScrolling == false) {
    window.requestAnimationFrame(function() {
      scrolling(e);
      scrollingLeft(e);
      scrollingRight(e);
      scrollingTextLeft(e);
      scrollingTextRight(e);
      scrollingFilled(e);
      isScrolling = false;
    });
  }
  isScrolling = true;
}

document.addEventListener("DOMContentLoaded", scrolling, false);
document.addEventListener("DOMContentLoaded", scrollingLeft, false);
document.addEventListener("DOMContentLoaded", scrollingRight, false);
document.addEventListener("DOMContentLoaded", scrollingTextLeft, false);
document.addEventListener("DOMContentLoaded", scrollingTextRight, false);
document.addEventListener("DOMContentLoaded", scrollingFilled, false);

var listItems = document.querySelectorAll(".animation");
var listItemsLeft = document.querySelectorAll(".swii__block-item--left");
var listItemsRight = document.querySelectorAll(".swii__block-item--right");
var listItemsTextRight = document.querySelectorAll(".swii__block-item_text--right");
var listItemsTextLeft = document.querySelectorAll(".swii__block-item_text--left");
var listItemsFilled = document.querySelectorAll(".filled");

function scrolling(e) {
  for (var i = 0; i < listItems.length; i++) {
    var listItem = listItems[i];

    if (isPartiallyVisible(listItem)) {
      listItem.classList.add("animation-fadein");
    } else {
      listItem.classList.remove("animation-fadein");
    }
  }
}
function scrollingFilled(e) {
  for (var i = 0; i < listItemsFilled.length; i++) {
    var listItem = listItemsFilled[i];

    if (isPartiallyVisible(listItem)) {
      listItem.classList.add("filled--active");
    }
  }
}
function scrollingLeft(e) {
  for (var i = 0; i < listItemsLeft.length; i++) {
    var listItem = listItemsLeft[i];

    if (isPartiallyVisibleBlock(listItem)) {
      listItem.classList.add("swii__block-item--left-action");
    } 
  }
}

function scrollingRight(e) {
  for (var i = 0; i < listItemsRight.length; i++) {
    var listItem = listItemsRight[i];

    if (isPartiallyVisibleBlock(listItem)) {
      listItem.classList.add("swii__block-item--right-action");
    } 
  }
}

function scrollingTextLeft(e) {
  for (var i = 0; i < listItemsTextLeft.length; i++) {
    var listItem = listItemsTextLeft[i];

    if (isPartiallyVisibleText(listItem)) {
      listItem.classList.add("swii__block-item_text--left-action");
    } 
  }
}
function scrollingTextRight(e) {
  for (var i = 0; i < listItemsTextRight.length; i++) {
    var listItem = listItemsTextRight[i];

    if (isPartiallyVisibleText(listItem)) {
      listItem.classList.add("swii__block-item_text--right-action");
    } 
  }
}


function isPartiallyVisible(el) {
  if (typeof(el) === "undefined"){return false;}
  var elementBoundary = el.getBoundingClientRect();

  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;

  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}

function isPartiallyVisibleBlock(el) {
  var elementBoundary = el.getBoundingClientRect();

  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;

  return ((top + height >= 0) && (height + window.innerHeight >= bottom + 300));
}
function isPartiallyVisibleText(el) {
  var elementBoundary = el.getBoundingClientRect();

  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;

  return ((top + height >= 0) && (height + window.innerHeight >= bottom + 200));
}




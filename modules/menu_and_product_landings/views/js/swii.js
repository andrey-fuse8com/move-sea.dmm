if ("undefined" !== typeof(Swiper)) {
    var swiper = new Swiper('#swii__swiper-container', {
        slidesPerView: 2,
        centeredSlides: true,
        spaceBetween: 0,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 1,
                spaceBetween: 20
            }
        }
    });

//youtube video

    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;

    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            videoId: 'c9LSek2A-PU',
            playerVars: {
                'autoplay': 0,
                'controls': 1,
                'showinfo': 1,
                'loop': 1
            },
      
        });
    }  

    $('.swii__experience-item img').click(function (e) {
        player.loadVideoById($(this).data('video'))
        $('.swii__popap-video').css('display', 'block');
        $("body").css("overflow", "hidden");
    });

    $(document).mouseup(function (e) {
        var popup = $('.swii__popap-video');
        if (e.target != popup[0] && popup.has(e.target).length === 0) {
            player.stopVideo();
            $(".swii__popap-video").css('display', 'none');
            $("body").css("overflow", "auto");
        }
    });

    $('.seabow__popap-video-close').click(function (e) { 
        player.stopVideo();
        $(".swii__popap-video").css('display', 'none');
        $("body").css("overflow", "auto");
    });
}
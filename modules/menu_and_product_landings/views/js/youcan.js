
//youtube video

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
    vh = $(window).height();
    vw = $(window).width();

function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        videoId: 'gBsHjRwJUFo',
        playerVars: {
            'autoplay': 0,
            'controls': 1,
            'showinfo': 1,
            'loop': 1
        },
    });
}


$('.youcan__video-btn').click(function () {
  player.playVideo();
  $('.youcan__video-popup').css("display", "block");
  $("body").css("overflow", "hidden");
});

$('.youcan__video-popup_close').click(function () {
  player.stopVideo();
  $('.youcan__video-popup').css("display", "none");
  $("body").css("overflow", "auto");
});

$(document).mouseup(function (e) { 
  var popup = $('.youcan__video-popup_container');
 if (e.target!=popup[0]&&popup.has(e.target).length === 0){
    player.stopVideo();
     $(".youcan__video-popup").css('display', 'none');
 }
});

$('.seabow__video-image').click(function (e) { 
    player.playVideo();
  $('.seabow__popap-video').css('display', 'block');
  $("body").css("overflow", "hidden");
});



var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player_seabow', {
        videoId: 'PA2aCY0o7dY',
        playerVars: {
            'autoplay': 0,
            'controls': 1,
            'showinfo': 1,
            'loop': 1
        },
    });
}


$(document).mouseup(function (e) { 
    var popup = $('.seabow__popap-video');
   if (e.target!=popup[0]&&popup.has(e.target).length === 0){
      if (typeof(player.stopVideo) !== "undefined"){
          player.stopVideo();
          $(".seabow__popap-video").css('display', 'none');
          $("body").css("overflow", "auto");
       }
   }
  });

$('.seabow__popap-video-close').click(function (e) { 
    player.stopVideo();
    $(".seabow__popap-video").css('display', 'none');
    $("body").css("overflow", "auto");
});
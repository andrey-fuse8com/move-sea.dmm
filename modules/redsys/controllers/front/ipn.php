<?php
/**
* Card payment REDSYS platform (SERVIRED / SERMEPA)
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2017 idnovate
*  @license   See above
*/

class RedsysipnModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        if (!empty($_POST)) {
            $redsys = new Redsys();
            $signObject = new RedsysAPI();
            $redsystpv = new redsystpv();
            $parameters = Tools::getValue("Ds_MerchantParameters");
            $ds_signature = Tools::getValue("Ds_Signature");

            $decodec = $signObject->decodeMerchantParameters($parameters);
            $decodec_array = json_decode($decodec, true);
            $merchant_data = $decodec_array['Ds_MerchantData'];

            $merchant_data_array = explode(';', str_replace('+', ' ', $merchant_data));
            if (count($merchant_data_array) < 3) {
                $merchant_data_array = explode('%3B', str_replace('+', ' ', $merchant_data));
            }

            $amount_noconvert = $merchant_data_array[0];
            $validateOrdeName = $merchant_data_array[2];
            $tpv = new RedsysTPV($merchant_data_array[1]);

            $signature = $signObject->createMerchantSignatureNotif($tpv->encryption_key, $parameters);

            $total = $signObject->getParameter("Ds_Amount");
            $response = $signObject->getParameter("Ds_Response");
            $autorisation_code = $signObject->getParameter("Ds_AuthorisationCode");
            $ds_currency = $signObject->getParameter("Ds_Currency");
            $ds_order = $signObject->getParameter("Ds_Order");
            $ds_transactionType = $signObject->getParameter("Ds_TransactionType");

            if ($tpv->create_order == 0) {
                $cart = $redsys->getCartByOrderReference($ds_order);
                if (empty($cart->id)) {
                    $id_order = Tools::substr($ds_order, 0, 8);
                    $cart = new Cart((int)$id_order);
                }
                $id_order_ps = Order::getOrderByCartId($cart->id);
            } else {
                $id_order_ps = ltrim($ds_order, '0');
                $cart = new Cart((int)Order::getCartIdStatic($id_order_ps));
            }

            $extra_vars = array();
            $extra_vars['transaction_id'] = $ds_order;
            $id_currency_from_value = (int)$cart->id_currency;
            $id_currency_merchant_value = (int)$redsys->getIdByIsoCodeNum((int)$ds_currency, 0);

            if ($id_currency_from_value != $id_currency_merchant_value) {
                $total = number_format($amount_noconvert, 2, '.', '');
            } else {
                $total  = number_format($total / 100, 2, '.', '');
            }

            $total_order = $total;
            if ($tpv->advanced_payment && $tpv->advanced_percentage > 0) {
                $total_order = $cart->getOrderTotal(true);
                if ($tpv->fee_discount) {
                    $fee_discount_amount = $redsys->getFeeDiscount($tpv, $cart);
                    if ($fee_discount_amount != 0) {
                        $total_order += $fee_discount_amount;
                    }
                }
            }

            if ($signature === $ds_signature) {
                if ((int)$response < 101) {
                    if ($tpv->create_order == 1) {
                        if ($id_order_ps) {
                            $history = new OrderHistory();
                            $history->id_order = $id_order_ps;
                            if ($tpv->advanced_payment && $tpv->advanced_payment_state) {
                                $history->changeIdOrderState(Configuration::get('REDSYS_ADVANCED_PAYMENT_STATE'), $id_order_ps);
                            } else {
                                $history->changeIdOrderState(_PS_OS_PAYMENT_, $id_order_ps);
                            }
                            $history->addWithemail();
                            // send email confirmation blocked previously
                            $this->sendConfirmationEmail($id_order_ps, $cart, $extra_vars);
                            $sql = "INSERT INTO `".pSQL(_DB_PREFIX_.$redsys->name) ."_transaction` (`id_customer`, `id_tpv`, `id_cart`, `id_currency`, `ds_order`, `id_order`, `ds_response`, `ds_authorisationcode`, `amount`, `amount_total`, `transaction_date`, `transaction_type`, `id_shop`)
                                    VALUES (".pSQL($cart->id_customer).",".pSQL($tpv->id).",".pSQL($cart->id).",'".pSQL($ds_currency). "','".pSQL($ds_order)."',".pSQL($id_order_ps).",'".pSQL($response)."','".pSQL($autorisation_code)."','".pSQL($total)."','".pSQL($total_order)."','".date('Y-m-d H:i:s')."','".pSQL($ds_transactionType)."','".pSQL($cart->id_shop)."')";
                            try {
                                Db::getInstance()->Execute($sql);
                            } catch (Exception $ex) {
                                //$logger->logDebug("error al insertar: ".$ex->getMessage());
                            }
                        }
                    } else {
                        if ($tpv->advanced_payment && $tpv->advanced_percentage > 0) {
                            if ($tpv->advanced_payment_state) {
                                $redsys->validateOrderRedsys($id_order, Configuration::get('REDSYS_ADVANCED_PAYMENT_STATE'), $total, $validateOrdeName, null, $extra_vars, null, false, $cart->secure_key, null, $tpv);
                            } else {
                                $redsys->validateOrderRedsys($id_order, _PS_OS_PAYMENT_, $total, $validateOrdeName, null, $extra_vars, null, false, $cart->secure_key, null, $tpv);
                            }
                        } else {
                            if ($tpv->fee_discount) {
                                $redsys->validateOrderRedsys($id_order, _PS_OS_PAYMENT_, $total, $validateOrdeName, null, $extra_vars, null, false, $cart->secure_key, null, $tpv);
                            } else {
                               $redsys->validateOrder($id_order, _PS_OS_PAYMENT_, $total, $validateOrdeName, null, $extra_vars, null, false, $cart->secure_key);
                            }
                        }
                        //$id_order_ps = Order::getOrderByCartId($cart->id);
                        $id_order_ps = (int)$redsys->currentOrder;
                        $sql = "INSERT INTO `".pSQL(_DB_PREFIX_.$redsys->name) ."_transaction` (`id_customer`, `id_tpv`, `id_cart`, `id_currency`, `ds_order`, `id_order`, `ds_response`, `ds_authorisationcode`, `amount`, `amount_total`, `transaction_date`, `transaction_type`, `id_shop`)
                                VALUES (".pSQL($cart->id_customer).",".pSQL($tpv->id).",".pSQL($cart->id).",'".pSQL($ds_currency). "','".pSQL($ds_order)."',".pSQL($id_order_ps).",'".pSQL($response)."','".pSQL($autorisation_code)."','".pSQL($total)."','".pSQL($total_order)."','".date('Y-m-d H:i:s')."','".pSQL($ds_transactionType)."','".pSQL($cart->id_shop)."')";

                        try {
                            Db::getInstance()->Execute($sql);
                        } catch (Exception $ex) {
                            //$logger->logDebug("error al insertar: ".$ex->getMessage());
                        }
                    }
                } else {
                    if ($tpv->create_order == 1) {
                        if ($id_order_ps) {
                            $history = new OrderHistory();
                            $history->id_order = $id_order_ps;
                            $history->changeIdOrderState(_PS_OS_ERROR_, $id_order_ps);
                            $history->addWithemail();
                        }
                    } elseif ($tpv->payment_error == 1) {
                        $redsys->validateOrder($id_order, _PS_OS_ERROR_, $total, $validateOrdeName, null, $extra_vars, null, false, $cart->secure_key);
                        $id_order_ps = Order::getOrderByCartId($cart->id);
                    }

                    if (!$id_order_ps) {
                        $id_order_ps = 0;
                    }
                    $sql = "INSERT INTO `".pSQL(_DB_PREFIX_.$redsys->name) ."_transaction` (`id_customer`, `id_tpv`, `id_cart`, `id_currency`, `ds_order`, `id_order`, `ds_response`, `ds_authorisationcode`, `amount`, `transaction_date`, `transaction_type`, `id_shop`)
                        VALUES (".pSQL($cart->id_customer).",".pSQL($tpv->id).",".pSQL($cart->id).",'".pSQL($ds_currency). "',".pSQL($ds_order).",".pSQL($id_order_ps).",".pSQL($response).",'".pSQL($autorisation_code)."','".pSQL($total)."','".date('Y-m-d H:i:s')."','".pSQL($ds_transactionType)."','".pSQL($cart->id_shop)."')";
                    try {
                        Db::getInstance()->Execute($sql);
                    } catch (Exception $ex) {
                        //$logger->logDebug("error al insertar: ".$ex->getMessage());
                    }
                }
            }
        } else {
            echo "Peticion sin POST";
        }
        die;
    }

    protected function sendConfirmationEmail($id_order, $cart, $extra_vars)
    {
        $customer = new Customer($cart->id_customer);
        $order = new Order((int)$id_order);
        $currency = new Currency($cart->id_currency);

        $invoice = new Address((int)$order->id_address_invoice);
        $delivery = new Address((int)$order->id_address_delivery);
        $delivery_state = $delivery->id_state ? new State((int)$delivery->id_state) : false;
        $invoice_state = $invoice->id_state ? new State((int)$invoice->id_state) : false;
        $carrier = new Carrier($order->id_carrier);

        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $carrierNameData = ($virtual_product || !isset($carrier->name)) ? $this->trans('No carrier', array(), 'Admin.Payment.Notification') : $carrier->name;
        } else {
            $carrierNameData = ($virtual_product || !isset($carrier->name)) ? Tools::displayError('No carrier') : $carrier->name;
        }

        $product_list = $order->getProducts();

        foreach ($product_list as $product) {

            $price = Product::getPriceStatic((int)$product['product_id'], false, ($product['product_attribute_id'] ? (int)$product['product_attribute_id'] : null), 6, null, false, true, $product['product_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
            $price_wt = Product::getPriceStatic((int)$product['product_id'], true, ($product['product_attribute_id'] ? (int)$product['product_attribute_id'] : null), 2, null, false, true, $product['product_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});

            $product_price = Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt;

            $product_var_tpl = array(
                'reference' => $product['product_reference'],
                'name' => $product['product_name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : ''),
                'unit_price' => Tools::displayPrice($product_price, $currency, false),
                'price' => Tools::displayPrice($product_price * $product['product_quantity'], $currency, false),
                'quantity' => $product['product_quantity'],
                'customization' => array()
            );

            $customized_datas = Product::getAllCustomizedDatas((int)$order->id_cart);
            if (isset($customized_datas[$product['product_id']][$product['product_attribute_id']])) {
                $product_var_tpl['customization'] = array();
                foreach ($customized_datas[$product['product_id']][$product['product_attribute_id']][$order->id_address_delivery] as $customization) {
                    $customization_text = '';
                    if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD])) {
                        foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text) {
                            $customization_text .= $text['name'].': '.$text['value'].'<br />';
                        }
                    }

                    if (isset($customization['datas'][Product::CUSTOMIZE_FILE])) {
                        $customization_text .= sprintf(Tools::displayError('%d image(s)'), count($customization['datas'][Product::CUSTOMIZE_FILE])).'<br />';
                    }

                    $customization_quantity = (int)$product['customization_quantity'];

                    $product_var_tpl['customization'][] = array(
                        'customization_text' => $customization_text,
                        'customization_quantity' => $customization_quantity,
                        'quantity' => Tools::displayPrice($customization_quantity * $product_price, $currency, false)
                    );
                }
            }

            $product_var_tpl_list[] = $product_var_tpl;
            // Check if is not a virutal product for the displaying of shipping
            if (!$product['is_virtual']) {
                $virtual_product &= false;
            }
        }

        $product_list_txt = '';
        $product_list_html = '';
        if (count($product_var_tpl_list) > 0) {
            $product_list_txt = $this->getEmailTemplateContent('order_conf_product_list.txt', Mail::TYPE_TEXT, $product_var_tpl_list);
            $product_list_html = $this->getEmailTemplateContent('order_conf_product_list.tpl', Mail::TYPE_HTML, $product_var_tpl_list);
        }

        $cart_rules_order = $order->getCartRules();
        foreach ($cart_rules_order as $cart_rule) {
            $cart_rules_list[] = array(
                'voucher_name' => $cart_rule['obj']->name,
                'voucher_reduction' => ($values['tax_incl'] != 0.00 ? '-' : '').Tools::displayPrice($values['tax_incl'], $this->context->currency, false)
            );
        }

        $cart_rules_list_txt = '';
        $cart_rules_list_html = '';
        if (count($cart_rules_list) > 0) {
            $cart_rules_list_txt = $this->getEmailTemplateContent('order_conf_cart_rules.txt', Mail::TYPE_TEXT, $cart_rules_list);
            $cart_rules_list_html = $this->getEmailTemplateContent('order_conf_cart_rules.tpl', Mail::TYPE_HTML, $cart_rules_list);
        }

        $data = array(
            '{firstname}' => $customer->firstname,
            '{lastname}' => $customer->lastname,
            '{email}' => $customer->email,
            '{delivery_block_txt}' => $this->_getFormatedAddress($delivery, "\n"),
            '{invoice_block_txt}' => $this->_getFormatedAddress($invoice, "\n"),
            '{delivery_block_html}' => $this->_getFormatedAddress($delivery, '<br />', array(
                'firstname'    => '<span style="font-weight:bold;">%s</span>',
                'lastname'    => '<span style="font-weight:bold;">%s</span>'
            )),
            '{invoice_block_html}' => $this->_getFormatedAddress($invoice, '<br />', array(
                    'firstname'    => '<span style="font-weight:bold;">%s</span>',
                    'lastname'    => '<span style="font-weight:bold;">%s</span>'
            )),
            '{delivery_company}' => $delivery->company,
            '{delivery_firstname}' => $delivery->firstname,
            '{delivery_lastname}' => $delivery->lastname,
            '{delivery_address1}' => $delivery->address1,
            '{delivery_address2}' => $delivery->address2,
            '{delivery_city}' => $delivery->city,
            '{delivery_postal_code}' => $delivery->postcode,
            '{delivery_country}' => $delivery->country,
            '{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
            '{delivery_phone}' => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
            '{delivery_other}' => $delivery->other,
            '{invoice_company}' => $invoice->company,
            '{invoice_vat_number}' => $invoice->vat_number,
            '{invoice_firstname}' => $invoice->firstname,
            '{invoice_lastname}' => $invoice->lastname,
            '{invoice_address2}' => $invoice->address2,
            '{invoice_address1}' => $invoice->address1,
            '{invoice_city}' => $invoice->city,
            '{invoice_postal_code}' => $invoice->postcode,
            '{invoice_country}' => $invoice->country,
            '{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
            '{invoice_phone}' => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
            '{invoice_other}' => $invoice->other,
            '{order_name}' => $order->getUniqReference(),
            '{date}' => Tools::displayDate(date('Y-m-d H:i:s'), null, 1),
            '{carrier}' => $carrierNameData,
            '{payment}' => Tools::substr($order->payment, 0, 255),
            '{products}' => $product_list_html,
            '{products_txt}' => $product_list_txt,
            '{discounts}' => $cart_rules_list_html,
            '{discounts_txt}' => $cart_rules_list_txt,
            '{total_paid}' => Tools::displayPrice($order->total_paid, $currency, false),
            '{total_products}' => Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $order->total_products : $order->total_products_wt, $currency, false),
            '{total_discounts}' => Tools::displayPrice($order->total_discounts, $currency, false),
            '{total_shipping}' => Tools::displayPrice($order->total_shipping, $currency, false),
            '{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $currency, false),
            '{total_tax_paid}' => Tools::displayPrice(($order->total_products_wt - $order->total_products) + ($order->total_shipping_tax_incl - $order->total_shipping_tax_excl), $currency, false));

        if (is_array($extra_vars)) {
            $data = array_merge($data, $extra_vars);
        }

        // Join PDF invoice
        if ((int)Configuration::get('PS_INVOICE') && $order_status->invoice && $order->invoice_number) {
            $order_invoice_list = $order->getInvoicesCollection();
            Hook::exec('actionPDFInvoiceRender', array('order_invoice_list' => $order_invoice_list));
            $pdf = new PDF($order_invoice_list, PDF::TEMPLATE_INVOICE, $this->context->smarty);
            $file_attachement['content'] = $pdf->render(false);
            $file_attachement['name'] = Configuration::get('PS_INVOICE_PREFIX', (int)$order->id_lang, null, $order->id_shop).sprintf('%06d', $order->invoice_number).'.pdf';
            $file_attachement['mime'] = 'application/pdf';
        } else {
            $file_attachement = null;
        }

        $orderLanguage = new Language((int) $order->id_lang);

        if (Validate::isEmail($customer->email)) {
            if (version_compare(_PS_VERSION_, '1.7', '>=')) {
                Mail::Send(
                    (int)$order->id_lang,
                    'order_conf',
                    Context::getContext()->getTranslator()->trans(
                        'Order confirmation',
                        array(),
                        'Emails.Subject',
                        $orderLanguage->locale
                    ),
                    $data,
                    $customer->email,
                    $customer->firstname.' '.$customer->lastname,
                    null,
                    null,
                    $file_attachement,
                    null, _PS_MAIL_DIR_, false, (int)$order->id_shop
                );
            } else {
                Mail::Send(
                    (int)$order->id_lang,
                    'order_conf',
                    Mail::l('Order confirmation', (int)$order->id_lang),
                    $data,
                    $customer->email,
                    $customer->firstname.' '.$customer->lastname,
                    null,
                    null,
                    $file_attachement,
                    null, _PS_MAIL_DIR_, false, (int)$order->id_shop
                );
            }
        }
    }

    protected function getEmailTemplateContent($template_name, $mail_type, $var)
    {
        $email_configuration = Configuration::get('PS_MAIL_TYPE');

        if ($email_configuration != $mail_type && $email_configuration != Mail::TYPE_BOTH) {
            return '';
        }

        $theme_template_path = _PS_THEME_DIR_.'mails'.DIRECTORY_SEPARATOR.'es'.DIRECTORY_SEPARATOR.$template_name;
        $default_mail_template_path = _PS_MAIL_DIR_.'en'.DIRECTORY_SEPARATOR.$template_name;

        if (Tools::file_exists_cache($theme_template_path)) {
            $default_mail_template_path = $theme_template_path;
        }
        if (Tools::file_exists_cache($default_mail_template_path)) {
            $this->context->smarty->assign('list', $var);
            return $this->context->smarty->fetch($default_mail_template_path);
        }

        return '';
    }

    protected function _getFormatedAddress(Address $the_address, $line_sep, $fields_style = array())
    {
        return AddressFormat::generateAddress($the_address, array('avoid' => array()), $line_sep, ' ', $fields_style);
    }
}

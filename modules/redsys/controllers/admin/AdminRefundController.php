<?php
/**
* Card payment REDSYS platform (SERVIRED / SERMEPA)
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2017 idnovate
*  @license   See above
*/

class AdminRefundController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }
    public function init()
    {
        parent::init();
        if (Tools::isSubmit('method')) {
            switch (Tools::getValue('method')) {
                case 'refund_order':
                	$redsys = new Redsys();
                    $id_order_to_refund = Tools::getValue('id_order');
                    $id_tpv = Tools::getValue('id_tpv');
                    $tpv = new redsystpv($id_tpv);
                    $id_currency = Tools::getValue('id_currency');
                    $order = new Order($id_order_to_refund);
                    $refund_complete = 0;
                    if (Tools::isSubmit('partial_refund')) {
                        $amount_refund = Tools::getValue('partial_refund');
                    } else {
                        $refund_complete = 1;
                        $amount_refund = $order->total_paid;
                    }
                    $ds_order = Tools::getValue('ds_order');
                    $transactiontype = 3;
                    $amount_refund = $amount_refund * 100;
                    if ($tpv->environment_real == 0) {
                        $url_refund = 'https://sis-t.redsys.es:25443/sis/operaciones';
                    } else {
                        $url_refund = 'https://sis.redsys.es/sis/operaciones';
                    }
                    $implementation = new DOMImplementation();
                    $doc = $implementation->createDocument();
                    $doc->encoding = 'UTF-8';
                    $request = $doc->createElement('REQUEST');
                    $request = $doc->appendChild($request);
                    $datosEntrada = $doc->createElement('DATOSENTRADA');
                    $datosEntrada = $request->appendChild($datosEntrada);
                    $currency = new Currency($redsys->getIdByIsoCodeNum($id_currency, (int)Context::getContext()->shop->id));
                    /* set DATOSENTRADA */
                    $rArr = array(
                        'DS_MERCHANT_AMOUNT'            => $amount_refund,
                        'DS_MERCHANT_ORDER'             => $ds_order,
                        'DS_MERCHANT_MERCHANTCODE'      => $tpv->number,
                        'DS_MERCHANT_TERMINAL'          => $tpv->terminal,
                        'DS_MERCHANT_CURRENCY'          => $currency->iso_code_num,
                        'DS_MERCHANT_TRANSACTIONTYPE'   => $transactiontype
                    );
                    foreach ($rArr as $name => $value) {
                        $element = $doc->createElement($name, $value);
                        $datosEntrada->appendChild($element);
                    }
                    $version = "HMAC_SHA256_V1";
                    $signatureVersion = $doc->createElement('DS_SIGNATUREVERSION', $version);
                    $request->appendChild($signatureVersion);
                    $XMLDatosEntrada = $doc->saveXML();
                    $datosEntradaString = null;
                    $isFoundRequestString = preg_match('/<DATOSENTRADA.*<\/DATOSENTRADA>/', $XMLDatosEntrada, $datosEntradaString);

                    if (!$isFoundRequestString || !is_array($datosEntradaString) || (count($datosEntradaString) == 0)) {
                        return false;
                    }

                    $keyConfig = $tpv->encryption_key;
                    $base64decodedKey = base64_decode($keyConfig);
                    $keyByOrderReference = $this->encrypt3des($rArr['DS_MERCHANT_ORDER'], $base64decodedKey);
                    $signatureForRefund = hash_hmac('sha256', $datosEntradaString[0], $keyByOrderReference, true);
                    $signatureForRefund = base64_encode($signatureForRefund);

                    /* fix Redsys bug with "+" character, replacing by its hexadecimal caracter */
                    $signatureForRefund = str_replace("+", "%2B", $signatureForRefund);
                    $signature = $doc->createElement('DS_SIGNATURE', $signatureForRefund);
                    $request->appendChild($signature);

                    if (!function_exists('curl_init')) {
                        throw new Exception('CURL initialization error, not found');
                    }

                    $rd = $doc->saveXML();
                    $entrada = 'entrada=' . $rd;

                    $curlSession = curl_init();
                    curl_setopt($curlSession, CURLOPT_URL, $url_refund);
                    curl_setopt($curlSession, CURLOPT_HEADER, 1);
                    curl_setopt($curlSession, CURLOPT_POST, 1);
                    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $entrada);
                    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curlSession, CURLOPT_FRESH_CONNECT, 1);
                    curl_setopt($curlSession, CURLOPT_TIMEOUT, 30);
                    curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($curlSession, CURLOPT_SSLVERSION, 6);
                    curl_setopt($curlSession, CURLOPT_SSL_VERIFYHOST, 2);

                    $response = curl_exec($curlSession);
                    preg_match('/<codigo>(.*?)<\/codigo>/i', $response, $codigoError);
                    $codigoError = $codigoError[1];
                    $errorMessage = '';
                    $responseCode = '';
                    $ds_authorisationCode_response = '';
                    $ds_order_response = '';
                    $refund_date = date('Y-m-d H:i:s');
                    $ds_currency = '';
                    $ds_amount = '';

                    if ($codigoError == '0') {
                        preg_match('/<operacion>(.*?)<\/operacion>/i', $response, $resultado);
                        $resultado = $resultado[1];
                        preg_match('/<ds_response>(.*?)<\/ds_response>/i', $resultado, $responseCode);
                        $responseCode = $responseCode[1];
                        preg_match('/<ds_order>(.*?)<\/ds_order>/i', $resultado, $ds_order_response);
                        $ds_order_response = $ds_order_response[1];
                        preg_match('/<Ds_AuthorisationCode>(.*?)<\/Ds_AuthorisationCode>/i', $resultado, $ds_authorisationCode_response);
                        $ds_authorisationCode_response = $ds_authorisationCode_response[1];
                        preg_match('/<Ds_Currency>(.*?)<\/Ds_Currency>/i', $resultado, $ds_currency);
                        $ds_currency = $ds_currency[1];
                        preg_match('/<Ds_Amount>(.*?)<\/Ds_Amount>/i', $resultado, $ds_amount);
                        $ds_amount = $ds_amount[1]/100;
                        $redsys = new redsys();

                        if ($responseCode == '0900') {
                            $sql = "INSERT INTO `" . _DB_PREFIX_ . "redsys_refund` (`id_order`, `id_tpv`, `id_customer`, `ds_currency`, `ds_order`, `ds_response`, `ds_authorisationcode`, `amount_refunded`, `refund_date`)
                                     VALUES (".(int)$id_order_to_refund."," . (int)$tpv->id ."," . (int)$order->id_customer . ",'" . pSQL($ds_currency) . "','" . pSQL($ds_order_response) . "'," . pSQL($responseCode) . ",'" . pSQL($ds_authorisationCode_response) . "','" . pSQL($ds_amount) . "','" . pSQL($refund_date) . "')";
                            Db::getInstance()->execute($sql);

                            $errorMessage = $this->l('Refund done successfully');
                        } else {
                            $errorMessage = $this->l('Error with the refund process:').' '.$responseCode;
                        }
                    } else {
                        $errorMessage = $this->l('Error with the refund process:').' '.$codigoError;
                    }

                    if ($codigoError == "SIS0295") {
                        $errorMessage .= '. '.$this->l('Please try again later.');
                    }

                    $currency = $redsys->getIdByIsoCodeNum($ds_currency, (int)Context::getContext()->shop->id);
                    $amount_refunded_formatted = Tools::displayPrice($ds_amount, (int)$currency);
                    $return = array(
                        'id_order_to_refund' => $id_order_to_refund,
                        'amount_refunded'  => $ds_amount,
                        'amount_refunded_formatted'  => $amount_refunded_formatted,
                        'codigo_error' => $codigoError,
                        'message' => $errorMessage,
                        'response' => $responseCode,
                        'id_transaction_refund' => $ds_authorisationCode_response,
                        'order_response' => $ds_order_response,
                        'refund_complete' => $refund_complete,
                        'refund_date' => $refund_date
                    );
                    die(Tools::jsonEncode($return));
                    break;
                default:
                    throw new PrestaShopException('Unknown method "'.Tools::getValue('method').'"');
            }
        } else {
            throw new PrestaShopException('Method is not defined');
        }
    }
    public function encrypt3des($merchantOrder, $base64decodedKey)
    {
        $bytes  = array(0,0,0,0,0,0,0,0); //byte [] IV = {0, 0, 0, 0, 0, 0, 0, 0}
        $iv     = implode(array_map("chr", $bytes)); //PHP 4 >= 4.0.2
        $ciphertext = mcrypt_encrypt(MCRYPT_3DES, $base64decodedKey, $merchantOrder, MCRYPT_MODE_CBC, $iv); //PHP 4 >= 4.0.2
        return $ciphertext;
    }
}

<?php
/**
* Card payment REDSYS platform (SERVIRED / SERMEPA)
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2017 idnovate
*  @license   See above
*/

class RedsysTpv extends ObjectModel
{
    public $id;
    public $id_shop;
    public $environment_real;
    public $name;
    public $number;
    public $encryption_key;
    public $terminal;
    public $currency;
    public $payment_type;
    public $clicktopay;
    public $iupay;
    public $transaction_type;
    public $payment_size;
    public $integration;
    public $payment_text;
    public $min_amount;
    public $max_amount;
    public $carriers;
    public $countries;
    public $zones;
    public $suppliers;
    public $categories;
    public $manufacturers;
    public $languages;
    public $currencies;
    public $filter_store;
    public $payment_error;
    public $create_order;
    public $enable_translation;
    public $ssl;
    public $active = true;
    public $date_add;
    public $date_upd;
    public $advanced_payment;
    public $advanced_percentage;
    public $advanced_payment_text;
    public $advanced_payment_state;
    public $fee_discount;
    public $mode;
    public $type;
    public $order_total;
    public $fix;
    public $percentage;
    public $minimum_amount;
    public $maximum_amount;
    public $min_order_amount;
    public $max_order_amount;
    public $advanced_summary;
    public $position;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'redsys_tpv',
        'primary' => 'id_redsys_tpv',
        'multilang' => true,
        'fields' => array(
            'name' =>                       array('type' => self::TYPE_STRING, 'required' => true),
            'number' =>                     array('type' => self::TYPE_STRING, 'required' => true, 'size' => 256),
            'encryption_key' =>             array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'required' => true, 'size' => 32),
            'terminal' =>                   array('type' => self::TYPE_STRING, 'copy_post' => false),
            'environment_real' =>           array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'currency' =>                   array('type' => self::TYPE_STRING, 'required' => true, 'size' => 32),
            'transaction_type' =>           array('type' => self::TYPE_INT, 'required' => true),
            'clicktopay' =>                 array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'iupay' =>                      array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'payment_type' =>               array('type' => self::TYPE_STRING, 'size' => 1),
            'payment_size' =>               array('type' => self::TYPE_STRING, 'size' => 256),
            'payment_text' =>               array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isAnything', 'size' => 256),
            'integration' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'min_amount' =>                 array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'max_amount' =>                 array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'carriers' =>                   array('type' => self::TYPE_STRING, 'size' => 2048),
            'countries' =>                  array('type' => self::TYPE_STRING, 'size' => 2048),
            'zones' =>                      array('type' => self::TYPE_STRING, 'size' => 2048),
            'suppliers' =>                  array('type' => self::TYPE_STRING),
            'categories' =>                 array('type' => self::TYPE_STRING),
            'manufacturers' =>              array('type' => self::TYPE_STRING),
            'languages' =>                  array('type' => self::TYPE_STRING),
            'currencies' =>                 array('type' => self::TYPE_STRING),
            'filter_store' =>               array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'ssl' =>                        array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'payment_error' =>              array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'create_order' =>               array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'enable_translation' =>         array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'active' =>                     array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'id_shop' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'date_add' =>                   array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>                   array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'advanced_payment' =>           array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'advanced_percentage' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'advanced_payment_text' =>      array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isAnything', 'size' => 256),
            'advanced_payment_state' =>     array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'position' =>                   array('type' => self::TYPE_INT),
            'fee_discount' =>               array('type' => self::TYPE_INT),
            'mode' =>                       array('type' => self::TYPE_INT),
            'type' =>                       array('type' => self::TYPE_INT),
            'order_total' =>                array('type' => self::TYPE_INT),
            'fix' =>                        array('type' => self::TYPE_FLOAT),
            'percentage' =>                 array('type' => self::TYPE_FLOAT),
            'minimum_amount' =>             array('type' => self::TYPE_FLOAT),
            'maximum_amount' =>             array('type' => self::TYPE_FLOAT),
            'min_order_amount' =>           array('type' => self::TYPE_FLOAT),
            'max_order_amount' =>           array('type' => self::TYPE_FLOAT),
            'advanced_summary' =>           array('type' => self::TYPE_INT),
        ),
    );

    public function __construct($id = null, $id_lang = null)
    {
        parent::__construct($id, $id_lang);
        $this->image_dir = _PS_IMG_DIR_.'redsys';
    }

    public function add($autodate = true, $null_values = true)
    {
        $this->id_shop = ($this->id_shop) ? $this->id_shop : Context::getContext()->shop->id;

        $success = parent::add($autodate, $null_values);
        return $success;
    }

    public function toggleStatus()
    {
        parent::toggleStatus();

        return Db::getInstance()->execute('
        UPDATE `'._DB_PREFIX_.bqSQL($this->def['table']).'`
        SET `date_upd` = NOW()
        WHERE `'.bqSQL($this->def['primary']).'` = '.(int)$this->id);
    }

    public function delete()
    {
        if (parent::delete()) {
            return $this->deleteImage();
        }
    }

    public static function getTpvs($id_shop = 0, $id_lang = false, $carrier = false, $country = false, $zone = false, $suppliers = false, $manufacturers = false, $products = false, $id_currency = 1)
    {
        $id_shop = Context::getContext()->shop->id;

        if (!$id_lang) {
            $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        }

        $array_tpvs_result = array();

        $query = 'SELECT tpv.*, tpvlang.`payment_text`
            FROM `'._DB_PREFIX_.'redsys_tpv` tpv
            '.Shop::addSqlAssociation('redsys_tpv', 'tpv').'
            LEFT JOIN `'._DB_PREFIX_.'redsys_tpv_lang` tpvlang ON (tpv.`id_redsys_tpv` = tpvlang.`id_redsys_tpv` AND tpvlang.`id_lang` = '.(int)$id_lang.')
            WHERE tpv.`active` = 1 ';
        $query .= $id_shop ? 'AND tpv.`id_shop` = '.(int)$id_shop : '';

        $query .= ' ORDER BY position';

        $tpvs = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        if ($tpvs === false) {
            return false;
        }

        foreach ($tpvs as $tpv) {
            if ($tpv['currencies'] == 'all') {
                $tpv['currencies'] = '';
            }
            if ($tpv['languages'] == 'all') {
                $tpv['languages'] = '';
            }
            if ($tpv['carriers'] == 'all') {
                $tpv['carriers'] = '';
            }
            if ($tpv['countries'] == 'all') {
                $tpv['countries'] = '';
            }
            if ($tpv['zones'] == 'all') {
                $tpv['zones'] = '';
            }
            if ($tpv['suppliers'] == 'all') {
                $tpv['suppliers'] = '';
            }
            if ($tpv['categories'] == 'all') {
                $tpv['categories'] = '';
            }
            if ($tpv['manufacturers'] == 'all') {
                $tpv['manufacturers'] = '';
            }

            if ($id_shop == 0 || ($tpv['carriers'] == '' && $tpv['countries'] == '' && $tpv['zones'] == '' && $tpv['suppliers'] == '' && $tpv['currencies'] == '' && $tpv['languages'] == '' && $tpv['categories'] == '' && $tpv['manufacturers'] == '')) {
                $array_tpvs_result[] = $tpv;
                continue;
            }

            $filter_currencies = true;
            if ($tpv['currencies'] !== '') {
                $currencies_array = explode(';', $tpv['currencies']);
                if (!in_array($id_currency, $currencies_array)) {
                    $filter_currencies = false;
                }
            }
            $filter_languages = true;
            if ($tpv['languages'] !== '') {
                $languages_array = explode(';', $tpv['languages']);
                if (!in_array($id_lang, $languages_array)) {
                    $filter_languages = false;
                }
            }

            $filter_carriers = true;
            if ($tpv['carriers'] !== '') {
                $carriers_array = explode(';', $tpv['carriers']);
                if (!in_array($carrier, $carriers_array)) {
                    $filter_carriers = false;
                    continue;
                }
            }
            $filter_countries = true;
            if ($tpv['countries'] !== '') {
                $countries_array = explode(';', $tpv['countries']);
                if (!in_array($country->id, $countries_array)) {
                    $filter_countries = false;
                    continue;
                }
            }
            $filter_zones = true;
            if ($tpv['zones'] !== '') {
                $zones_array = explode(';', $tpv['zones']);
                if (!in_array($zone, $zones_array)) {
                    $filter_zones = false;
                    continue;
                }
            }
            $filter_categories = true;
            if ($tpv['categories'] !== '') {
                if (@unserialize($tpv['categories']) !== false) {
                    $categories_array = unserialize($tpv['categories']);
                } else {
                    $categories_array = explode(';', $tpv['categories']);
                }
                foreach ($products as $product) {
                    $categories = Product::getProductCategories($product['id_product']);
                    foreach ($categories as $category) {
                        if (in_array($category, $categories_array)) {
                            $filter_categories = true;
                            break;
                        } else {
                            $filter_categories = false;
                        }
                    }
                }
            }
            $filter_manufacturers = true;
            if ($tpv['manufacturers'] !== '') {
                $manufacturers_array = explode(';', $tpv['manufacturers']);
                foreach ($manufacturers as $manufacturer) {
                    if (!in_array($manufacturer, $manufacturers_array)) {
                        $filter_manufacturers = false;
                        break;
                    }
                }
                if (!$filter_manufacturers) {
                    continue;
                }
            } else {
                $filter_manufacturers = true;
            }

            $filter_suppliers = true;
            if ($tpv['suppliers'] !== '') {
                $suppliers_array = explode(';', $tpv['suppliers']);
                foreach ($suppliers as $supplier) {
                    if (!in_array($supplier, $suppliers_array)) {
                        $filter_suppliers = false;
                        break;
                    }
                }
                if (!$filter_suppliers) {
                    continue;
                }
            } else {
                $filter_suppliers = true;
            }

            if ($filter_carriers && $filter_countries && $filter_zones && $filter_suppliers && $filter_currencies && $filter_languages && $filter_categories && $filter_manufacturers) {
                $array_tpvs_result[] = $tpv;
            }
        }

        if (count($array_tpvs_result) > 0) {
            return $array_tpvs_result;
        } else {
            return false;
        }
    }

/*        private function _updatePositions($positions)
    {
        foreach ($positions as $key => $position) {
            $pos = explode('_', $position);
            Db::getInstance()->execute('
                UPDATE `'.pSQL(_DB_PREFIX_.$this->module_name).'_tpv`
                SET `position` = '.(int)$key.'
                WHERE `id_redsys_tpv` = '.(int)$pos[2]);
        }
        return true;
    }
*/
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS(
            'SELECT `id_redsys_tpv`, `position`
            FROM `'._DB_PREFIX_.'redsys_tpv`
            ORDER BY `position` ASC'
        )) {
            return false;
        }

        foreach ($res as $tpv) {
            if ((int)$tpv['id_redsys_tpv'] == (int)$this->id) {
                $moved_tpv = $tpv;
            }
        }

        if (!isset($moved_tpv) || !isset($moved_tpv)) {
            return false;
        }

        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        return (Db::getInstance()->execute('
            UPDATE `'._DB_PREFIX_.'redsys_tpv`
            SET `position`= `position` '.($way ? '- 1' : '+ 1').'
            WHERE `position`
            '.($way
                ? '> '.(int)$moved_tpv['position'].' AND `position` <= '.(int)$position
                : '< '.(int)$moved_tpv['position'].' AND `position` >= '.(int)$position))
        && Db::getInstance()->execute('
            UPDATE `'._DB_PREFIX_.'redsys_tpv`
            SET `position` = '.(int)$position.'
            WHERE `id_redsys_tpv` = '.(int)$moved_tpv['id_redsys_tpv']));
    }

    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'redsys_tpv`';

        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
}

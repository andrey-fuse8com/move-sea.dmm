{**
* Card payment REDSYS platform (SERVIRED / SERMEPA)
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2017 idnovate
*  @license   See above
*}

<script type="text/javascript">
	var selectAll = "{l s='Select all' mod='redsys' js=1}";
	var deselectAll = "{l s='Deselect all' mod='redsys' js=1}";
	var selected = "{l s='selected' mod='redsys' js=1}";
	var itemsSelected_nil = "{l s='No options selected (rule applied to all values)' mod='redsys' js=1}";
    var itemsSelected = "{l s='selected option (rule applied only to this value)' mod='redsys' js=1}";
    var itemsSelected_plural = "{l s='options selected (rule applied only to these values)' mod='redsys' js=1}";

	var itemsAvailable_nil = "{l s='No items available' mod='redsys' js=1}";
	var itemsAvailable = "{l s='options available' mod='redsys' js=1}";
	var itemsAvailable_plural = "{l s='options available' mod='redsys' js=1}";

	var itemsFiltered_nil = "{l s='No options found' mod='redsys' js=1}";
	var itemsFiltered = "{l s='option found' mod='redsys' js=1}";
	var itemsFiltered_plural = "{l s='options found' mod='redsys' js=1}";

	var searchOptions = "{l s='Search Options' mod='redsys' js=1}";
	var collapseGroup = "{l s='Collapse Group' mod='redsys' js=1}";
	var expandGroup = "{l s='Expand Group' mod='redsys' js=1}";
	var searchAllGroup = "{l s='Select All Group' mod='redsys' js=1}";
	var deselectAllGroup = "{l s='Deselect All Group' mod='redsys' js=1}";
</script>


<script src="../modules/redsys/views/js/plugin/ui.multiselect.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".multiple_select").each(function() {
			$(this).multiselect();
		});
		toggleFields('filter_store');
	});
</script>
/**
* 2005-2017 Magic Toolbox
*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Magic Toolbox <support@magictoolbox.com>
*  @copyright Copyright (c) 2017 Magic Toolbox <support@magictoolbox.com>. All rights reserved
*  @license   https://www.magictoolbox.com/license/
*/


var magictoolboxImagesOrder;
updateMagicSlideshowIntervalID = null;

window['refreshProductImagesOriginal'] = window['refreshProductImages'];
window['refreshProductImages'] = function(id_product_attribute) {

    id_product_attribute = parseInt(id_product_attribute);

    //NOTE: to avoid double restart
    if (typeof(arguments.callee.last_id_product_attribute) !== 'undefined' && (arguments.callee.last_id_product_attribute == id_product_attribute)) {
        var r = window['refreshProductImagesOriginal'].apply(window, arguments);
        return r;
    }
    arguments.callee.last_id_product_attribute = id_product_attribute;

    //NOTE: does not scroll until tool is ready

    //NOTE: clears a timer
    if (updateMagicSlideshowIntervalID != null) {
        clearInterval(updateMagicSlideshowIntervalID);
        updateMagicSlideshowIntervalID = null;
    }
    //NOTE: set a timer
    mtIntervals = isProductMagicSlideshowReady ? 0 : 500;
    updateMagicSlideshowIntervalID = setInterval(function() {
        if (isProductMagicSlideshowReady) {
            clearInterval(updateMagicSlideshowIntervalID);
            updateMagicSlideshowIntervalID = null;

            var idCombination = $('#idCombination').val();
            for (var i in combinations) {
                if (combinations[i]['idCombination'] == idCombination) {
                    var position = jQuery.inArray(combinations[i]['image'], magictoolboxImagesOrder);
                    MagicSlideshow.jump('productMagicSlideshow', position+1);
                    //DEPRECATED:
                    //$('#bigpic').attr('src', $('#productMagicSlideshow .mss-slide-wrapper img').get(position).src);
                    break;
                }
            }
        }
    }, mtIntervals);

    return window['refreshProductImagesOriginal'].apply(window, arguments);

}


//NOTE: unbind click to disable fancybox
$(document).ready(function() {
    //NOTE: .off() was added in version 1.7
    if ($(document).off) {
        $(document).off('click', '#view_full_size, #image-block');
        $(document).off('click', '#image-block');
    }
    $('#image-block img').unbind('click');
});

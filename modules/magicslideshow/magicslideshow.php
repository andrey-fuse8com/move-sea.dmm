<?php
/**
* 2005-2017 Magic Toolbox
*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Magic Toolbox <support@magictoolbox.com>
*  @copyright Copyright (c) 2017 Magic Toolbox <support@magictoolbox.com>. All rights reserved
*  @license   https://www.magictoolbox.com/license/
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if (!isset($GLOBALS['magictoolbox'])) {
    $GLOBALS['magictoolbox'] = array();
    $GLOBALS['magictoolbox']['filters'] = array();
    $GLOBALS['magictoolbox']['isProductScriptIncluded'] = false;
    $GLOBALS['magictoolbox']['standardTool'] = '';
    $GLOBALS['magictoolbox']['selectorImageType'] = '';
    $GLOBALS['magictoolbox']['isProductBlockProcessed'] = false;
}

if (!isset($GLOBALS['magictoolbox']['magicslideshow'])) {
    $GLOBALS['magictoolbox']['magicslideshow'] = array();
    $GLOBALS['magictoolbox']['magicslideshow']['headers'] = false;
    $GLOBALS['magictoolbox']['magicslideshow']['scripts'] = '';
}

class MagicSlideshow extends Module
{

    /* PrestaShop v1.5 or above */
    public $isPrestaShop15x = false;

    /* PrestaShop v1.5.5.0 or above */
    public $isPrestaShop155x = false;

    /* PrestaShop v1.6 or above */
    public $isPrestaShop16x = false;

    /* PrestaShop v1.7 or above */
    public $isPrestaShop17x = false;

    /* Smarty v3 template engine */
    public $isSmarty3 = false;

    /* Smarty 'getTemplateVars' function name */
    public $getTemplateVars = 'getTemplateVars';

    /* Suffix was added to default images types since version 1.5.1.0 */
    public $imageTypeSuffix = '';

    /* To display 'product.js' file inline */
    public $displayInlineProductJs = false;

    /* Ajax request flag */
    public $isAjaxRequest = false;

    /* Featured Products module name */
    public $featuredProductsModule = 'homefeatured';

    /* Top-sellers block module name */
    public $topSellersModule = 'blockbestsellers';

    /* New Products module name */
    public $newProductsModule = 'blocknewproducts';

    /* Specials Products module name */
    public $specialsProductsModule = 'blockspecials';

    /* NOTE: identifying PrestaShop version class */
    public $psVersionClass = 'mt-ps-old';

    public function __construct()
    {
        $this->name = 'magicslideshow';
        $this->tab = 'Tools';
        $this->version = '5.9.19';
        $this->author = 'Magic Toolbox';

        $this->module_key = '5da7a3e13aa160e9536054d328f24f3d';


        //NOTE: to link bootstrap css for settings page in v1.6
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = 'Magic Slideshow';
        $this->description = "Display one image after another. Fade or slide, fast or slow, text or just images. It's your choice!";

        $this->confirmUninstall = 'All magicslideshow settings would be deleted. Do you really want to uninstall this module ?';

        $this->isPrestaShop15x = version_compare(_PS_VERSION_, '1.5', '>=');
        $this->isPrestaShop155x = version_compare(_PS_VERSION_, '1.5.5', '>=');
        $this->isPrestaShop16x = version_compare(_PS_VERSION_, '1.6', '>=');
        $this->isPrestaShop17x = version_compare(_PS_VERSION_, '1.7', '>=');

        $this->displayInlineProductJs = version_compare(_PS_VERSION_, '1.6.0.3', '>=') && version_compare(_PS_VERSION_, '1.6.0.7', '<');

        if ($this->isPrestaShop16x) {
            $this->tab = 'others';
        }

        $this->isSmarty3 = $this->isPrestaShop15x || Configuration::get('PS_FORCE_SMARTY_2') === '0';
        if ($this->isSmarty3) {
            //Smarty v3 template engine
            $this->getTemplateVars = 'getTemplateVars';
        } else {
            //Smarty v2 template engine
            $this->getTemplateVars = 'get_template_vars';
        }

        $this->imageTypeSuffix = version_compare(_PS_VERSION_, '1.5.1.0', '>=') ? '_default' : '';

        $this->isAjaxRequest = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest');

        if ($this->isPrestaShop17x) {
            $this->featuredProductsModule = 'ps_featuredproducts';
            $this->topSellersModule = 'ps_bestsellers';
            $this->newProductsModule = 'ps_newproducts';
            $this->specialsProductsModule = 'ps_specials';
        }

        if (version_compare(_PS_VERSION_, '1.4.5.1', '>=')) {
            $this->psVersionClass = 'mt-ps-1451x';
            if ($this->isPrestaShop15x) {
                $this->psVersionClass = 'mt-ps-15x';
                if ($this->isPrestaShop16x) {
                    $this->psVersionClass = 'mt-ps-16x';
                    if ($this->isPrestaShop17x) {
                        $this->psVersionClass = 'mt-ps-17x';
                    }
                }
            }
        }
    }

    protected function _generateConfigXml($need_instance = 1)
    {
        //NOTE: this fix an issue with description in PrestaShop 1.4
        $description = htmlentities($this->description, ENT_COMPAT, 'UTF-8');
        $this->description = htmlentities($description);
        return parent::_generateConfigXml($need_instance);
    }

    public function install()
    {
        if ($this->isPrestaShop15x) {
            if ($this->isPrestaShop16x) {
                if ($this->isPrestaShop17x) {
                    $homeHookID = Hook::getIdByName('displayHome');
                    $homeHookName = 'displayHome';
                } else {
                    $homeHookID = Hook::getIdByName('displayTopColumn');
                    $homeHookName = 'displayTopColumn';
                }
            } else {
                $homeHookID = Hook::getIdByName('displayHome');
                $homeHookName = 'displayHome';
            }
        } else {
            $homeHookID = Hook::get('home');
            $homeHookName = 'home';
        }
        $headerHookID = $this->isPrestaShop15x ? Hook::getIdByName('displayHeader') : Hook::get('header');

        if (!parent::install()
            || !$this->registerHook($this->isPrestaShop15x ? 'displayHeader' : 'header')
            || $this->isPrestaShop17x && !$this->registerHook('actionDispatcher')
            || !$this->registerHook($this->isPrestaShop15x ? 'displayFooterProduct' : 'productFooter')
            || !$this->registerHook($this->isPrestaShop15x ? 'displayFooter' : 'footer')
            || !$this->installDB()
            || !$this->fixCSS()
            || !$this->registerHook($homeHookName)
            //NOTICE: this function can return false if the module is the only one in this position
            || !($this->updatePosition($homeHookID, 0, 1) || true)
            || !$this->createImageFolder('magicslideshow')
            //NOTICE: this function can return false if the module is the only one in this position
            || !($this->updatePosition($headerHookID, 0, 1) || true)
            /**/) {
            return false;
        }

        return true;
    }

    private function createImageFolder($imageFolderName)
    {
        if (!is_dir(_PS_IMG_DIR_.$imageFolderName)) {
            if (!mkdir(_PS_IMG_DIR_.$imageFolderName, 0755)) {
                return false;
            }
        }
        return true;
    }

    private function installDB()
    {
        if (!Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'magicslideshow_settings` (
                                        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                                        `block` VARCHAR(32) NOT NULL,
                                        `name` VARCHAR(32) NOT NULL,
                                        `value` TEXT,
                                        `default_value` TEXT,
                                        `enabled` TINYINT(1) UNSIGNED NOT NULL,
                                        `default_enabled` TINYINT(1) UNSIGNED NOT NULL,
                                        PRIMARY KEY (`id`)
                                        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;')
            || !$this->fillDB()
            || !$this->fixDefaultValues()
            || !Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'magicslideshow_images` (
                                            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                                            `order` INT UNSIGNED DEFAULT 0,
                                            `name` VARCHAR(64) NOT NULL DEFAULT \'\',
                                            `ext` VARCHAR(16) NOT NULL DEFAULT \'\',
                                            `title` VARCHAR(64) NOT NULL DEFAULT \'\',
                                            `description` TEXT,
                                            `link` VARCHAR(256) NOT NULL DEFAULT \'\',
                                            `lang` INT(10) UNSIGNED DEFAULT 0,
                                            `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
                                            PRIMARY KEY (`id`)
                                            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;')
        /**/) {
            return false;
        }

        return true;
    }

    private function fixCSS()
    {
        //fix url's in css files
        $path = dirname(__FILE__);
        $list = glob($path.'/*');
        $files = array();
        if (is_array($list)) {
            $listLength = count($list);
            for ($i = 0; $i < $listLength; $i++) {
                if (is_dir($list[$i])) {
                    if (!in_array(basename($list[$i]), array('.svn', '.git'))) {
                        $add = glob($list[$i].'/*');
                        if (is_array($add)) {
                            $list = array_merge($list, $add);
                            $listLength += count($add);
                        }
                    }
                } elseif (preg_match('#\.css$#i', $list[$i])) {
                    $files[] = $list[$i];
                }
            }
        }
        foreach ($files as $file) {
            $cssPath = dirname($file);
            $cssRelPath = str_replace($path, '', $cssPath);
            $toolPath = _MODULE_DIR_.'magicslideshow'.$cssRelPath;
            $pattern = '#url\(\s*(\'|")?(?!data:|mhtml:|http(?:s)?:|/)([^\)\s\'"]+?)(?(1)\1)\s*\)#is';
            $replace = 'url($1'.$toolPath.'/$2$1)';
            $fileContents = Tools::file_get_contents($file);
            $fixedFileContents = preg_replace($pattern, $replace, $fileContents);
            //preg_match_all($pattern, $fileContents, $matches, PREG_SET_ORDER);
            //debug_log($matches);
            if ($fixedFileContents != $fileContents) {
                $fp = fopen($file, 'w+');
                if ($fp) {
                    fwrite($fp, $fixedFileContents);
                    fclose($fp);
                }
            }
        }

        return true;
    }


    public function fixDefaultValues()
    {
        $result = true;
        if (version_compare(_PS_VERSION_, '1.5.1.0', '>=')) {
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `value`=CONCAT(value, \'_default\'), `default_value`=CONCAT(default_value, \'_default\') WHERE (`name`=\'thumb-image\' OR `name`=\'selector-image\' OR `name`=\'large-image\') AND `value`!=\'original\'';
            $result = Db::getInstance()->Execute($sql);
        }
        if ($this->isPrestaShop16x) {
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `value`=\'Yes\', `default_value`=\'Yes\', `enabled`=1 WHERE `name`=\'arrows\' AND (`block`=\'product\' OR `block`=\'homefeatured\' OR `block`=\'blocknewproducts_home\' OR `block`=\'blockbestsellers_home\' OR `block`=\'blockspecials_home\')';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `value`=\'60%\', `default_value`=\'60%\', `enabled`=1 WHERE `name`=\'height\' AND (`block`=\'homefeatured\' OR `block`=\'blocknewproducts_home\' OR `block`=\'blockbestsellers_home\' OR `block`=\'blockspecials_home\')';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `value`=\'thickbox_default\', `default_value`=\'thickbox_default\', `enabled`=1 WHERE `name`=\'thumb-image\' AND (`block`=\'homefeatured\' OR `block`=\'blocknewproducts_home\' OR `block`=\'blockbestsellers_home\' OR `block`=\'blockspecials_home\')';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `value`=\'8\', `default_value`=\'8\', `enabled`=1 WHERE `name`=\'max-number-of-products\' AND (`block`=\'blockbestsellers\' OR `block`=\'blockbestsellers_home\')';
            $result = Db::getInstance()->Execute($sql);
        }
        if ($this->isPrestaShop17x) {
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `enabled`=1, `value`=\'large_default\', `default_value`=\'large_default\' WHERE `name`=\'large-image\'';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `enabled`=1, `value`=\'medium_default\', `default_value`=\'medium_default\' WHERE `name`=\'thumb-image\' AND `block`=\'product\'';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `enabled`=1, `value`=\'home_default\', `default_value`=\'home_default\' WHERE `name`=\'thumb-image\' AND (`block`=\'category\' OR `block`=\'manufacturer\' OR `block`=\'newproductpage\' OR `block`=\'bestsellerspage\' OR `block`=\'specialspage\')';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `enabled`=1, `value`=\'home_default\', `default_value`=\'home_default\' WHERE `name`=\'thumb-image\' AND (`block`=\'blocknewproducts_home\' OR `block`=\'blockbestsellers_home\' OR `block`=\'blockspecials_home\' OR `block`=\'homefeatured\')';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `enabled`=1, `value`=\'small_default\', `default_value`=\'small_default\' WHERE `name`=\'thumb-image\' AND (`block`=\'blocknewproducts\' OR `block`=\'blockbestsellers\' OR `block`=\'blockspecials\' OR `block`=\'blockviewed\')';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `enabled`=1, `value`=\'large_default\', `default_value`=\'large_default\' WHERE `name`=\'thumb-image\' AND `block`=\'homefeatured\'';
            $result = Db::getInstance()->Execute($sql);
            $sql = 'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `enabled`=1, `value`=\'Yes\', `default_value`=\'Yes\' WHERE `name`=\'caption\' AND `block`=\'homefeatured\'';
            $result = Db::getInstance()->Execute($sql);
        }
        return $result;
    }

    public function uninstall()
    {
        if (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
            $this->_clearCache('*');
        }
        if (!parent::uninstall() || !$this->uninstallDB()) {
            return false;
        }
        return true;
    }

    private function uninstallDB()
    {
        return Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'magicslideshow_settings`;');
    }

    public function disable($forceAll = false)
    {
        if (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
            $this->_clearCache('*');
        }
        return parent::disable($forceAll);
    }

    public function enable($forceAll = false)
    {
        if (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
            $this->_clearCache('*');
        }
        return parent::enable($forceAll);
    }

    public function _clearCache($template, $cache_id = null, $compile_id = null)
    {
        if ($this->isPrestaShop17x) {
            $this->name = 'ps_featuredproducts';
            parent::_clearCache('ps_featuredproducts.tpl', 'ps_featuredproducts');
            parent::_clearCache('module:ps_featuredproducts/views/templates/hook/ps_featuredproducts.tpl');

            $this->name = 'ps_bestsellers';
            parent::_clearCache('module:ps_bestsellers/views/templates/hook/ps_bestsellers.tpl');

            $this->name = 'ps_newproducts';
            parent::_clearCache('module:ps_newproducts/views/templates/hook/ps_newproducts.tpl');

            $this->name = 'ps_specials';
            parent::_clearCache('module:ps_specials/views/templates/hook/ps_specials.tpl');

            $this->name = 'magicslideshow';
            return;
        }

        $this->name = 'homefeatured';//NOTE: spike to clear cache for 'homefeatured.tpl'
        parent::_clearCache('homefeatured.tpl');
        parent::_clearCache('tab.tpl', 'homefeatured-tab');

        $this->name = 'blockbestsellers';
        parent::_clearCache('blockbestsellers.tpl');
        parent::_clearCache('blockbestsellers-home.tpl', 'blockbestsellers-home');
        parent::_clearCache('blockbestsellers.tpl', 'blockbestsellers_col');
        parent::_clearCache('tab.tpl', 'blockbestsellers-tab');

        $this->name = 'blocknewproducts';
        parent::_clearCache('blocknewproducts.tpl');
        parent::_clearCache('blocknewproducts_home.tpl', 'blocknewproducts-home');
        parent::_clearCache('tab.tpl', 'blocknewproducts-tab');

        $this->name = 'blockspecials';
        parent::_clearCache('blockspecials.tpl');
        parent::_clearCache('blockspecials-home.tpl', 'blockspecials-home');
        parent::_clearCache('tab.tpl', 'blockspecials-tab');

        $this->name = 'blockspecials';
        parent::_clearCache('blockspecials.tpl');

        $this->name = 'magicslideshow';
    }

    public function getImagesTypes()
    {
        if (!isset($GLOBALS['magictoolbox']['imagesTypes'])) {
            $GLOBALS['magictoolbox']['imagesTypes'] = array('original');
            //NOTE: get image type values
            $sql = 'SELECT name FROM `'._DB_PREFIX_.'image_type` ORDER BY `id_image_type` ASC';
            $result = Db::getInstance()->ExecuteS($sql);
            foreach ($result as $row) {
                $GLOBALS['magictoolbox']['imagesTypes'][] = $row['name'];
            }
        }
        return $GLOBALS['magictoolbox']['imagesTypes'];
    }

    public function getContent()
    {
        if ($this->needUpdateDb()) {
            Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'magicslideshow_settings`');
            $this->fillDB();
            $this->fixDefaultValues();
        }

        $action = Tools::getValue('magicslideshow-submit-action', false);
        $activeTab = Tools::getValue('magicslideshow-active-tab', false);

        if ($action == 'reset' && $activeTab) {
            Db::getInstance()->Execute(
                'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `value`=`default_value`, `enabled`=`default_enabled` WHERE `block`=\''.pSQL($activeTab).'\''
            );
        }

        $tool = $this->loadTool();
        $paramsMap = $this->getParamsMap();

        $_imagesTypes = array(
            'selector',
            'large',
            'thumb'
        );

        foreach ($_imagesTypes as $name) {
            foreach ($this->getBlocks() as $blockId => $blockLabel) {
                if ($tool->params->paramExists($name.'-image', $blockId)) {
                    $tool->params->setValues($name.'-image', $this->getImagesTypes(), $blockId);
                }
            }
        }

        $paramData = $tool->params->getParam('enable-effect', 'homeslideshow');
        $paramData['label'] = 'Show slideshow on home page';
        $paramData['description'] = '<h2>Slideshow shortcodes</h2>'.
            'In order to show slideshow on any CMS page just insert slideshow shortcode <b>[magicslideshow]</b>.<br />'.
            'If you want to show slideshow with specific images only, please use shortcode <b>[magicslideshow id=1,2,5]</b> where 1, 2 and 5 are numbers of images from the ID column.';

        $tool->params->appendParams(array('enable-effect' => $paramData), 'homeslideshow');


        //debug_log($_GET);
        //debug_log($_POST);

        $params = Tools::getValue('magicslideshow', false);

        //NOTE: save settings
        if ($action == 'save' && $params) {
            foreach ($paramsMap as $blockId => $groups) {
                foreach ($groups as $group) {
                    foreach ($group as $param => $required) {
                        if (isset($params[$blockId][$param])) {
                            $valueToSave = $value = trim($params[$blockId][$param]);
                            switch ($tool->params->getType($param)) {
                                case 'num':
                                    $valueToSave = $value = (int)$value;
                                    break;
                                case 'array':
                                    if (!in_array($value, $tool->params->getValues($param))) {
                                        $valueToSave = $value = $tool->params->getDefaultValue($param);
                                    }
                                    $valueToSave = pSQL($valueToSave);
                                    break;
                                case 'text':
                                    $valueToSave = $value = str_replace('"', '&quot;', $value);//NOTE: fixed issue with "
                                    $valueToSave = pSQL($value);
                                    break;
                            }
                            Db::getInstance()->Execute(
                                'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `value`=\''.$valueToSave.'\', `enabled`=1 WHERE `block`=\''.pSQL($blockId).'\' AND `name`=\''.pSQL($param).'\''
                            );
                            $tool->params->setValue($param, $value, $blockId);
                        } else {
                            Db::getInstance()->Execute(
                                'UPDATE `'._DB_PREFIX_.'magicslideshow_settings` SET `enabled`=0 WHERE `block`=\''.pSQL($blockId).'\' AND `name`=\''.pSQL($param).'\''
                            );
                            if ($tool->params->paramExists($param, $blockId)) {
                                $tool->params->removeParam($param, $blockId);
                            }
                        }
                    }
                }
            }
            if (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
                $this->_clearCache('*');
            }
        }

        $imageFilePath = _PS_IMG_DIR_.'magicslideshow/';
        $imagesTypes = ImageType::getImagesTypes();

        //NOTE: upload images
        if ($action == 'upload' && isset($_FILES['magicslideshow-image-files']['tmp_name'])
                                && is_array($_FILES['magicslideshow-image-files']['tmp_name'])
                                && count($_FILES['magicslideshow-image-files']['tmp_name'])) {
            $errors = array();
            $imageResizeMethod = 'imageResize';
            //NOTE: __autoload function in Prestashop 1.3.x leads to PHP fatal error because ImageManager class does not exists
            //      can not use class_exists('ImageManager', false) because ImageManager class will not load where it is needed
            //      so check version before
            if ($this->isPrestaShop15x && class_exists('ImageManager') && method_exists('ImageManager', 'resize')) {
                $imageResizeMethod = array('ImageManager', 'resize');
            }

            foreach ($_FILES['magicslideshow-image-files']['tmp_name'] as $key => $tempName) {
                if (!empty($tempName) && file_exists($tempName)) {
                    $tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                    if (!$tmpName || !move_uploaded_file($tempName, $tmpName)) {
                        $errors[] = 'An error occurred during the image upload.';
                    } else {
                        preg_match('/^(.*?)\.([^\.]*)$/is', $_FILES['magicslideshow-image-files']['name'][$key], $matches);
                        list(, $imageFileName, $imageFileExt) = $matches;
                        $imageSuffix = 0;
                        while (file_exists($imageFilePath.$imageFileName.($imageSuffix?'-'.$imageSuffix:'').'.'.$imageFileExt)) {
                            $imageSuffix++;
                        }
                        $imageFileName = $imageFileName.($imageSuffix?'-'.$imageSuffix:'');
                        if (!call_user_func($imageResizeMethod, $tmpName, $imageFilePath.$imageFileName.'.'.$imageFileExt, null, null, $imageFileExt)) {
                            $errors[] = 'An error occurred while copying image.';
                        } else {
                            foreach ($imagesTypes as $k => $imageType) {
                                if (!call_user_func($imageResizeMethod, $tmpName, $imageFilePath.$imageFileName.'-'.Tools::stripslashes($imageType['name']).'.'.$imageFileExt, $imageType['width'], $imageType['height'], $imageFileExt)) {
                                    $errors[] = 'An error occurred while copying resized image ('.Tools::stripslashes($imageType['name']).').';
                                }
                            }
                        }
                    }
                    @unlink($tmpName);
                    Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'magicslideshow_images` (`name`, `ext`, `title`, `description`, `link`, `lang`, `enabled`, `order`) VALUES (\''.pSQL($imageFileName).'\', \''.pSQL($imageFileExt).'\', \'\', \'\', \'\', 0, 1, 0)');
                    Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'magicslideshow_images` SET `order`=LAST_INSERT_ID() WHERE `id`=LAST_INSERT_ID()');
                }
            }
        }

        $imagesUpdateData = Tools::getValue('images-update-data', false);
        if (!$imagesUpdateData) {
            $imagesUpdateData = array();
        }
        // save images data
        if ($action == 'save' && !empty($imagesUpdateData)) {
            foreach ($imagesUpdateData as $imageId => $imageData) {
                if ((int)$imageData['delete']) {
                    $sql = 'SELECT `name`, `ext` FROM `'._DB_PREFIX_.'magicslideshow_images` WHERE `id`='.(int)$imageId;
                    $result = Db::getInstance()->ExecuteS($sql);
                    $result = $result[0];
                    foreach ($imagesTypes as $k => $imageType) {
                        @unlink($imageFilePath.$result['name'].'-'.Tools::stripslashes($imageType['name']).'.'.$result['ext']);
                    }

                    @unlink($imageFilePath.$result['name'].'.'.$result['ext']);
                    Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'magicslideshow_images` WHERE `id`='.(int)$imageId);
                } else {
                    Db::getInstance()->Execute(
                        'UPDATE `'._DB_PREFIX_.'magicslideshow_images` SET '.
                        '`order`='.(int)$imageData['order'].
                        ', `title`=\''.pSQL(htmlspecialchars($imageData['title'])).'\''.
                        ', `description`=\''.pSQL(htmlspecialchars($imageData['description'])).'\''.
                        ', `link`=\''.pSQL($imageData['link']).'\''.
                        ', `lang`=\''.pSQL($imageData['lang']).'\''.
                        ', `enabled`='.(isset($imageData['exclude']) ? '0' : '1').
                        ' WHERE `id`='.(int)$imageId
                    );
                }
            }
        }

        include(dirname(__FILE__).'/admin/magictoolbox.settings.editor.class.php');
        $settings = new MagictoolboxSettingsEditorClass(dirname(__FILE__).DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'js');
        $settings->paramsMap = $this->getParamsMap();
        $settings->core = $this->loadTool();
        $settings->profiles = $this->getBlocks();
        $settings->pathToJS = dirname(__FILE__).DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'js';
        $settings->action = htmlentities($_SERVER['REQUEST_URI']);
        $settings->setResourcesURL(_MODULE_DIR_.'magicslideshow/admin/resources/');
        $settings->setResourcesURL(_MODULE_DIR_.'magicslideshow/views/js/', 'js');
        $settings->setResourcesURL(_MODULE_DIR_.'magicslideshow/views/css/', 'css');
        $settings->namePrefix = 'magicslideshow';

        $settings->pageTitle .= '&nbsp;<a target="_blank" title="Watch tutorial" href="http://www.youtube.com/watch?v=E7WG86qjEoA&t=48" style="float: right;">Watch tutorial</a>';

        $settings->languagesData = Db::getInstance()->ExecuteS('SELECT id_lang as id, iso_code as code, active FROM `'._DB_PREFIX_.'lang` ORDER BY `id_lang` ASC');

        if ($activeTab) {
            $settings->activeTab = $activeTab;
        }

        $settings->imageBaseUrl = _PS_IMG_.'magicslideshow/';

        $result = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'magicslideshow_images` ORDER BY `order`');
        if ($result) {
            $settings->customSlideshowImagesData = $result;
        }
        foreach ($settings->customSlideshowImagesData as &$imageData) {
            $imageData['name'] = $imageData['name'].'-home'.$this->imageTypeSuffix.'.'.$imageData['ext'];
            $imageData['exclude'] = 1 - (int)$imageData['enabled'];
        }



        $html = $settings->getHTML();
        return $html;
    }

    public function needUpdateDb()
    {
        //NOTE: check if all new params are present in DB
        $params = array();
        $sql = 'SELECT `name`, `value`, `block` FROM `'._DB_PREFIX_.'magicslideshow_settings`';
        $result = Db::getInstance()->ExecuteS($sql);
        foreach ($result as $row) {
            if (!isset($params[$row['block']])) {
                $params[$row['block']] = array();
            }
            $params[$row['block']][$row['name']] = $row['value'];
        }

        $needUpdate = false;
        $paramsMap = $this->getParamsMap();
        foreach ($paramsMap as $blockId => $groups) {
            foreach ($groups as $group) {
                foreach ($group as $param => $required) {
                    if (!isset($params[$blockId][$param])) {
                        $needUpdate = true;
                        break 3;
                    }
                }
            }
        }

        return $needUpdate;
    }

    public function loadTool($profile = false, $force = false)
    {
        if (!isset($GLOBALS['magictoolbox']['magicslideshow']['class']) || $force) {
            require_once(dirname(__FILE__).'/magicslideshow.module.core.class.php');
            $GLOBALS['magictoolbox']['magicslideshow']['class'] = new MagicSlideshowModuleCoreClass();
            $tool = &$GLOBALS['magictoolbox']['magicslideshow']['class'];
            // load current params
            $sql = 'SELECT `name`, `value`, `block` FROM `'._DB_PREFIX_.'magicslideshow_settings` WHERE `enabled`=1';
            $result = Db::getInstance()->ExecuteS($sql);
            //NOTE: get data without cache
            //$result = Db::getInstance()->ExecuteS($sql, true, false);
            foreach ($result as $row) {
                $tool->params->setValue($row['name'], $row['value'], $row['block']);
            }

            // load translates
            $GLOBALS['magictoolbox']['magicslideshow']['translates'] = $this->getMessages();
            $translates = & $GLOBALS['magictoolbox']['magicslideshow']['translates'];
            foreach ($this->getBlocks() as $block => $label) {
                //NOTE: prepare image types
                foreach (array('large', 'selector', 'thumb') as $name) {
                    if ($tool->params->checkValue($name.'-image', 'original', $block)) {
                        $tool->params->setValue($name.'-image', false, $block);
                    }
                }
            }


        }

        $tool = &$GLOBALS['magictoolbox']['magicslideshow']['class'];

        if ($profile) {
            $tool->params->setProfile($profile);
        }

        return $tool;

    }
    public function hookHeader($params)
    {
        //global $smarty;
        $smarty = &$GLOBALS['smarty'];

        if (!$this->isPrestaShop15x) {
            ob_start();
        }

        $headers = '';
        $tool = $this->loadTool();
        $tool->params->resetProfile();

        if ($this->isPrestaShop17x) {
            $page = $smarty->{$this->getTemplateVars}('page');
            if (is_array($page) && isset($page['page_name'])) {
                $page = $page['page_name'];
            }
        } else {
            $page = $smarty->{$this->getTemplateVars}('page_name');
        }

        switch ($page) {
            case 'product':
            case 'index':
                break;
            default:
                $page = '';
        }

        if ($tool->params->checkValue('include-headers-on-all-pages', 'Yes', 'default') && ($GLOBALS['magictoolbox']['magicslideshow']['headers'] = true)
            || $tool->params->profileExists($page) && !$tool->params->checkValue('enable-effect', 'No', $page)
            || $page == 'index' && !$tool->params->checkValue('enable-effect', 'No', 'homeslideshow')
            || $page == 'index' && !$tool->params->checkValue('enable-effect', 'No', 'homefeatured') && parent::isInstalled($this->featuredProductsModule) && parent::getInstanceByName($this->featuredProductsModule)->active
            || $page == 'index' && !$tool->params->checkValue('enable-effect', 'No', 'blocknewproducts_home') && parent::isInstalled($this->newProductsModule) && parent::getInstanceByName($this->newProductsModule)->active
            || $page == 'index' && !$tool->params->checkValue('enable-effect', 'No', 'blockbestsellers_home') && parent::isInstalled($this->topSellersModule) && parent::getInstanceByName($this->topSellersModule)->active
            || $page == 'index' && !$tool->params->checkValue('enable-effect', 'No', 'blockspecials_home') && parent::isInstalled($this->specialsProductsModule) && parent::getInstanceByName($this->specialsProductsModule)->active
            || !$tool->params->checkValue('enable-effect', 'No', 'blockviewed') && parent::isInstalled('blockviewed') && parent::getInstanceByName('blockviewed')->active
            || !$tool->params->checkValue('enable-effect', 'No', 'blockspecials') && parent::isInstalled($this->specialsProductsModule) && parent::getInstanceByName($this->specialsProductsModule)->active
            || !$tool->params->checkValue('enable-effect', 'No', 'blocknewproducts') && parent::isInstalled($this->newProductsModule) && parent::getInstanceByName($this->newProductsModule)->active
            || !$tool->params->checkValue('enable-effect', 'No', 'blockbestsellers') && parent::isInstalled($this->topSellersModule) && parent::getInstanceByName($this->topSellersModule)->active
        /**/) {
            // include headers
            $headers = $tool->getHeadersTemplate(_MODULE_DIR_.'magicslideshow/views/js', _MODULE_DIR_.'magicslideshow/views/css');

            if (!$this->isPrestaShop17x) {
                //NOTE: if we need this on product page!?
                $headers .= '<script type="text/javascript" src="'._MODULE_DIR_.'magicslideshow/views/js/common.js"></script>';
            }

            if ($page == 'product' && !$tool->params->checkValue('enable-effect', 'No', 'product')) {
                $headers .= '
<script type="text/javascript">

    var isProductMagicSlideshowReady = false;
    window.MagicSlideshowOptions || (window.MagicSlideshowOptions = {});
    MagicSlideshowOptions.onready = function(id) {
        //console.log(\'MagicSlideshowOptions.onready: \', id);
        if (id == \'productMagicSlideshow\') {
            isProductMagicSlideshowReady = true;
        }
    };

</script>';
                if ($this->isPrestaShop17x) {
                    $headers .= "\n".'<script type="text/javascript" src="'._MODULE_DIR_.'magicslideshow/views/js/product17.js"></script>'."\n";
                }
                if (!$this->isPrestaShop17x && !$GLOBALS['magictoolbox']['isProductScriptIncluded']) {
                    if ($this->displayInlineProductJs || (bool)Configuration::get('PS_JS_DEFER')) {
                        //NOTE: include product.js as inline because it has to be called after previous inline scripts
                        $productJsCContents = Tools::file_get_contents(_PS_ROOT_DIR_.'/modules/magicslideshow/views/js/product.js');
                        $headers .= "\n".'<script type="text/javascript">'.$productJsCContents.'</script>'."\n";
                    } else {
                        $headers .= "\n".'<script type="text/javascript" src="'._MODULE_DIR_.'magicslideshow/views/js/product.js"></script>'."\n";
                    }

                    $GLOBALS['magictoolbox']['isProductScriptIncluded'] = true;
                }
            }
            if ($page == 'index' && !$this->isPrestaShop17x) {
                $headers .= '
<script type="text/javascript">
    var isPrestaShop16x = '.($this->isPrestaShop16x ? 'true' : 'false').';
    $(document).ready(function() {

        //NOTE: fix, because Prestashop adds class only for ul.tab-pane
        $(\'#index .tab-pane\').removeClass(\'active\');
        $(\'#index .tab-pane:first\').addClass(\'active\');

        if (isPrestaShop16x && typeof(MagicSlideshow) != \'undefined\') {
            var homepageLastTab = \'\';
            var homepageTabs = {};
            $(\'#home-page-tabs li a\').each(function(index) {
                homepageTabs[this.href.replace(/^.*?#([^#]+)$/, \'$1\')] = 0;
            });
            $(\'#home-page-tabs li.active a\').each(function(index) {
                homepageTabs[this.href.replace(/^.*?#([^#]+)$/, \'$1\')] = 1;
                homepageLastTab = this.href.replace(/^.*?#([^#]+)$/, \'$1\');
            });
            $(\'#home-page-tabs a[data-toggle="tab"]\').on(\'shown.bs.tab\', function (e) {
                var key = e.target.href.replace(/^.*?#([^#]+)$/, \'$1\');
                if (typeof(homepageTabs[key]) != \'undefined\') {
                    var toolEl = $(\'div#\'+key+\' .MagicSlideshow[id]\').get(0);
                    if (toolEl) {
                        if (homepageTabs[key]) {
                            MagicSlideshow.play(toolEl.id);
                        } else {
                            homepageTabs[key] = 1;
                            MagicSlideshow.refresh(toolEl.id);
                        }
                    }
                    toolEl = $(\'div#\'+homepageLastTab+\' .MagicSlideshow[id]\').get(0);
                    if (toolEl) {
                        MagicSlideshow.pause(toolEl.id);
                    }
                    homepageLastTab = key;
                }
            });
        }
    });
</script>
';
            }

            $domNotAvailable = extension_loaded('dom') ? false : true;
            if ($this->displayInlineProductJs && $domNotAvailable) {
                $scriptsPattern = '#(?:\s*+<script\b[^>]*+>.*?<\s*+/script\b[^>]*+>)++#Uims';
                if (preg_match($scriptsPattern, $headers, $scripts)) {
                    $GLOBALS['magictoolbox']['magicslideshow']['scripts'] =
                        '<!-- MAGICSLIDESHOW HEADERS START -->'.$scripts[0].'<!-- MAGICSLIDESHOW HEADERS END -->';
                    $headers = preg_replace($scriptsPattern, '', $headers);
                }
            }

            if ($this->isSmarty3) {
                //Smarty v3 template engine
                $smarty->registerFilter('output', array(Module::getInstanceByName('magicslideshow'), 'parseTemplateCategory'));
            } else {
                //Smarty v2 template engine
                $smarty->register_outputfilter(array(Module::getInstanceByName('magicslideshow'), 'parseTemplateCategory'));
            }
            $GLOBALS['magictoolbox']['filters']['magicslideshow'] = 'parseTemplateCategory';

            // presta create new class every time when hook called
            // so we need save our data in the GLOBALS
            $GLOBALS['magictoolbox']['magicslideshow']['cookie'] = $params['cookie'];
            $GLOBALS['magictoolbox']['magicslideshow']['productsViewedIds'] = (isset($params['cookie']->viewed) && !empty($params['cookie']->viewed)) ? explode(',', $params['cookie']->viewed) : array();

            $headers = '<!-- MAGICSLIDESHOW HEADERS START -->'.$headers.'<!-- MAGICSLIDESHOW HEADERS END -->';

        }

        return $headers;

    }

    public function hookActionDispatcher($params)
    {
        //NOTE: registered for 1.7.x
        if (!$this->isAjaxRequest) {
            return;
        }

        switch ($params['controller_class']) {
            case 'CategoryController':
                $page = 'category';
                break;
            case 'SearchController':
                $page = 'search';
                break;
            default:
                return;
        }

        $smarty = &$GLOBALS['smarty'];
        $smarty->assign('page', array(
            'page_name' => $page
        ));

        $this->hookHeader($params);
    }

    public function hookProductFooter($params)
    {
        //NOTE: we need save this data in the GLOBALS for compatible with some Prestashop modules which reset the $product smarty variable
        if ($this->isPrestaShop17x && is_array($params['product'])) {
            $GLOBALS['magictoolbox']['magicslideshow']['product'] = array(
                'id' => $params['product']['id'],
                'name' => $params['product']['name'],
                'link_rewrite' => $params['product']['link_rewrite']
            );
        } else {
            $GLOBALS['magictoolbox']['magicslideshow']['product'] = array(
                'id' => $params['product']->id,
                'name' => $params['product']->name,
                'link_rewrite' => $params['product']->link_rewrite
            );
        }
        return '';
    }

    public function hookFooter($params)
    {
        if (!$this->isPrestaShop15x) {

            $contents = ob_get_contents();
            ob_end_clean();

            $matches = array();
            $lang = isset($params['cart']->id_lang) ? $params['cart']->id_lang : 0;
            if (preg_match_all('/\[magicslideshow(?:\sid=(\d+(?:,\d+)*))?\]/', $contents, $matches, PREG_SET_ORDER)) {
                foreach ($matches as $match) {
                    $contents = str_replace($match[0], $this->getCustomSlideshow(empty($match[1]) ? '' : $match[1], $lang, false), $contents);
                }
                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
            }

            if ($GLOBALS['magictoolbox']['magicslideshow']['headers'] == false) {
                $contents = preg_replace('/<\!-- MAGICSLIDESHOW HEADERS START -->.*?<\!-- MAGICSLIDESHOW HEADERS END -->/is', '', $contents);
            } else {
                $contents = preg_replace('/<\!-- MAGICSLIDESHOW HEADERS (START|END) -->/is', '', $contents);
                //NOTE: add class for identifying PrestaShop version
                if (preg_match('#(<body\b[^>]*?\sclass\s*+=\s*+"[^"]*+)("[^>]*+>)#is', $contents)) {
                    $contents = preg_replace('#(<body\b[^>]*?\sclass\s*+=\s*+"[^"]*+)("[^>]*+>)#is', '$1 '.$this->psVersionClass.'$2', $contents);
                } else {
                    $contents = preg_replace('#(<body\s[^>]*+)>#is', '$1 class="'.$this->psVersionClass.'">', $contents);
                }
            }

            echo $contents;

        }

        return '';

    }

    public function hookDisplayTopColumn($params)
    {
        $page = $params['smarty']->{$this->getTemplateVars}('page_name');
        return $page == 'index' ? $this->hookHome($params) : '';
    }

    public function hookHome($params)
    {
        $tool = $this->loadTool();
        $tool->params->setProfile('homeslideshow');
        if ($tool->params->checkValue('enable-effect', 'No')) {
            return '';
        }
        $lang = isset($params['cart']->id_lang) ? $params['cart']->id_lang : 0;
        $slideshow = $this->getCustomSlideshow('', $lang, true);
        if (!empty($slideshow)) {
            $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
        }
        return $slideshow;
    }

    public function getCustomSlideshow($ids = '', $lang = 0, $enabledOnly = false)
    {
        $slideshow = '';
        $tool = $this->loadTool();
        $tool->params->setProfile('homeslideshow');
        if (empty($ids)) {
            $where = '';
            $order = 'ORDER BY `order`';
        } else {
            $ids = pSQL($ids);
            $where = '`id` IN ('.$ids.') AND ';
            $order = 'ORDER BY FIELD(`id`,'.$ids.')';
        }
        $where .= $enabledOnly ? '`enabled`=1 AND ' : '';
        $where .= $lang ? '(`lang`=0 OR `lang`='.(int)$lang.') ' : '`lang`=0 ';
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'magicslideshow_images` WHERE '.$where.$order;
        $result = Db::getInstance()->ExecuteS($sql);
        if (is_array($result) && count($result)) {
            $imagesData = array();
            $thumbSuffix = $tool->params->getValue('selector-image');
            $thumbSuffix = $thumbSuffix ? '-'.$thumbSuffix : '';
            $imgSuffix = $tool->params->getValue('thumb-image');
            $imgSuffix = $imgSuffix ? '-'.$imgSuffix : '';
            $fullscreenSuffix = $tool->params->getValue('large-image');
            $fullscreenSuffix = $fullscreenSuffix ? '-'.$fullscreenSuffix : '';
            foreach ($result as $row) {
                $imagesData[$row['id']]['link'] = $row['link'];
                $imagesData[$row['id']]['title'] = $row['title'];
                $imagesData[$row['id']]['description'] = htmlspecialchars_decode($row['description']);
                $imagesData[$row['id']]['thumb'] = _PS_IMG_.'magicslideshow/'.$row['name'].$thumbSuffix.'.'.$row['ext'];
                $imagesData[$row['id']]['img'] = _PS_IMG_.'magicslideshow/'.$row['name'].$imgSuffix.'.'.$row['ext'];
                $imagesData[$row['id']]['fullscreen'] = _PS_IMG_.'magicslideshow/'.$row['name'].$fullscreenSuffix.'.'.$row['ext'];
            }
            $slideshow = '<div class="MagicToolboxContainer">'.$tool->getMainTemplate($imagesData, array('id' => 'customSlideshow'.md5($where))).'</div>';
        }
        return $slideshow;
    }

    private static $outputMatches = array();

    public function prepareOutput($output, $index = 'DEFAULT')
    {
        if (!isset(self::$outputMatches[$index])) {
            $regExp = '<div\b[^>]*?\sclass\s*+=\s*+"[^"]*?(?<=\s|")MagicToolboxContainer(?=\s|")[^"]*+"[^>]*+>'.
                        '('.
                        '(?:'.
                            '[^<]++'.
                            '|'.
                            '<(?!/?div\b|!--)'.
                            '|'.
                            '<!--.*?-->'.
                            '|'.
                            '<div\b[^>]*+>'.
                                '(?1)'.
                            '</div\s*+>'.
                        ')*+'.
                        ')'.
                        '</div\s*+>';
            preg_match_all('#'.$regExp.'#is', $output, self::$outputMatches[$index]);
            foreach (self::$outputMatches[$index][0] as $key => $match) {
                $output = str_replace($match, 'MAGICSLIDESHOW_MATCH_'.$index.'_'.$key.'_', $output);
            }
        } else {
            foreach (self::$outputMatches[$index][0] as $key => $match) {
                $output = str_replace('MAGICSLIDESHOW_MATCH_'.$index.'_'.$key.'_', $match, $output);
            }
            unset(self::$outputMatches[$index]);
        }
        return $output;

    }

    public function parseTemplateCategory($output, $smarty)
    {
        if ($this->isSmarty3) {
            //Smarty v3 template engine
            $currentTemplate = Tools::substr(basename($smarty->template_resource), 0, -4);
            if ($currentTemplate == 'breadcrumb') {
                $currentTemplate = 'product';
            } elseif ($currentTemplate == 'pagination') {
                $currentTemplate = 'category';
            }
        } else {
            //Smarty v2 template engine
            $currentTemplate = $smarty->currentTemplate;
        }

        if ($this->isPrestaShop17x && ($currentTemplate == 'index' || $currentTemplate == 'page') ||
            $this->isPrestaShop15x && $currentTemplate == 'layout') {

            $matches = array();
            $lang = (int)$GLOBALS['magictoolbox']['magicslideshow']['cookie']->id_lang;
            if (preg_match_all('/\[magicslideshow(?:\sid=(\d+(?:,\d+)*))?\]/', $output, $matches, PREG_SET_ORDER)) {
                foreach ($matches as $match) {
                    $output = str_replace($match[0], $this->getCustomSlideshow(empty($match[1]) ? '' : $match[1], $lang, false), $output);
                }
                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
            }

            if (version_compare(_PS_VERSION_, '1.5.5.0', '>=')) {
                //NOTE: because we do not know whether the effect is applied to the blocks in the cache
                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
            }
            //NOTE: full contents in prestashop 1.5.x
            if ($GLOBALS['magictoolbox']['magicslideshow']['headers'] == false) {
                $output = preg_replace('/<\!-- MAGICSLIDESHOW HEADERS START -->.*?<\!-- MAGICSLIDESHOW HEADERS END -->/is', '', $output);
            } else {
                $output = preg_replace('/<\!-- MAGICSLIDESHOW HEADERS (START|END) -->/is', '', $output);
            }
            return $output;
        }

        switch ($currentTemplate) {
            case 'search':
            case 'manufacturer':
                //$currentTemplate = 'manufacturer';
                break;
            case 'best-sales':
                $currentTemplate = 'bestsellerspage';
                break;
            case 'new-products':
                $currentTemplate = 'newproductpage';
                break;
            case 'prices-drop':
                $currentTemplate = 'specialspage';
                break;
            case 'blockbestsellers-home':
                $currentTemplate = 'blockbestsellers_home';
                break;
            case 'blockspecials-home':
                $currentTemplate = 'blockspecials_home';
                break;
            case 'product-list'://for 'Layered navigation block'
                if (strpos($_SERVER['REQUEST_URI'], 'blocklayered-ajax.php') !== false) {
                    $currentTemplate = 'category';
                }
                break;
            //NOTE: just in case (issue 88975)
            case 'ProductController':
                $currentTemplate = 'product';
                break;
            case 'products':
                if ($this->isPrestaShop17x && $this->isAjaxRequest) {
                    $page = $smarty->{$this->getTemplateVars}('page');
                    if (is_array($page) && isset($page['page_name'])) {
                        $currentTemplate = $page['page_name'];
                    }
                }
                break;
            case 'ps_featuredproducts':
                if ($this->isPrestaShop17x) {
                    $currentTemplate = 'homefeatured';
                }
                break;
            case 'ps_bestsellers':
                if ($this->isPrestaShop17x) {
                    $currentTemplate = 'blockbestsellers_home';
                }
                break;
            case 'ps_newproducts':
                if ($this->isPrestaShop17x) {
                    $currentTemplate = 'blocknewproducts_home';
                }
                break;
            case 'ps_specials':
                if ($this->isPrestaShop17x) {
                    $currentTemplate = 'blockspecials_home';
                }
                break;
        }

        $tool = $this->loadTool();
        if (!$tool->params->profileExists($currentTemplate) || $tool->params->checkValue('enable-effect', 'No', $currentTemplate)) {
            return $output;
        }
        $tool->params->setProfile($currentTemplate);

        //global $link;
        $link = &$GLOBALS['link'];
        $cookie = &$GLOBALS['magictoolbox']['magicslideshow']['cookie'];
        if (method_exists($link, 'getImageLink')) {
            $_link = &$link;
        } else {
            /* for Prestashop ver 1.1 */
            $_link = &$this;
        }

        $output = self::prepareOutput($output);

        switch ($currentTemplate) {
            case 'homefeatured':
                $categoryID = $this->isPrestaShop15x ? Context::getContext()->shop->getCategory() : 1;
                $category = new Category($categoryID);
                $nb = (int)Configuration::get('HOME_FEATURED_NBR');//Number of product displayed
                $products = $category->getProducts((int)$cookie->id_lang, 1, ($nb ? $nb : 10));
                if (!is_array($products)) {
                    break;
                }
                if (count($products) < 2) {
                    break;
                }
                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
                $productImagesData = array();
                $useLink = $tool->params->checkValue('link-to-product-page', 'Yes');
                foreach ($products as $p_key => $product) {
                    $productImagesData[$p_key]['link'] = $useLink?$link->getProductLink($product['id_product'], $product['link_rewrite'], isset($product['category']) ? $product['category'] : null):'';
                    $productImagesData[$p_key]['title'] = $product['name'];
                    $productImagesData[$p_key]['img'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('thumb-image'));
                    $productImagesData[$p_key]['thumb'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('selector-image'));
                    $productImagesData[$p_key]['fullscreen'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('large-image'));
                }
                $html = $tool->getMainTemplate($productImagesData, array('id' => 'homefeaturedMagicSlideshow'));
                if ($this->isPrestaShop16x) {
                    $html = '<div id="homefeatured" class="MagicToolboxContainer homefeatured tab-pane">'.$html.'</div>';
                } else {
                    $html = '<div class="MagicToolboxContainer">'.$html.'</div>';
                }

                if ($this->isPrestaShop17x) {
                    $pattern =  '<div\b[^>]*?\bclass\s*+=\s*+"[^"]*?\bproducts\b[^"]*+"[^>]*+>'.
                                '('.
                                '(?:'.
                                    '[^<]++'.
                                    '|'.
                                    '<(?!/?div\b|!--)'.
                                    '|'.
                                    '<!--.*?-->'.
                                    '|'.
                                    '<div\b[^>]*+>'.
                                        '(?1)'.
                                    '</div\s*+>'.
                                ')*+'.
                                ')'.
                                '</div\s*+>';
                } else {
                    $pattern = '<ul\b[^>]*?>.*?</ul>';
                }
                // preg_match_all('#'.$pattern.'#is', $output, $matches, PREG_SET_ORDER);
                // debug_log($matches);

                $output = preg_replace('#'.$pattern.'#is', $html, $output);
                break;
            case 'product':
                //debug_log('MagicSlideshow parseTemplateCategory product');
                if (!isset($GLOBALS['magictoolbox']['magicslideshow']['product'])) {
                    //for skip loyalty module product.tpl
                    break;
                }

                //NOTE: if product block was processed by another tool (magic360)
                if ($GLOBALS['magictoolbox']['isProductBlockProcessed']) {
                    break;
                }

                //NOTE: get some data from $GLOBALS for compatible with Prestashop modules which reset the $product smarty variable
                //$product = &$GLOBALS['magictoolbox']['magicslideshow']['product'];
                $product = new Product((int)$GLOBALS['magictoolbox']['magicslideshow']['product']['id'], true, (int)$cookie->id_lang);
                $lrw = $product->link_rewrite;
                $pid = (int)$product->id;


                $images = $product->getImages((int)$cookie->id_lang);
                if (empty($images) || !is_array($images)) {
                    break;
                }
                if (count($images) < ($this->isPrestaShop17x ? 1 : 2)) {
                    break;
                }

                if ($this->isPrestaShop17x) {
                    $cover = $smarty->{$this->getTemplateVars}('product');
                    $cover = isset($cover['cover']) ? $cover['cover'] : array();
                } else {
                    $cover = $smarty->{$this->getTemplateVars}('cover');
                }

                if (!isset($cover['id_image'])) {
                    break;
                }

                $productImagesData = array();
                $selectorIDs = array();
                foreach ($images as $image) {
                    $id_image = (int)$image['id_image'];
                    //NOTE: to prevent dublicates
                    if (isset($selectorIDs[$id_image])) {
                        continue;
                    }
                    $selectorIDs[] = $id_image;
                    //if ($image['cover']) $coverID = $id_image;
                    $productImagesData[$id_image]['title'] = $image['legend'];
                    $productImagesData[$id_image]['thumb'] = $_link->getImageLink($lrw, $pid.'-'.$id_image, $tool->params->getValue('selector-image'));
                    $productImagesData[$id_image]['fullscreen'] = $_link->getImageLink($lrw, $pid.'-'.$id_image, $tool->params->getValue('large-image'));
                    $productImagesData[$id_image]['img'] = $_link->getImageLink($lrw, $pid.'-'.$id_image, $tool->params->getValue('thumb-image'));
                }

                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;

                $html = $tool->getMainTemplate($productImagesData, array('id' => 'productMagicSlideshow'));

                if ($this->isPrestaShop17x) {
                    $attributeId = $smarty->{$this->getTemplateVars}('product');
                    $attributeId = isset($attributeId['id_product_attribute']) ? $attributeId['id_product_attribute'] : null;
                    $combinationImages = $smarty->{$this->getTemplateVars}('combinationImages');
                    $combinationData = array(
                        'attributes' => array(),
                        'order' => $selectorIDs
                    );
                    if (is_array($combinationImages)) {
                        foreach ($combinationImages as $attrId => $combImages) {
                            $combinationData['attributes'][$attrId] = array();
                            foreach ($combImages as $combImage) {
                                $combinationData['attributes'][$attrId][] = (int)$combImage['id_image'];
                            }
                        }
                    }
                    $html .= '
<script type="text/javascript">
    //<![CDATA[
    var mtCombinationData = '.Tools::jsonEncode($combinationData).';
    //]]>
</script>
';
                } else {
                    $html .= '
<script type="text/javascript">
    //<![CDATA[
    magictoolboxImagesOrder = ['.implode(',', $selectorIDs).'];
    //]]>
</script>';
                }

                //NOTE: append main container
                if ($this->isPrestaShop17x) {
                    $mainImagePattern = '<div\b[^>]*?\bclass\s*+=\s*+"[^"]*?\bimages-container\b[^"]*+"[^>]*+>'.
                                        '('.
                                        '(?:'.
                                            '[^<]++'.
                                            '|'.
                                            '<(?!/?div\b|!--)'.
                                            '|'.
                                            '<!--.*?-->'.
                                            '|'.
                                            '<div\b[^>]*+>'.
                                                '(?1)'.
                                            '</div\s*+>'.
                                        ')*+'.
                                        ')'.
                                        '</div\s*+>';
                } else {
                    //NOTE: 'image' class added to support custom theme #53897
                    $mainImagePattern = '(<div\b[^>]*?(?:\bid\s*+=\s*+"image-block"|\bclass\s*+=\s*+"[^"]*?\bimage\b[^"]*+")[^>]*+>)'.
                                        '('.
                                        '(?:'.
                                            '[^<]++'.
                                            '|'.
                                            '<(?!/?div\b|!--)'.
                                            '|'.
                                            '<!--.*?-->'.
                                            '|'.
                                            '<div\b[^>]*+>'.
                                                '(?2)'.
                                            '</div\s*+>'.
                                        ')*+'.
                                        ')'.
                                        '</div\s*+>';
                }

                $matches = array();
                //preg_match_all('#'.$mainImagePattern.'#is', $output, $matches, PREG_SET_ORDER);
                //debug_log($matches);

                if (!preg_match('#'.$mainImagePattern.'#is', $output, $matches)) {
                    break;
                }

                if ($this->isPrestaShop17x) {
                    //NOTE: div.hidden-important can be replaced with ajax contents
                    $output = str_replace(
                        $matches[0],
                        '<div class="hidden-important">'.$matches[0].'</div>'.$html,
                        $output
                    );

                    //NOTE: cut arrows
                    $arrowsPattern = '<div\b[^>]*?\bclass\s*+=\s*+"[^"]*?\bscroll-box-arrows\b[^"]*+"[^>]*+>'.
                                        '('.
                                        '(?:'.
                                            '[^<]++'.
                                            '|'.
                                            '<(?!/?div\b|!--)'.
                                            '|'.
                                            '<!--.*?-->'.
                                            '|'.
                                            '<div\b[^>]*+>'.
                                                '(?1)'.
                                            '</div\s*+>'.
                                        ')*+'.
                                        ')'.
                                        '</div\s*+>';
                    $output = preg_replace('#'.$arrowsPattern.'#', '', $output);

                    $output = preg_replace('/<\!-- MAGICSLIDESHOW HEADERS (START|END) -->/is', '', $output);
                } else {
                    $iconsPattern = '<span\b[^>]*?\bclass\s*+=\s*+"[^"]*?\b(?:new-box|sale-box|discount)\b[^"]*+"[^>]*+>'.
                                    '('.
                                    '(?:'.
                                        '[^<]++'.
                                        '|'.
                                        '<(?!/?span\b|!--)'.
                                        '|'.
                                        '<!--.*?-->'.
                                        '|'.
                                        '<span\b[^>]*+>'.
                                            '(?1)'.
                                        '</span\s*+>'.
                                    ')*+'.
                                    ')'.
                                    '</span\s*+>';
                    $iconMatches = array();
                    if (preg_match_all('%'.$iconsPattern.'%is', $matches[2], $iconMatches, PREG_SET_ORDER)) {
                        foreach ($iconMatches as $key => $iconMatch) {
                            $matches[2] = str_replace($iconMatch[0], '', $matches[2]);
                            $iconMatches[$key] = $iconMatch[0];
                        }
                    }
                    $icons = implode('', $iconMatches);

                    $output = str_replace($matches[0], "{$matches[1]}{$icons}<div class=\"hidden-important\">{$matches[2]}</div>{$html}</div>", $output);

                    //NOTE: hide selectors from contents
                    //NOTE: 'image-additional' added to support custom theme #53897
                    //NOTE: div#views_block is parent for div#thumbs_list
                    $thumbsPattern =	'(<div\b[^>]*?(?:\bid\s*+=\s*+"(?:views_block|thumbs_list)"|\bclass\s*+=\s*+"[^"]*?\bimage-additional\b[^"]*+")[^>]*+>)'.
                                        '('.
                                        '(?:'.
                                            '[^<]++'.
                                            '|'.
                                            '<(?!/?div\b|!--)'.
                                            '|'.
                                            '<!--.*?-->'.
                                            '|'.
                                            '<div\b[^>]*+>'.
                                                '(?2)'.
                                            '</div\s*+>'.
                                        ')*+'.
                                        ')'.
                                        '</div\s*+>';
                    $matches = array();
                    if (preg_match("#{$thumbsPattern}#is", $output, $matches)) {
                        if (strpos($matches[1], 'class')) {
                            $replace = preg_replace('#\bclass\s*+=\s*+"#i', '$0hidden-important ', $matches[1]);
                        } else {
                            $replace = preg_replace('#<div\b#i', '$0 class="hidden-important"', $matches[1]);
                        }

                        $output = str_replace($matches[1], $replace, $output);
                    }

                    //NOTE: remove "View full size" link (in old PrestaShop)
                    $output = preg_replace('/<li[^>]*+>[^<]*+<span[^>]*?id="view_full_size"[^>]*+>[^<]*<\/span>[^<]*+<\/li>/is', '', $output);

                    //NOTE: hide span#wrapResetImages or a#resetImages
                    $matches = array();
                    if (preg_match('#(?:<span\b[^>]*?\bid\s*+=\s*+"wrapResetImages"[^>]*+>|<a\b[^>]*?\bid\s*+=\s*+"resetImages"[^>]*+>)#is', $output, $matches)) {
                        if (strpos($matches[0], 'class')) {
                            $replace = preg_replace('#\bclass\s*+=\s*+"#i', '$0hidden-important ', $matches[0]);
                        } else {
                            $replace = preg_replace('#<span\b#i', '$0 class="hidden-important"', $matches[0]);
                        }

                        $output = str_replace($matches[0], $replace, $output);
                    }
                }

                $GLOBALS['magictoolbox']['isProductBlockProcessed'] = true;
                break;
            case 'blockspecials':
                if (version_compare(_PS_VERSION_, '1.4', '<')) {
                    $products = $this->getAllSpecial((int)$cookie->id_lang);
                } else {
                    $products = Product::getPricesDrop((int)($cookie->id_lang), 0, 10, false, 'position', 'asc');
                }

                if (!is_array($products)) {
                    break;
                }
                $pCount = count($products);
                if ($pCount < 2) {
                    break;
                }
                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
                $productImagesData = array();
                $useLink = $tool->params->checkValue('link-to-product-page', 'Yes');

                foreach ($products as $p_key => $product) {
                    if ($useLink && (!Tools::getValue('id_product', false) || (Tools::getValue('id_product', false) != $product['id_product']))) {
                        $productImagesData[$p_key]['link'] = $link->getProductLink($product['id_product'], $product['link_rewrite'], isset($product['category']) ? $product['category'] : null);
                    } else {
                        $productImagesData[$p_key]['link'] = '';
                    }

                    $productImagesData[$p_key]['title'] = $product['name'];
                    $productImagesData[$p_key]['img'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('thumb-image'));
                    $productImagesData[$p_key]['thumb'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('selector-image'));
                    $productImagesData[$p_key]['fullscreen'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('large-image'));
                }

                $html = $tool->getMainTemplate($productImagesData, array('id' => 'blockspecialsMagicSlideshow'));

                //NOTE: to place it in the center (can't use "margin: 0 auto" because "display: inline-block")
                $html = '<div class="MagicToolboxContainer">'.$html.'</div>';

                $pattern = '<ul[^>]*?>.*?<\/ul>';
                $output = preg_replace('/'.$pattern.'/is', $html, $output);
                break;
            case 'blockspecials_home':
                if ($this->isPrestaShop17x) {
                    $products = $smarty->{$this->getTemplateVars}('products');
                } else {
                    $products = $smarty->{$this->getTemplateVars}('specials');
                }
                if (!is_array($products)) {
                    break;
                }
                $pCount = count($products);
                if ($pCount < 2) {
                    break;
                }

                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
                $productImagesData = array();
                $useLink = $tool->params->checkValue('link-to-product-page', 'Yes');

                foreach ($products as $p_key => $product) {
                    if ($useLink/* && (!Tools::getValue('id_product', false) || (Tools::getValue('id_product', false) != $product['id_product']))*/) {
                        $productImagesData[$p_key]['link'] = $link->getProductLink($product['id_product'], $product['link_rewrite'], isset($product['category']) ? $product['category'] : null);
                    } else {
                        $productImagesData[$p_key]['link'] = '';
                    }

                    $productImagesData[$p_key]['title'] = $product['name'];
                    $productImagesData[$p_key]['img'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('thumb-image'));
                    $productImagesData[$p_key]['thumb'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('selector-image'));
                    $productImagesData[$p_key]['fullscreen'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('large-image'));
                }

                $html = $tool->getMainTemplate($productImagesData, array('id' => 'blockspecialsMagicSlideshow'));

                if ($this->isPrestaShop17x) {
                    $html = '<div id="blockspecials" class="MagicToolboxContainer blockspecials">'.$html.'</div>';
                    $pattern =  '<div\b[^>]*?\bclass\s*+=\s*+"[^"]*?\bproducts\b[^"]*+"[^>]*+>'.
                                '('.
                                '(?:'.
                                    '[^<]++'.
                                    '|'.
                                    '<(?!/?div\b|!--)'.
                                    '|'.
                                    '<!--.*?-->'.
                                    '|'.
                                    '<div\b[^>]*+>'.
                                        '(?1)'.
                                    '</div\s*+>'.
                                ')*+'.
                                ')'.
                                '</div\s*+>';
                    $matches = array();
                    if (preg_match('#'.$pattern.'#is', $output, $matches)) {
                        $pattern = '(<article\b[^>]*+>.*?</article>[^<]*+)++';
                        $html = preg_replace('#'.$pattern.'#is', $html, $matches[0]);
                        $output = str_replace($matches[0], $html, $output);
                    }
                } else {
                    $html = '<div id="blockspecials" class="MagicToolboxContainer blockspecials tab-pane">'.$html.'</div>';
                    $pattern = '<ul\b[^>]*+>.*?</ul>';
                    $output = preg_replace('#'.$pattern.'#is', $html, $output);
                }

                break;
            case 'blockviewed':
                $productIDs = $GLOBALS['magictoolbox']['magicslideshow']['productsViewedIds'];
                if ($this->isPrestaShop155x) {
                    $productIDs = array_reverse($productIDs);
                }

                $pCount = count($productIDs);
                if ($pCount < 2) {
                    break;
                }
                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
                $productImagesData = array();
                $useLink = $tool->params->checkValue('link-to-product-page', 'Yes');

                foreach ($productIDs as $id_product) {
                    $productViewedObj = new Product((int)$id_product, false, (int)$cookie->id_lang);
                    if (!Validate::isLoadedObject($productViewedObj) || !$productViewedObj->active) {
                        continue;
                    } else {
                        $images = $productViewedObj->getImages((int)$cookie->id_lang);
                        foreach ($images as $image) {
                            if ($image['cover']) {
                                $productViewedObj->cover = $productViewedObj->id.'-'.$image['id_image'];
                                $productViewedObj->legend = $image['legend'];
                                break;
                            }
                        }
                        if (!isset($productViewedObj->cover)) {
                            $productViewedObj->cover = Language::getIsoById($cookie->id_lang).'-default';
                            $productViewedObj->legend = '';
                        }
                        $lrw = $productViewedObj->link_rewrite;
                        if ($useLink && (!Tools::getValue('id_product', false) || (Tools::getValue('id_product', false) != $id_product))) {
                            $productImagesData[$id_product]['link'] = $link->getProductLink($id_product, $lrw, $productViewedObj->category);
                        } else {
                            $productImagesData[$id_product]['link'] = '';
                        }

                        $productImagesData[$id_product]['title'] = $productViewedObj->name;
                        $productImagesData[$id_product]['img'] = $_link->getImageLink($lrw, $productViewedObj->cover, $tool->params->getValue('thumb-image'));
                        $productImagesData[$id_product]['thumb'] = $_link->getImageLink($lrw, $productViewedObj->cover, $tool->params->getValue('selector-image'));
                        $productImagesData[$id_product]['fullscreen'] = $_link->getImageLink($lrw, $productViewedObj->cover, $tool->params->getValue('large-image'));
                    }
                }
                $html = $tool->getMainTemplate($productImagesData, array('id' => 'blockviewedMagicSlideshow'));

                //NOTE: to place it in the center (can't use "margin: 0 auto" because "display: inline-block")
                $html = '<div class="MagicToolboxContainer">'.$html.'</div>';

                $pattern = '<ul\b[^>]*?>.*?</ul>';
                $output = preg_replace('#'.$pattern.'#is', $html, $output);
                break;
            case 'blockbestsellers':
            case 'blockbestsellers_home':
            case 'blocknewproducts':
            case 'blocknewproducts_home':
                if (in_array($currentTemplate, array('blockbestsellers', 'blockbestsellers_home'))) {
                    $nb_products = $tool->params->getValue('max-number-of-products', $currentTemplate);
                    //$products = $smarty->{$this->getTemplateVars}('best_sellers');
                    //to get with description etc.
                    $products = ProductSale::getBestSales((int)$cookie->id_lang, 0, $nb_products);
                } else {
                    if ($this->isPrestaShop17x) {
                        $products = $smarty->{$this->getTemplateVars}('products');
                    } else {
                        $products = $smarty->{$this->getTemplateVars}('new_products');
                    }
                }

                if (!is_array($products)) {
                    break;
                }
                $pCount = count($products);
                if ($pCount < 2/* || !$products*/) {
                    break;
                }
                $GLOBALS['magictoolbox']['magicslideshow']['headers'] = true;
                $productImagesData = array();
                $useLink = $tool->params->checkValue('link-to-product-page', 'Yes');
                foreach ($products as $p_key => $product) {
                    if ($useLink && (!Tools::getValue('id_product', false) || (Tools::getValue('id_product', false) != $product['id_product']))) {
                        $productImagesData[$p_key]['link'] = $link->getProductLink($product['id_product'], $product['link_rewrite'], isset($product['category']) ? $product['category'] : null);
                    } else {
                        $productImagesData[$p_key]['link'] = '';
                    }

                    $productImagesData[$p_key]['title'] = $product['name'];
                    $productImagesData[$p_key]['img'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('thumb-image'));
                    $productImagesData[$p_key]['thumb'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('selector-image'));
                    $productImagesData[$p_key]['fullscreen'] = $_link->getImageLink($product['link_rewrite'], $product['id_image'], $tool->params->getValue('large-image'));
                }
                $html = $tool->getMainTemplate($productImagesData, array('id' => $currentTemplate.'MagicSlideshow'));
                if ($this->isPrestaShop16x) {
                    if ($currentTemplate == 'blockbestsellers_home') {
                        $html = '<div id="blockbestsellers" class="MagicToolboxContainer blockbestsellers tab-pane">'.$html.'</div>';
                    } elseif ($currentTemplate == 'blocknewproducts_home') {
                        $html = '<div id="blocknewproducts" class="MagicToolboxContainer blocknewproducts tab-pane active">'.$html.'</div>';
                    }
                } else {
                    //NOTE: to place it in the center (can't use "margin: 0 auto" because "display: inline-block")
                    $html = '<div class="MagicToolboxContainer">'.$html.'</div>';
                }
                if ($this->isPrestaShop17x) {
                    $pattern =  '<div\b[^>]*?\bclass\s*+=\s*+"[^"]*?\bproducts\b[^"]*+"[^>]*+>'.
                                '('.
                                '(?:'.
                                    '[^<]++'.
                                    '|'.
                                    '<(?!/?div\b|!--)'.
                                    '|'.
                                    '<!--.*?-->'.
                                    '|'.
                                    '<div\b[^>]*+>'.
                                        '(?1)'.
                                    '</div\s*+>'.
                                ')*+'.
                                ')'.
                                '</div\s*+>';
                    $matches = array();
                    if (preg_match('#'.$pattern.'#is', $output, $matches)) {
                        $pattern = '(<article\b[^>]*+>.*?</article>[^<]*+)++';
                        $html = preg_replace('#'.$pattern.'#is', $html, $matches[0]);
                        $output = str_replace($matches[0], $html, $output);
                    }
                } else {
                    $pattern = '<ul\b[^>]*+>.*?</ul>';
                    $output = preg_replace('#'.$pattern.'#is', $html, $output);
                }
                break;
        }

        return self::prepareOutput($output);

    }

    public function getAllSpecial($id_lang, $beginning = false, $ending = false)
    {
        $currentDate = date('Y-m-d');
        $result = Db::getInstance()->ExecuteS('
        SELECT p.*, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, p.`ean13`,
            i.`id_image`, il.`legend`, t.`rate`
        FROM `'._DB_PREFIX_.'product` p
        LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.')
        LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
        LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
        LEFT JOIN `'._DB_PREFIX_.'tax` t ON t.`id_tax` = p.`id_tax`
        WHERE (`reduction_price` > 0 OR `reduction_percent` > 0)
        '.((!$beginning && !$ending) ?
            'AND (`reduction_from` = `reduction_to` OR (`reduction_from` <= \''.pSQL($currentDate).'\' AND `reduction_to` >= \''.pSQL($currentDate).'\'))'
        :
            ($beginning ? 'AND `reduction_from` <= \''.pSQL($beginning).'\'' : '').($ending ? 'AND `reduction_to` >= \''.pSQL($ending).'\'' : '')).'
        AND p.`active` = 1
        ORDER BY RAND()');

        if (!$result) {
            return false;
        }

        $rows = array();
        foreach ($result as $row) {
            $rows[] = Product::getProductProperties($id_lang, $row);
        }

        return $rows;
    }

    /* for Prestashop ver 1.1 */
    public function getImageLink($name, $ids, $type = null)
    {
        return _THEME_PROD_DIR_.$ids.($type ? '-'.$type : '').'.jpg';
    }


    public function getProductDescription($id_product, $id_lang)
    {
        $sql = 'SELECT `description` FROM `'._DB_PREFIX_.'product_lang` WHERE `id_product` = '.(int)($id_product).' AND `id_lang` = '.(int)($id_lang);
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
        return isset($result[0]['description'])? $result[0]['description'] : '';
    }

    public function setImageSizes()
    {
        static $sizes = array();
        $tool = $this->loadTool();
        $profile = $tool->params->getProfile();
        if (!isset($sizes[$profile])) {
            $thumbImageType = $tool->params->getValue('thumb-image');
            $selectorImageType = $tool->params->getValue('selector-image');
            $sql = 'SELECT name, width, height FROM `'._DB_PREFIX_.'image_type` WHERE name in (\''.pSQL($thumbImageType).'\', \''.pSQL($selectorImageType).'\')';
            $result = Db::getInstance()->ExecuteS($sql);
            $result[$result[0]['name']] = $result[0];
            $result[$result[1]['name']] = $result[1];
            $tool->params->setValue('thumb-max-width', $result[$thumbImageType]['width']);
            $tool->params->setValue('thumb-max-height', $result[$thumbImageType]['height']);
            $tool->params->setValue('selector-max-width', $result[$selectorImageType]['width']);
            $tool->params->setValue('selector-max-height', $result[$selectorImageType]['height']);
            $sizes[$profile] = true;
        }
    }

    public function fillDB()
    {
        $sql = 'INSERT INTO `'._DB_PREFIX_.'magicslideshow_settings` (`block`, `name`, `value`, `default_value`, `enabled`, `default_enabled`) VALUES
                (\'default\', \'include-headers-on-all-pages\', \'No\', \'No\', 1, 1),
                (\'default\', \'thumb-image\', \'large\', \'large\', 1, 1),
                (\'default\', \'selector-image\', \'small\', \'small\', 1, 1),
                (\'default\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'default\', \'width\', \'auto\', \'auto\', 1, 1),
                (\'default\', \'height\', \'auto\', \'auto\', 1, 1),
                (\'default\', \'orientation\', \'horizontal\', \'horizontal\', 1, 1),
                (\'default\', \'arrows\', \'No\', \'No\', 1, 1),
                (\'default\', \'loop\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'effect\', \'slide\', \'slide\', 1, 1),
                (\'default\', \'effect-speed\', \'600\', \'600\', 1, 1),
                (\'default\', \'effect-easing\', \'ease\', \'ease\', 1, 1),
                (\'default\', \'autoplay\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'slide-duration\', \'6000\', \'6000\', 1, 1),
                (\'default\', \'shuffle\', \'No\', \'No\', 1, 1),
                (\'default\', \'kenburns\', \'No\', \'No\', 1, 1),
                (\'default\', \'pause\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'selectors-style\', \'bullets\', \'bullets\', 1, 1),
                (\'default\', \'selectors\', \'none\', \'none\', 1, 1),
                (\'default\', \'selectors-size\', \'45\', \'45\', 1, 1),
                (\'default\', \'selectors-eye\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'bullets-preview\', \'top\', \'top\', 1, 1),
                (\'default\', \'selectors-fill\', \'No\', \'No\', 1, 1),
                (\'default\', \'caption\', \'No\', \'No\', 1, 1),
                (\'default\', \'fullscreen\', \'No\', \'No\', 1, 1),
                (\'default\', \'preload\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'keyboard\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'show-loader\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'autostart\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'link-to-product-page\', \'Yes\', \'Yes\', 1, 1),
                (\'default\', \'show-message\', \'No\', \'No\', 1, 1),
                (\'default\', \'message\', \'\', \'\', 1, 1),
                (\'product\', \'thumb-image\', \'large\', \'large\', 0, 0),
                (\'product\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'product\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'product\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'product\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'product\', \'orientation\', \'horizontal\', \'horizontal\', 0, 0),
                (\'product\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'product\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'product\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'product\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'product\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'product\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'product\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'product\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'product\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'product\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'product\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'product\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'product\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'product\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'product\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'product\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'product\', \'caption\', \'No\', \'No\', 0, 0),
                (\'product\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'product\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'product\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'product\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'product\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'product\', \'enable-effect\', \'Yes\', \'Yes\', 1, 1),
                (\'product\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'product\', \'message\', \'\', \'\', 0, 0),
                (\'blocknewproducts\', \'thumb-image\', \'home\', \'home\', 1, 1),
                (\'blocknewproducts\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'blocknewproducts\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'blocknewproducts\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'blocknewproducts\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'blocknewproducts\', \'orientation\', \'vertical\', \'vertical\', 1, 1),
                (\'blocknewproducts\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'blocknewproducts\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'blocknewproducts\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'blocknewproducts\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'blocknewproducts\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'blocknewproducts\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'blocknewproducts\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'blocknewproducts\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'blocknewproducts\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts\', \'caption\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'blocknewproducts\', \'link-to-product-page\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts\', \'message\', \'\', \'\', 0, 0),
                (\'blocknewproducts_home\', \'thumb-image\', \'large\', \'large\', 0, 0),
                (\'blocknewproducts_home\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'blocknewproducts_home\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'blocknewproducts_home\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'blocknewproducts_home\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'blocknewproducts_home\', \'orientation\', \'horizontal\', \'horizontal\', 0, 0),
                (\'blocknewproducts_home\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts_home\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'blocknewproducts_home\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'blocknewproducts_home\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'blocknewproducts_home\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'blocknewproducts_home\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts_home\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts_home\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'blocknewproducts_home\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'blocknewproducts_home\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'blocknewproducts_home\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'blocknewproducts_home\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts_home\', \'caption\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts_home\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts_home\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'blocknewproducts_home\', \'link-to-product-page\', \'Yes\', \'Yes\', 0, 0),
                (\'blocknewproducts_home\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'blocknewproducts_home\', \'message\', \'\', \'\', 0, 0),
                (\'blockbestsellers\', \'thumb-image\', \'home\', \'home\', 1, 1),
                (\'blockbestsellers\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'blockbestsellers\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'blockbestsellers\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'blockbestsellers\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'blockbestsellers\', \'orientation\', \'vertical\', \'vertical\', 1, 1),
                (\'blockbestsellers\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'blockbestsellers\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'blockbestsellers\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'blockbestsellers\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'blockbestsellers\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'blockbestsellers\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'blockbestsellers\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'blockbestsellers\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'blockbestsellers\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers\', \'caption\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'max-number-of-products\', \'5\', \'5\', 1, 1),
                (\'blockbestsellers\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'blockbestsellers\', \'link-to-product-page\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers\', \'message\', \'\', \'\', 0, 0),
                (\'blockbestsellers_home\', \'thumb-image\', \'large\', \'large\', 0, 0),
                (\'blockbestsellers_home\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'blockbestsellers_home\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'blockbestsellers_home\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'blockbestsellers_home\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'blockbestsellers_home\', \'orientation\', \'horizontal\', \'horizontal\', 0, 0),
                (\'blockbestsellers_home\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers_home\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'blockbestsellers_home\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'blockbestsellers_home\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'blockbestsellers_home\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'blockbestsellers_home\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers_home\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers_home\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'blockbestsellers_home\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'blockbestsellers_home\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'blockbestsellers_home\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'blockbestsellers_home\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers_home\', \'caption\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers_home\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers_home\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'max-number-of-products\', \'8\', \'8\', 1, 1),
                (\'blockbestsellers_home\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'blockbestsellers_home\', \'link-to-product-page\', \'Yes\', \'Yes\', 0, 0),
                (\'blockbestsellers_home\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'blockbestsellers_home\', \'message\', \'\', \'\', 0, 0),
                (\'blockspecials\', \'thumb-image\', \'home\', \'home\', 1, 1),
                (\'blockspecials\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'blockspecials\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'blockspecials\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'blockspecials\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'blockspecials\', \'orientation\', \'vertical\', \'vertical\', 1, 1),
                (\'blockspecials\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'blockspecials\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'blockspecials\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'blockspecials\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'blockspecials\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'blockspecials\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'blockspecials\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'blockspecials\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'blockspecials\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'blockspecials\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'blockspecials\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'blockspecials\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'blockspecials\', \'caption\', \'No\', \'No\', 0, 0),
                (\'blockspecials\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'blockspecials\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'blockspecials\', \'link-to-product-page\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'blockspecials\', \'message\', \'\', \'\', 0, 0),
                (\'blockspecials_home\', \'thumb-image\', \'large\', \'large\', 0, 0),
                (\'blockspecials_home\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'blockspecials_home\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'blockspecials_home\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'blockspecials_home\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'blockspecials_home\', \'orientation\', \'horizontal\', \'horizontal\', 0, 0),
                (\'blockspecials_home\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'blockspecials_home\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'blockspecials_home\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'blockspecials_home\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'blockspecials_home\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'blockspecials_home\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'blockspecials_home\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'blockspecials_home\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'blockspecials_home\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'blockspecials_home\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'blockspecials_home\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'blockspecials_home\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'blockspecials_home\', \'caption\', \'No\', \'No\', 0, 0),
                (\'blockspecials_home\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'blockspecials_home\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'blockspecials_home\', \'link-to-product-page\', \'Yes\', \'Yes\', 0, 0),
                (\'blockspecials_home\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'blockspecials_home\', \'message\', \'\', \'\', 0, 0),
                (\'blockviewed\', \'thumb-image\', \'home\', \'home\', 1, 1),
                (\'blockviewed\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'blockviewed\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'blockviewed\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'blockviewed\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'blockviewed\', \'orientation\', \'vertical\', \'vertical\', 1, 1),
                (\'blockviewed\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'blockviewed\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'blockviewed\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'blockviewed\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'blockviewed\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'blockviewed\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'blockviewed\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'blockviewed\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'blockviewed\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'blockviewed\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'blockviewed\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'blockviewed\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'blockviewed\', \'caption\', \'No\', \'No\', 0, 0),
                (\'blockviewed\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'blockviewed\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'blockviewed\', \'link-to-product-page\', \'Yes\', \'Yes\', 0, 0),
                (\'blockviewed\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'blockviewed\', \'message\', \'\', \'\', 0, 0),
                (\'homefeatured\', \'thumb-image\', \'large\', \'large\', 0, 0),
                (\'homefeatured\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'homefeatured\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'homefeatured\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'homefeatured\', \'height\', \'auto\', \'auto\', 0, 0),
                (\'homefeatured\', \'orientation\', \'horizontal\', \'horizontal\', 0, 0),
                (\'homefeatured\', \'arrows\', \'No\', \'No\', 0, 0),
                (\'homefeatured\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'homefeatured\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'homefeatured\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'homefeatured\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'homefeatured\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'homefeatured\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'homefeatured\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'homefeatured\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'homefeatured\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'homefeatured\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'homefeatured\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'homefeatured\', \'caption\', \'No\', \'No\', 0, 0),
                (\'homefeatured\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'homefeatured\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'homefeatured\', \'link-to-product-page\', \'Yes\', \'Yes\', 0, 0),
                (\'homefeatured\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'homefeatured\', \'message\', \'\', \'\', 0, 0),
                (\'homeslideshow\', \'thumb-image\', \'original\', \'original\', 1, 1),
                (\'homeslideshow\', \'selector-image\', \'small\', \'small\', 0, 0),
                (\'homeslideshow\', \'large-image\', \'original\', \'original\', 1, 1),
                (\'homeslideshow\', \'width\', \'auto\', \'auto\', 0, 0),
                (\'homeslideshow\', \'height\', \'60%\', \'60%\', 1, 1),
                (\'homeslideshow\', \'orientation\', \'horizontal\', \'horizontal\', 0, 0),
                (\'homeslideshow\', \'arrows\', \'Yes\', \'Yes\', 1, 1),
                (\'homeslideshow\', \'loop\', \'Yes\', \'Yes\', 0, 0),
                (\'homeslideshow\', \'effect\', \'slide\', \'slide\', 0, 0),
                (\'homeslideshow\', \'effect-speed\', \'600\', \'600\', 0, 0),
                (\'homeslideshow\', \'effect-easing\', \'ease\', \'ease\', 0, 0),
                (\'homeslideshow\', \'autoplay\', \'Yes\', \'Yes\', 0, 0),
                (\'homeslideshow\', \'slide-duration\', \'6000\', \'6000\', 0, 0),
                (\'homeslideshow\', \'shuffle\', \'No\', \'No\', 0, 0),
                (\'homeslideshow\', \'kenburns\', \'No\', \'No\', 0, 0),
                (\'homeslideshow\', \'pause\', \'Yes\', \'Yes\', 0, 0),
                (\'homeslideshow\', \'selectors-style\', \'bullets\', \'bullets\', 0, 0),
                (\'homeslideshow\', \'selectors\', \'none\', \'none\', 0, 0),
                (\'homeslideshow\', \'selectors-size\', \'45\', \'45\', 0, 0),
                (\'homeslideshow\', \'selectors-eye\', \'Yes\', \'Yes\', 0, 0),
                (\'homeslideshow\', \'bullets-preview\', \'top\', \'top\', 0, 0),
                (\'homeslideshow\', \'selectors-fill\', \'No\', \'No\', 0, 0),
                (\'homeslideshow\', \'caption\', \'Yes\', \'Yes\', 1, 1),
                (\'homeslideshow\', \'fullscreen\', \'No\', \'No\', 0, 0),
                (\'homeslideshow\', \'preload\', \'Yes\', \'Yes\', 0, 0),
                (\'homeslideshow\', \'keyboard\', \'Yes\', \'Yes\', 0, 0),
                (\'homeslideshow\', \'show-loader\', \'Yes\', \'Yes\', 0, 0),
                (\'homeslideshow\', \'autostart\', \'Yes\', \'Yes\', 0, 0),
                (\'homeslideshow\', \'enable-effect\', \'No\', \'No\', 1, 1),
                (\'homeslideshow\', \'show-message\', \'No\', \'No\', 0, 0),
                (\'homeslideshow\', \'message\', \'\', \'\', 0, 0)';
        if (!$this->isPrestaShop16x) {
            $sql = preg_replace('/\r\n\s*..(?:blockbestsellers_home|blocknewproducts_home|blockspecials_home)\b[^\r]*+/i', '', $sql);
            $sql = rtrim($sql, ',');
        }
        return Db::getInstance()->Execute($sql);
    }

    public function getBlocks()
    {
        $blocks = array(
            'default' => 'Default settings',
            'product' => 'Product page',
            'blocknewproducts' => 'New products sidebar',
            'blocknewproducts_home' => 'New products block',
            'blockbestsellers' => 'Bestsellers sidebar',
            'blockbestsellers_home' => 'Bestsellers block',
            'blockspecials' => 'Specials sidebar',
            'blockspecials_home' => 'Specials block',
            'blockviewed' => 'Viewed sidebar',
            'homefeatured' => 'Featured block',
            'homeslideshow' => 'Home page/custom slideshow'
        );
        if (!$this->isPrestaShop16x) {
            unset($blocks['blockbestsellers_home'], $blocks['blocknewproducts_home'], $blocks['blockspecials_home']);
        }
        if ($this->isPrestaShop17x) {
            unset($blocks['blocknewproducts'], $blocks['manufacturer'], $blocks['blockspecials'], $blocks['blockbestsellers'], $blocks['blockviewed']);
        }
        return $blocks;
    }

    public function getMessages()
    {
        return array(
            'default' => array(
            ),
            'product' => array(
            ),
            'blocknewproducts' => array(
            ),
            'blocknewproducts_home' => array(
            ),
            'blockbestsellers' => array(
            ),
            'blockbestsellers_home' => array(
            ),
            'blockspecials' => array(
            ),
            'blockspecials_home' => array(
            ),
            'blockviewed' => array(
            ),
            'homefeatured' => array(
            ),
            'homeslideshow' => array(
            )
        );
    }

    public function getParamsMap()
    {
        $map = array(
            'default' => array(
                'General' => array(
                    'include-headers-on-all-pages' => true
                ),
                'Image type' => array(
                    'thumb-image' => true,
                    'selector-image' => true,
                    'large-image' => true
                ),
                'Common settings' => array(
                    'width' => true,
                    'height' => true,
                    'orientation' => true,
                    'arrows' => true,
                    'loop' => true,
                    'effect' => true,
                    'effect-speed' => true,
                    'effect-easing' => true
                ),
                'Autoplay' => array(
                    'autoplay' => true,
                    'slide-duration' => true,
                    'shuffle' => true,
                    'kenburns' => true,
                    'pause' => true
                ),
                'Selectors' => array(
                    'selectors-style' => true,
                    'selectors' => true,
                    'selectors-size' => true,
                    'selectors-eye' => true,
                    'bullets-preview' => true,
                    'selectors-fill' => true
                ),
                'Caption' => array(
                    'caption' => true
                ),
                'Other settings' => array(
                    'fullscreen' => true,
                    'preload' => true,
                    'keyboard' => true,
                    'show-loader' => true,
                    'autostart' => true
                ),
                'Miscellaneous' => array(
                    'link-to-product-page' => true,
                    'show-message' => true,
                    'message' => true
                )
            ),
            'product' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'show-message' => false,
                    'message' => false
                )
            ),
            'blocknewproducts' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'link-to-product-page' => false,
                    'show-message' => false,
                    'message' => false
                )
            ),
            'blocknewproducts_home' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'link-to-product-page' => false,
                    'show-message' => false,
                    'message' => false
                )
            ),
            'blockbestsellers' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'max-number-of-products' => true,
                    'link-to-product-page' => false,
                    'show-message' => false,
                    'message' => false
                )
            ),
            'blockbestsellers_home' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'max-number-of-products' => true,
                    'link-to-product-page' => false,
                    'show-message' => false,
                    'message' => false
                )
            ),
            'blockspecials' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'link-to-product-page' => false,
                    'show-message' => false,
                    'message' => false
                )
            ),
            'blockspecials_home' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'link-to-product-page' => false,
                    'show-message' => false,
                    'message' => false
                )
            ),
            'blockviewed' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'link-to-product-page' => false,
                    'show-message' => false,
                    'message' => false
                )
            ),
            'homefeatured' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'link-to-product-page' => false,
                    'show-message' => false,
                    'message' => false
                )
            ),
            'homeslideshow' => array(
                'Enable effect' => array(
                    'enable-effect' => true
                ),
                'Slideshow images' => array(
                ),
                'Image type' => array(
                    'thumb-image' => false,
                    'selector-image' => false,
                    'large-image' => false
                ),
                'Common settings' => array(
                    'width' => false,
                    'height' => false,
                    'orientation' => false,
                    'arrows' => false,
                    'loop' => false,
                    'effect' => false,
                    'effect-speed' => false,
                    'effect-easing' => false
                ),
                'Autoplay' => array(
                    'autoplay' => false,
                    'slide-duration' => false,
                    'shuffle' => false,
                    'kenburns' => false,
                    'pause' => false
                ),
                'Selectors' => array(
                    'selectors-style' => false,
                    'selectors' => false,
                    'selectors-size' => false,
                    'selectors-eye' => false,
                    'bullets-preview' => false,
                    'selectors-fill' => false
                ),
                'Caption' => array(
                    'caption' => false
                ),
                'Other settings' => array(
                    'fullscreen' => false,
                    'preload' => false,
                    'keyboard' => false,
                    'show-loader' => false,
                    'autostart' => false
                ),
                'Miscellaneous' => array(
                    'show-message' => false,
                    'message' => false
                )
            )
        );
        if (!$this->isPrestaShop16x) {
            unset($map['blockbestsellers_home'], $map['blocknewproducts_home'], $map['blockspecials_home']);
        }
        if ($this->isPrestaShop17x) {
            unset($map['blocknewproducts'], $map['manufacturer'], $map['blockspecials'], $map['blockbestsellers'], $map['blockviewed']);
        }
        return $map;
    }

    public function gebugVars($smarty = null)
    {
        if ($smarty === null) {
            $smarty = &$GLOBALS['smarty'];
        }
        $result = array();
        $vars = $smarty->{$this->getTemplateVars}();
        if (is_array($vars)) {
            foreach ($vars as $key => $value) {
                $result[$key] = gettype($value);
            }
        } else {
            $result = gettype($vars);
        }
        return $result;
    }
}

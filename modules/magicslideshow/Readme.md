# Magic Slideshow

## About

Display one image after another. Fade or slide, fast or slow, text or just images. It's your choice!

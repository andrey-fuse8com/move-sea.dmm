/**
* 2005-2017 Magic Toolbox
*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Magic Toolbox <support@magictoolbox.com>
*  @copyright Copyright (c) 2017 Magic Toolbox <support@magictoolbox.com>. All rights reserved
*  @license   https://www.magictoolbox.com/license/
*/

function mtDefer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { mtDefer(method) }, 50);
    }
}

mtDefer(function () {

    $(document).ready(function() {

        prestashop.on('updatedProduct', function (resp) {
        });
    });
});

function mtCreateSelectorContainer() {
    var productId = $('#product_page_product_id').val();

    switch(mtLayout) {
        case 'original':
            $('.mt-images-container .product-cover').after(
                '<div class="MagicToolboxSelectorsContainer js-qv-mask mask">'+
                  '<ul id="MagicToolboxSelectors'+productId+'" class="product-images js-qv-product-images">'+
                  '</ul>'+
                '</div>'
            );
            break;
        case 'bottom':
        case 'right':
            $('#content .MagicToolboxContainer').append(
                '<div class="MagicToolboxSelectorsContainer">'+
                  '<div id="MagicToolboxSelectors'+productId+'">'+
                  '</div>'+
                '</div>'
            );
            break;
        case 'top':
        case 'left':
            $('#content .MagicToolboxContainer').prepend(
                '<div class="MagicToolboxSelectorsContainer">'+
                  '<div id="MagicToolboxSelectors'+productId+'">'+
                  '</div>'+
                '</div>'
            );
            break;
        default:
            break;
    }
}

function mtGetPrimarySelector(with360) {
    var queries = [], selector = null;

    if (typeof(with360) == 'undefined') {
        with360 = true;
    }

    if (with360 && m360AsPrimaryImage) {
        queries.push('.m360-selector');
    }
    queries.push('[data-mt-selector-id="'+mtCombinationData.coverId+'"]');
    queries.push('[data-mt-selector-id]:first');
    if (with360 && !m360AsPrimaryImage) {
        queries.push('.m360-selector');
    }

    for (var i = 0; i < queries.length; i++) {
        selector = $(queries[i]);
        if (selector.length) {
            break;
        }
    }
    return selector;
}

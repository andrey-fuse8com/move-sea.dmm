/**
* 2005-2017 Magic Toolbox
*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Magic Toolbox <support@magictoolbox.com>
*  @copyright Copyright (c) 2017 Magic Toolbox <support@magictoolbox.com>. All rights reserved
*  @license   https://www.magictoolbox.com/license/
*/





//for blockcart module
$(document).ready(function() {
    var container = $('div.MagicToolboxContainer');
    if (!container.length) {
        return;
    }
    var containerOffset = container.offset();
    var topOffset = parseInt(containerOffset.top);
    var leftOffset = parseInt(containerOffset.left);
    $('#bigpic').css({'position': 'absolute', 'top': topOffset, 'left': leftOffset});
    $('#bigpic').offset({ top: topOffset, left: leftOffset});

    //NOTE: for including magic360 icon in standard template
    //$('#views_block').css('display', 'block');
    //var ulWidth = $('#thumbs_list_frame').css('width');
    //ulWidth = ulWidth.replace('px', '') - 0;
    //var liWidth = $('#thumbs_list_frame > li:first').css('width');
    //liWidth = liWidth.replace('px', '') - 0;
    //$('#thumbs_list_frame').width(ulWidth + liWidth);

});

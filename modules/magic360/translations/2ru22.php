<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{magic360}prestashop>magic360_220d9102465c9223781b8f99af48c154'] = 'Изображение водяного знака не является реальным гифом, пожалуйста, ПРЕОБРАЗУЙТЕ изображение.';
$_MODULE['<{magic360}prestashop>magic360_a8193d87501f2827804fe043fa48407c'] = 'Параметры по умолчанию загрузка текста';
$_MODULE['<{magic360}prestashop>magic360_5a8ed3ded00116613de43d894433e007'] = 'Настройки по умолчанию загрузка текста на весь экран';
$_MODULE['<{magic360}prestashop>magic360_4ca9e82057cd7af8dd2d618c351651d1'] = 'Настройки по умолчанию текст подсказки на рабочем столе';
$_MODULE['<{magic360}prestashop>magic360_ada1f6da1d258049e33ce8cd41058e4a'] = 'Настройки по умолчанию текст подсказки на iOS/Android устройствах';
$_MODULE['<{magic360}prestashop>magic360_1c601faf14329227caa0e44304e03637'] = 'Сообщение настроек по умолчанию (в Magic 360)';

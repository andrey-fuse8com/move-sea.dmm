####################################################### 
 
 Magic 360™ for Prestashop
 Bulk image upload scripts 
 
 Copyright 2015 Magic Toolbox 
 
####################################################### 
 
MASS IMAGE UPLOAD INSTRUCTIONS: 

Upload all your 360 spins in one shot using this bulk upload script.

1. Create a folder in the root of your Prestashop website. The folder can have any name, for example:

/magic360images/

2. Upload all your 360 spins to the folder you created. Each spin must be stored in its own folder, with a folder name matching the product ID. For example:

/magic360images/{product-id1}/01.jpg
/magic360images/{product-id1}/02.jpg
/magic360images/{product-id1}/03.jpg
etc.
/magic360images/{product-id2}/01.jpg
/magic360images/{product-id2}/02.jpg
/magic360images/{product-id2}/03.jpg
etc.

You don't need to name your image files in any special way - the spin will be built in alphanumeric order. Images should be .jpg files (PNG is not supported).

3. Download the latest version of Magic 360 for Prestashop.

4. Unzip the extension.

5. Find the bulk upload script:

bulk360.php

6. Upload the script to the root of your Prestashop site e.g.

http://your-website-url/bulk360.php

7. Open the script URL in your browser and click the button to execute it.

8. All your images will be copied into your Prestashop database. It can take about 1 second per 36 image spin.

9. Once the script has finished, it will show you a report of all the images copied.

10. You've finished! You might like to delete the images that you uploaded. You might also like to change the file name of the PHP script to avoid malicious usage. The same script can be used again next time you need to upload images.
 
 
Further information: 
 
https://www.magictoolbox.com/magic360/modules/prestashop/ 
 
Tech support: 
 
https://www.magictoolbox.com/contact/
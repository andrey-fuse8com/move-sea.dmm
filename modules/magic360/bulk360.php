<?php
/**
* 2005-2017 Magic Toolbox
*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Magic Toolbox <support@magictoolbox.com>
*  @copyright Copyright (c) 2017 Magic Toolbox <support@magictoolbox.com>. All rights reserved
*  @license   https://www.magictoolbox.com/license/
*/

defined('DS') or define('DS', DIRECTORY_SEPARATOR);
ini_set('max_execution_time', '120');

if (!file_exists(dirname(__FILE__).'/config/config.inc.php')) {
    echo 'Wrong path! Please run this script from Prestashop root directory!';
    return;
}

//NOTE: to load 'Tools' class
require(dirname(__FILE__).'/config/config.inc.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $message = uploadImages(Tools::getValue('images-dir', ''));
}

?>
<html>
    <head>
        <title>Magic360 upload images tool</title>
    </head>
    <body>
        <div style="width:600px; margin: 0 auto; text-align:center;">
<?php
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    ?>
            Type source images folder name and press button to import images to PrestaShop products<br /><br />
            <form method="post" action="bulk360.php">
                <input type="text" name="images-dir" placeholder="source images folder" value="magic360images"><br /><br />
                <input type="submit" name="formSubmit" value="Import images">
            </form>
    <?php
} else {
        echo $message.'<br /><br />';
        echo '<a href="'.htmlentities($_SERVER['REQUEST_URI']).'">Try again!</a>';
}
    ?>
        </div>
    </body>
</html>
<?php

function uploadImages($imagesDir = '')
{

    if (empty($imagesDir)) {
        return 'Path to images is not set!';
    }

    $pathToPrestashop = dirname(__FILE__);

    $isDbSettingsDefined = defined('_DB_SERVER_') && defined('_DB_USER_') && defined('_DB_PASSWD_') && defined('_DB_NAME_') && defined('_DB_PREFIX_');

    if (!$isDbSettingsDefined) {
        return 'Some DB settings is not defined! Please check your Prestashop config!';
    }

    define('SRC_PATH', $pathToPrestashop.DS.$imagesDir);
    define('DEST_PATH', $pathToPrestashop.DS.'img'.DS.'magic360');

    if (!file_exists(SRC_PATH)) {
        return 'Wrong path! Please set the correct path to images!';
    }

    if (!is_dir(DEST_PATH)) {
        if (!createDir(DEST_PATH)) {
            return 'Can\'t create path: "'.DEST_PATH.'" ! Please check permissions!';
        }
    }

    $productIds = array();
    $list = glob(SRC_PATH.'/*');
    $count = 0;
    if ($list !== false) {
        foreach ($list as $dir) {
            if (is_dir($dir)) {
                $files = glob($dir.'/*');
                if ($files !== false) {
                    $images = array();
                    foreach ($files as $file) {
                        if (preg_match('#\.(?:jpg|jpeg|png|gif)$#is', $file)) {
                            $images[] = basename($file);
                        }
                    }
                    natsort($images);
                    $productId = basename($dir);
                    $productIds[] = $productId;
                    removeImages($productId);
                    foreach ($images as $position => $image) {
                        addImage($image, $productId, $position);
                    }
                    $columns = count($images);
                    addColumns($productId, $columns);
                    $count += $columns;
                }
            }
        }
    }

    file_put_contents(DEST_PATH.'/magic360.txt', implode(", ", $productIds));

    $message = "{$count} images was added!<br />";
    if ($count) {
        $message .= "Now please regenerate thumbnails on the Magic 360 configuration page.";
    }

    return $message;
}

function createDir($dir, $base = '')
{
    if (!empty($base)) {
        $dir = preg_replace('#^'.preg_quote($base).'#is', '', $dir);
    }
    $subDirs = explode(DS, $dir);
    $_dir = '';
    if (!empty($base)) {
        $_dir = $base;
    }
    foreach ($subDirs as $subDir) {
        if (!Tools::strlen($subDir)) {
            continue;
        }
        $_dir .= DS.$subDir;
        if (!is_dir($_dir) && (!mkdir($_dir) || !chmod($_dir, 0755))) {
            return false;
        }
    }
    return true;
}

function removeImages($productId)
{
    $query = "SELECT * FROM "._DB_PREFIX_."magic360_images WHERE `id_product`={$productId}";
    $result = executeQuery($query);
    if (!empty($result)) {
        foreach ($result as $image) {
            $pattern = DEST_PATH.DS.$image['id_product'].'-'.$image['id_image'].'{.,-*.}jpg';
            //1-1-cart_default.jpg
            //1-1.jpg
            $files = glob($pattern, GLOB_BRACE);
            if (!empty($files) && count($files) > 1) {
                foreach ($files as $file) {
                    unlink($file);
                }
            }
        }
        $query = "DELETE FROM "._DB_PREFIX_."magic360_images WHERE `id_product`={$productId}";
        executeQuery($query);
        $query = "DELETE FROM "._DB_PREFIX_."magic360_columns WHERE `id_product`={$productId}";
        executeQuery($query);
    }
}

function addImage($srcFileName, $productId, $position)
{
    $query = "INSERT INTO "._DB_PREFIX_."magic360_images (`id_image`, `id_product`, `position`) VALUES (NULL, {$productId}, {$position})";
    $imageId = executeQuery($query);
    if ($imageId !== false) {
        copy(SRC_PATH.DS.$productId.DS.$srcFileName, DEST_PATH.DS."{$productId}-{$imageId}.jpg");
        chmod(DEST_PATH.DS."{$productId}-{$imageId}.jpg", 0777);
    }
}

function addColumns($productId, $columns)
{
    $query = "SELECT * FROM "._DB_PREFIX_."magic360_columns WHERE id_product={$productId}";
    $result = executeQuery($query);
    if (empty($result)) {
        $query = "INSERT IGNORE INTO "._DB_PREFIX_."magic360_columns (`id_product`, `columns`) VALUES ({$productId}, {$columns})";
        executeQuery($query);
    } else {
        $query = "UPDATE "._DB_PREFIX_."magic360_columns SET `columns`={$columns} WHERE `id_product`={$productId}";
        executeQuery($query);
    }
}

function executeQuery($query, $table = '')
{
    $link = mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_, _DB_NAME_);
    if (!$link) {
        return false;
    }
    if ($table) {
        $query = str_replace('_TABLE_', _DB_PREFIX_.$table, $query);
    }
    $result = mysqli_query($link, $query);
    if (!$result || preg_match('#^(?:UPDATE|DELETE|DROP)\b#is', $query)) {
        mysqli_close($link);
        return $result;
    }
    if (preg_match('#^INSERT\b#is', $query)) {
        $result = mysqli_insert_id($link);
        mysqli_close($link);
        return $result;
    }
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
    mysqli_free_result($result);
    mysqli_close($link);
    return $data;
}

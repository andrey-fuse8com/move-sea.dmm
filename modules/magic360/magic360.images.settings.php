<?php
/**
* 2005-2017 Magic Toolbox
*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Magic Toolbox <support@magictoolbox.com>
*  @copyright Copyright (c) 2017 Magic Toolbox <support@magictoolbox.com>. All rights reserved
*  @license   https://www.magictoolbox.com/license/
*/

$prestashop_base_dir = dirname(dirname(dirname(__FILE__)));
$admin_folder = '';
$files = scandir($prestashop_base_dir);
if (is_array($files)) {
    $files = array_diff($files, array('.', '..'));
    foreach ($files as $file) {
        $file_path = $prestashop_base_dir.DIRECTORY_SEPARATOR.$file;
        if (is_dir($file_path)) {
            $file_path = $file_path.DIRECTORY_SEPARATOR.'index.php';
            if (file_exists($file_path)) {
                $f_handle = fopen($file_path, 'r');
                $contents = fread($f_handle, filesize($file_path));
                fclose($f_handle);
                if (preg_match('#define\(\'(_)?PS_ADMIN_DIR(?(1)_)\'#', $contents)) {
                    $admin_folder = $file;
                    break;
                }
            }
        }
    }
}

/* NOTE: fix for PrestaShop Cloud store */
if (preg_match('#/www/ftp$#i', $prestashop_base_dir)) {
    $prestashop_base_dir = preg_replace('#/www/ftp$#i', '/www', $prestashop_base_dir);
    $admin_folder = 'backoffice';
}

if (!$admin_folder) {
    return;
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!isset($_SERVER['HTTP_REFERER'])) {
        return;
    }
} else {
    ini_set('upload_max_filesize', '40M');
    ini_set('max_execution_time', '120');
    ini_set('post_max_size', '40M');
    ini_set('max_file_uploads', '72');
}

/*
NOTE: in some cases, the constant __PS_BASE_URI__ depends on $_SERVER['SCRIPT_NAME']
      this results in an incorrect links, because $_SERVER['SCRIPT_NAME'] = /modules/magic360/magic360.images.settings.php
      this is spike resolve it
*/
$_SERVER['SCRIPT_NAME'] = "/{$admin_folder}/index.php";
$_SERVER['SCRIPT_FILENAME'] = $_SERVER['DOCUMENT_ROOT']."/{$admin_folder}/index.php";

$admin_path = $prestashop_base_dir.DIRECTORY_SEPARATOR.$admin_folder;

chdir($admin_path);
define('_PS_ADMIN_DIR_', getcwd());
define('PS_ADMIN_DIR', _PS_ADMIN_DIR_);
include(PS_ADMIN_DIR.'/../config/config.inc.php');
if (file_exists(PS_ADMIN_DIR.'/functions.php')) {
    include(PS_ADMIN_DIR.'/functions.php');
}

$pid = (int)Tools::getValue('id_product', 0);

$theme = Tools::getValue('theme', '');
$shopId = Tools::getValue('id_shop', '');

$imageTypeSuffix = version_compare(_PS_VERSION_, '1.5.1.0', '>=') ? '_default' : '';

/* jQuery */
$files = glob(_PS_ROOT_DIR_.'/js/jquery/jquery-?.{?,??}.{?,??}.{pack,min}.js', GLOB_BRACE);
if (!empty($files)) {
    $jQueryURI = __PS_BASE_URI__.'js/jquery/'.basename($files[0]);
} else {
    $jQueryURI = '//code.jquery.com/jquery-1.7.2.min.js';
}
$jQueryVersion = array();
preg_match('#\d++(?:\.\d++)++#', basename($jQueryURI), $jQueryVersion);
$jQueryVersion = $jQueryVersion[0];
$jQueryMigrateURI = null;
if (version_compare('1.9', $jQueryVersion, '<=')) {
    $files = glob(_PS_ROOT_DIR_.'/js/jquery/jquery-migrate-?.{?,??}.{?,??}.{pack,min}.js', GLOB_BRACE);
    if (!empty($files)) {
        $jQueryMigrateURI = __PS_BASE_URI__.'js/jquery/'.basename($files[0]);
    } else {
        $jQueryMigrateURI = '//code.jquery.com/jquery-migrate-1.2.1.min.js';
    }
}

$imageFilePath = _PS_IMG_DIR_.'magic360/';
$imagesTypes = ImageType::getImagesTypes();

/* update images data */
$magic360ImagesData = Tools::getValue('magic360-images-data', array());
if (!empty($magic360ImagesData) && Tools::getValue('action', '') == 'applyChanges') {
    foreach ($magic360ImagesData as $imageId => $imageData) {
        if (isset($imageData['delete']) && (int)$imageData['delete']) {
            foreach ($imagesTypes as $k => $imageType) {
                @unlink($imageFilePath.$pid.'-'.$imageId.'-'.Tools::stripslashes($imageType['name']).'.jpg');
            }
            @unlink($imageFilePath.$pid.'-'.$imageId.'.jpg');
            Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'magic360_images` WHERE `id_image`='.(int)$imageId);
        } else {
            Db::getInstance()->Execute(
                'UPDATE `'._DB_PREFIX_.'magic360_images` SET '.
                '`position`='.(int)$imageData['position'].
                ' WHERE `id_image`='.(int)$imageId
            );
        }
    }
}

/* upload images */
if (!empty($_FILES) && isset($_FILES['magic360_upload_file']) && Tools::getValue('action', '') == 'uploadImages') {

    $isWatermarkActive = false;
    $watermarkSuffix = '';
    if (Module::isInstalled('watermark')) {
        $isWatermarkActive = Module::getInstanceByName('watermark')->active;
        if (version_compare(_PS_VERSION_, '1.5', '>=') && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $str_shop = '-'.(int)$shopId;
        } else {
            $str_shop = '';
        }
        $watermarkGif = _PS_MODULE_DIR_.'watermark/watermark'.$str_shop.'.gif';
        if (!file_exists($watermarkGif)) {
            $watermarkGif = _PS_MODULE_DIR_.'watermark/watermark.gif';
        }
        $config = Configuration::getMultiple(array('WATERMARK_Y_ALIGN', 'WATERMARK_X_ALIGN', 'WATERMARK_TRANSPARENCY'));
        $yAlign = isset($config['WATERMARK_Y_ALIGN']) ? $config['WATERMARK_Y_ALIGN'] : '';
        $xAlign = isset($config['WATERMARK_X_ALIGN']) ? $config['WATERMARK_X_ALIGN'] : '';
        $transparency = isset($config['WATERMARK_TRANSPARENCY']) ? $config['WATERMARK_TRANSPARENCY'] : 60;
    }

    $imageResizeMethod = 'imageResize';
    /*
    NOTE: __autoload function in Prestashop 1.3.x leads to PHP fatal error because ImageManager class does not exists
          can not use class_exists('ImageManager', false) because ImageManager class will not load where it is needed
          so check version before
    */
    if (version_compare(_PS_VERSION_, '1.5', '>=') && class_exists('ImageManager') && method_exists('ImageManager', 'resize')) {
        $imageResizeMethod = array('ImageManager', 'resize');
    }

    $originalNames = $_FILES['magic360_upload_file']['name'];
    natsort($originalNames);
    $tmpNames = array();
    foreach ($originalNames as $i => $originalName) {
        $tmpNames[] = $_FILES['magic360_upload_file']['tmp_name'][$i];
    }

    foreach ($tmpNames as $tmp_name) {
        if (!empty($tmp_name) && file_exists($tmp_name)) {
            if (($tempName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) && move_uploaded_file($tmp_name, $tempName)) {
                $imageFileExt = 'jpg';
                $position = Db::getInstance()->getValue('SELECT MAX(position) as position FROM `'._DB_PREFIX_.'magic360_images` WHERE id_product='.$pid);
                $position = ($position === false) ? 0 : $position + 1;
                Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'magic360_images` (`id_image`, `id_product`, `position`) VALUES (NULL, '.$pid.', '.$position.')');
                $imageId = Db::getInstance()->Insert_ID();
                $imageFileName = $pid.'-'.$imageId;
                if (call_user_func($imageResizeMethod, $tempName, $imageFilePath.$imageFileName.'.'.$imageFileExt, null, null, $imageFileExt)) {
                    if ($isWatermarkActive) {
                        $watermarkedImage = $imageFilePath.$imageFileName.'-watermark.'.$imageFileExt;
                        if (is_file($watermarkedImage)) {
                            @unlink($watermarkedImage);
                        }
                        if (watermarkByImage($imageFilePath.$imageFileName.'.'.$imageFileExt, $watermarkGif, $watermarkedImage)) {
                            $watermarkSuffix = '-watermark';
                        }
                    }
                    foreach ($imagesTypes as $k => $imageType) {
                        call_user_func(
                            $imageResizeMethod,
                            $imageFilePath.$imageFileName.$watermarkSuffix.'.'.$imageFileExt,
                            $imageFilePath.$imageFileName.'-'.Tools::stripslashes($imageType['name']).'.'.$imageFileExt,
                            $imageType['width'],
                            $imageType['height'],
                            $imageFileExt
                        );
                    }
                }
            }
            @unlink($tempName);
        }
    }
}

$images = Db::getInstance()->ExecuteS('SELECT id_image, position FROM `'._DB_PREFIX_.'magic360_images` WHERE id_product='.$pid.' ORDER BY position');
$imagesCount = count($images);

$columns = Db::getInstance()->getValue('SELECT columns FROM `'._DB_PREFIX_.'magic360_columns` WHERE id_product='.$pid);
$multiRows = false;
$rowsNumber = 1;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $columnsNumber = $imagesCount;
    $magic360Columns = Tools::getValue('magic360-columns', '');
    if (!empty($magic360Columns) && Tools::getValue('action', '') == 'save' && (int)$magic360Columns < $imagesCount) {
        $columnsNumber = (int)$magic360Columns;
    }
    if ($columns === false) {
        Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'magic360_columns` (`id_product`, `columns`) VALUES ('.$pid.', '.$columnsNumber.')');
    } else {
        Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'magic360_columns` SET `columns`='.$columnsNumber.' WHERE `id_product`='.$pid);
    }
} else {
    $columnsNumber = ($columns === false ? 0 : $columns);
}
if ($columnsNumber && ($columnsNumber < $imagesCount)) {
    $rowsNumber = floor($imagesCount / (int)$columnsNumber);
    $multiRows = true;
}

/* NOTE: spike for prestashop validator */
$GLOBALS['magictoolbox_temp_theme'] = $theme;
$GLOBALS['magictoolbox_temp_admin_folder'] = $admin_folder;
$GLOBALS['magictoolbox_temp_jQueryURI'] = $jQueryURI;
$GLOBALS['magictoolbox_temp_jQueryMigrateURI'] = $jQueryMigrateURI;
$GLOBALS['magictoolbox_temp_imagesCount'] = $imagesCount;
$GLOBALS['magictoolbox_temp_multiRows'] = $multiRows;
$GLOBALS['magictoolbox_temp_columnsNumber'] = $columnsNumber;
$GLOBALS['magictoolbox_temp_rowsNumber'] = $rowsNumber;
$GLOBALS['magictoolbox_temp_images'] = $images;
$GLOBALS['magictoolbox_temp_pid'] = $pid;
$GLOBALS['magictoolbox_temp_imageTypeSuffix'] = $imageTypeSuffix;
include dirname(__FILE__).'/magic360.images.settings.tpl.php';
unset($GLOBALS['magictoolbox_temp_theme']);
unset($GLOBALS['magictoolbox_temp_admin_folder']);
unset($GLOBALS['magictoolbox_temp_jQueryURI']);
unset($GLOBALS['magictoolbox_temp_jQueryMigrateURI']);
unset($GLOBALS['magictoolbox_temp_imagesCount']);
unset($GLOBALS['magictoolbox_temp_multiRows']);
unset($GLOBALS['magictoolbox_temp_columnsNumber']);
unset($GLOBALS['magictoolbox_temp_rowsNumber']);
unset($GLOBALS['magictoolbox_temp_images']);
unset($GLOBALS['magictoolbox_temp_pid']);
unset($GLOBALS['magictoolbox_temp_imageTypeSuffix']);

function watermarkByImage($imagepath, $watermarkpath, $outputpath)
{
    //global $xAlign, $yAlign, $transparency;
    $xAlign = $GLOBALS['xAlign'];
    $yAlign = $GLOBALS['yAlign'];
    $transparency = $GLOBALS['transparency'];
    $Xoffset = $Yoffset = $xpos = $ypos = 0;
    if (!$image = imagecreatefromjpeg($imagepath)) {
        return false;
    }
    if (!$imagew = imagecreatefromgif($watermarkpath)) {
        return false;
    }
    list($watermarkWidth, $watermarkHeight) = getimagesize($watermarkpath);
    list($imageWidth, $imageHeight) = getimagesize($imagepath);
    if ($xAlign == 'middle') {
        $xpos = $imageWidth / 2 - $watermarkWidth / 2 + $Xoffset;
    }
    if ($xAlign == 'left') {
        $xpos = 0 + $Xoffset;
    }
    if ($xAlign == 'right') {
        $xpos = $imageWidth - $watermarkWidth - $Xoffset;
    }
    if ($yAlign == 'middle') {
        $ypos = $imageHeight / 2 - $watermarkHeight / 2 + $Yoffset;
    }
    if ($yAlign == 'top') {
        $ypos = 0 + $Yoffset;
    }
    if ($yAlign == 'bottom') {
        $ypos = $imageHeight - $watermarkHeight - $Yoffset;
    }
    if (!imagecopymerge($image, $imagew, $xpos, $ypos, 0, 0, $watermarkWidth, $watermarkHeight, $transparency)) {
        return false;
    }
    return imagejpeg($image, $outputpath, 100);
}

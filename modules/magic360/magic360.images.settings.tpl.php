<?php
/**
* 2005-2017 Magic Toolbox
*
* NOTICE OF LICENSE
*
* This file is licenced under the Software License Agreement.
* With the purchase or the installation of the software in your application
* you accept the licence agreement.
*
* You must not modify, adapt or create derivative works of this source code
*
*  @author    Magic Toolbox <support@magictoolbox.com>
*  @copyright Copyright (c) 2017 Magic Toolbox <support@magictoolbox.com>. All rights reserved
*  @license   https://www.magictoolbox.com/license/
*/

$theme = $GLOBALS['magictoolbox_temp_theme'];
$admin_folder = $GLOBALS['magictoolbox_temp_admin_folder'];
$jQueryURI = $GLOBALS['magictoolbox_temp_jQueryURI'];
$jQueryMigrateURI = $GLOBALS['magictoolbox_temp_jQueryMigrateURI'];
$imagesCount = $GLOBALS['magictoolbox_temp_imagesCount'];
$multiRows = $GLOBALS['magictoolbox_temp_multiRows'];
$columnsNumber = $GLOBALS['magictoolbox_temp_columnsNumber'];
$rowsNumber = $GLOBALS['magictoolbox_temp_rowsNumber'];
$images = $GLOBALS['magictoolbox_temp_images'];
$pid = $GLOBALS['magictoolbox_temp_pid'];
$imageTypeSuffix = $GLOBALS['magictoolbox_temp_imageTypeSuffix'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
if (version_compare(_PS_VERSION_, '1.6', '<=')) {
    echo '<link type="text/css" rel="stylesheet" href="'.__PS_BASE_URI__.'css/admin.css" />';
    if (!empty($theme)) {
        echo '<link type="text/css" rel="stylesheet" href="'.__PS_BASE_URI__.$admin_folder.'/themes/'.$theme.'/admin.css" />';
    }
} else {
    echo '<link type="text/css" rel="stylesheet" href="'.__PS_BASE_URI__.'modules/magic360/views/css/admin.css" />';
}
?>
    <style type="text/css">
    body {
        background: none repeat scroll 0 0 #FFFFF0;
        margin: 5px;
    }
    #upload_list {
        border-spacing: 0px 2px;
        margin: 10px 0 10px 0;
        display: none;
    }
    tr.file-row {
        width: 500px;
        text-align: left;
        background-color: #F9F9F9;
    }
    tr.file-row td.file-info {
        border: 1px solid #CCCCCC;
        border-right: none;
        width: 430px;
        padding: 2px 5px;
    }
    tr.file-row td.action {
        border: 1px solid #CCCCCC;
        border-left: none;
        padding: 2px 5px;
    }
    span.file-info {
        line-height: 1.7em;
        display: block;
    }
    input.delete-button {
        cursor: pointer;
        width: 60px;
        text-align: center;
    }
    div.file-inputs-container {
        position: relative;
        width: 100px;
        height: 24px;
    }
    input.invisible {
        position: relative;
        text-align: right;
        -moz-opacity:0 ;
        filter:alpha(opacity: 0);
        opacity: 0;
        z-index: 2;
        width: 100%;
        height: 100%;
    }
    input.browse-button {
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 1;
        width: 100%;
        height: 100%;
    }
    input.hidden {
        position: absolute;
        left: -1000px;
    }
    div.cover {
        width: 500px;
        height: 24px;
        z-index: 1000;
        position: absolute;
        top: 0px;
        left: 100px;
    }
    .upload-button {
        width: 110px;
        /*height: 24px;*/
        z-index: 1001;
        position: absolute;
        top: 0px;
        left: 100px;
        margin-left: 5px;
    }
    /* NOTE: for old prestashop version */
    input[type="text"]:disabled {
        border:1px solid #ccc;
        background-color:#dedede;
        color: #444444;
    }

    </style>
    <script type="text/javascript" src="<?php echo $jQueryURI ?>"></script>
    <?php
    if ($jQueryMigrateURI) {
        ?>
        <script type="text/javascript" src="<?php echo $jQueryMigrateURI ?>"></script>
        <?php
    }
    ?>
    <script type="text/javascript">
    //<![CDATA[

    function MultiSelector(listContainer)
    {

        this.listContainer = listContainer;
        this.count = 0;
        this.id = 0;
        this.max = 50;

        this.addElement = function(element) {
            if (element.tagName == 'INPUT' && element.type == 'file') {
                element.multiSelector = this;
                element.onchange = function() {
                    this.className = 'hidden';
                    this.name = 'magic360_upload_file[]';
                    this.id = 'image_file-'+this.multiSelector.id;
                    this.multiSelector.id++;
                    if (typeof(this.files) != 'undefined') {
                        this.multiSelector.count += this.files.length;
                    } else {
                        this.multiSelector.count++
                    }
                    var newElement = document.createElement('input');
                    newElement.type = 'file';
                    newElement.className = 'invisible';
                    newElement.multiple = true;
                    newElement.accept = 'image/*';
                    this.parentNode.insertBefore(newElement, this.nextSibling);
                    this.multiSelector.addElement(newElement);
                    this.multiSelector.addListRow(this);
                    magicToolboxFixFrameHeight();
                };
                if (this.max && this.count >= this.max) {
                    element.disabled = true;
                };
                this.currentElement = element;
            } else {
                alert('Error: not a file input element');
            };
        };

        this.addListRow = function(element) {
            var row = document.createElement('tr');
            row.className = 'file-row';
            var cell = document.createElement('td');
            cell.className = 'file-info';
            if (typeof(element.files) != 'undefined') {
                for (var i = 0; i < element.files.length; i++) {
                    var info = document.createElement('span');
                    info.className = 'file-info';
                    var name = element.files[i].name;
                    if (name.length > 50) {
                        name = '...'+name.substr(name.length-50);
                    }
                    info.innerHTML = name;
                    cell.appendChild(info);
                }
            } else {
                var info = document.createElement('span');
                info.className = 'file-info';
                var name = element.value.replace(/^.*?([^\\]*)$/g, '$1');
                if (name.length > 50) {
                    name = '...'+name.substr(name.length-50);
                }
                info.innerHTML = name;
                cell.appendChild(info);
            }
            row.appendChild(cell);
            cell = document.createElement('td');
            cell.className = 'action';
            var button = document.createElement('input');
            button.type = 'button';
            button.className = 'button delete-button';
            button.element = element;
            if (typeof(element.files) != 'undefined') {
                button.filesLength = element.files.length;
            } else {
                button.filesLength = 1;
            }
            button.onclick = function() {
                // Remove element from form
                this.element.parentNode.removeChild(this.element);
                // Remove this row from the list
                this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
                // Decrement counter
                this.element.multiSelector.count -= this.filesLength;
                // Re-enable input element (if it's disabled)
                if (this.element.multiSelector.max && this.element.multiSelector.count < this.element.multiSelector.max) {
                    this.element.multiSelector.currentElement.disabled = false;
                };
                // Hide table if it has not rows
                if (this.element.multiSelector.count == 0) this.element.multiSelector.listContainer.style.display = 'none';
                magicToolboxFixFrameHeight();
                // Appease Safari
                //    without it Safari wants to reload the browser window
                //    which nixes your already queued uploads
                return false;
            };
            button.title = 'Remove';
            button.value = 'Remove';
            cell.appendChild(button);
            row.appendChild(cell);
            this.listContainer.appendChild(row);
            this.listContainer.style.display = 'block';
        };
    };


    function magicToolboxDeleteImage(imageId)
    {
        if (parseInt($('#delete-'+imageId).val())) {
            $('#row-'+imageId).css("background-color", "#FFFFF0");
            $('#delete-'+imageId).val(0).attr("alt", "Delete").attr("title", "Delete");
        } else {
            $('#row-'+imageId).css("background-color", "#EEEEE0");
            $('#delete-'+imageId).val(1).attr("alt", "Not delete").attr("title", "Not delete");
        }
    }

    function magicToolboxChangePosition(imageId, direction)
    {
        var currImageOrder = $('#position-'+imageId).val();
        var targetRowID, arrowPressed;
        var arrow = new Object();
        if (direction) {
            arrow.pressed = 'Down';
            arrow.notpressed = 'Up';
            targetRowID = $('#row-'+imageId).next().attr('id');
        } else {
            arrow.pressed = 'Up';
            arrow.notpressed = 'Down';
            targetRowID = $('#row-'+imageId).prev().attr('id');
        }
        var targetImageId = targetRowID.replace('row-', '');
        var targetImageOrder = $('#position-'+targetImageId).val();
        $('#position-'+targetImageId).val(currImageOrder);
        $('#position-'+imageId).val(targetImageOrder);
        if ($('#arrow'+arrow.pressed+'-'+targetImageId).css('display') == 'none') {
            $('#arrow'+arrow.pressed+'-'+targetImageId).css('display', 'inline');
            $('#arrow'+arrow.pressed+'-'+imageId).css('display', 'none');
        }
        if ($('#arrow'+arrow.notpressed+'-'+imageId).css('display') == 'none') {
            $('#arrow'+arrow.notpressed+'-'+imageId).css('display', 'inline');
            $('#arrow'+arrow.notpressed+'-'+targetImageId).css('display', 'none');
        }
        if (direction) {
            $('#row-'+imageId).before($('#row-'+targetImageId));
        } else {
            $('#row-'+imageId).after($('#row-'+targetImageId));
        }
    }

    function magicToolboxFixFrameHeight()
    {
        var frame = parent.document.getElementById(window.name);
        /*var heightMax = Math.max(
            document.body.scrollHeight,
            document.body.offsetHeight,
            document.body.clientHeight
            document.documentElement.offsetHeight,
            document.documentElement.clientHeight,
            document.documentElement.scrollHeight
        );*/

        //console.log('frame.height: ', typeof(frame.height)+' ', frame.height);
        //console.log('document.body.scrollHeight: ', document.body.scrollHeight);
        //console.log('document.body.offsetHeight: ', document.body.offsetHeight);
        //console.log('document.body.clientHeight: ', document.body.clientHeight);
        //console.log('document.documentElement.scrollHeight: ', document.documentElement.scrollHeight);
        //console.log('document.documentElement.offsetHeight: ', document.documentElement.offsetHeight);
        //console.log('document.documentElement.clientHeight: ', document.documentElement.clientHeight);

        if (document.documentElement.offsetHeight) {
            if (typeof($.browser) != 'undefined' && typeof($.browser.msie) != 'undefined' && $.browser.msie) {
                frame.height = document.documentElement.scrollHeight;
                //NOTE: while deleting pictures, height is larger than necessary after saving
                //NOTE: scroll up
                parent.document.documentElement.scrollTop = 0;
            } else {
                frame.height = document.documentElement.offsetHeight;
            }

            //NOTE: to hide scroll bar
            frame.height = Math.floor(frame.height) + 2;
        }

    }

    function checkAll(checkbox)
    {
        $(checkbox).attr('disabled', true);
        $('.delete-checkbox').click();
        $(checkbox).attr('disabled', false);
    }

    //]]>
    </script>
</head>
<body onload="magicToolboxFixFrameHeight()" >
<form id="magic360Form" action="<?php echo htmlentities($_SERVER['REQUEST_URI']) ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="admin_folder" id="admin_folder" value="<?php echo $admin_folder ?>" />
    <input type="hidden" name="action" id="action" value="save" />

    <?php
    if ($imagesCount > 0) {
        ?>
        <label style="text-align: left; padding-left: 10px;">Multi-row spin options</label><br/><br/>
        <table cellpadding="5" style="text-align: left; width: 500px">
        <tr>
            <td style="width:150" valign="top">Multi-row spin:</td>
            <td>
                <input type="checkbox" id="magic360-multi-rows" name="magic360-multi-rows"<?php echo $multiRows ? ' checked="checked"' : '' ?> onclick="if ($('#magic360-multi-rows').attr('checked')) { $('#magic360-columns').removeAttr('disabled'); $('#magic360-rows').removeAttr('disabled'); } else { $('#magic360-columns').attr('disabled', 'disabled'); $('#magic360-rows').attr('disabled', 'disabled'); }" />
            </td>
        </tr>
        <tr>
            <td style="width:150" valign="top">Number of images on X-axis</td>
            <td>
                <input type="text" id="magic360-columns" name="magic360-columns" size="10"<?php echo $multiRows ? '' : ' disabled="disabled"' ?> value="<?php echo $columnsNumber ?>" />
            </td>
        </tr>
        <tr>
            <td style="width:150" valign="top">Number of images on Y-axis</td>
            <td>
                <input type="text" id="magic360-rows" name="magic360-rows" size="10"<?php echo $multiRows ? '' : ' disabled="disabled"' ?> value="<?php echo $rowsNumber ?>" />
            </td>
        </tr>
        </table>
        <div style="width: 52px;">
            <input class="button" type="submit" id="save-button-first" value="Save" />
        </div>
        <hr style="width:100%;clear: both;">
        <div class="separation"></div>
        <label style="text-align: left; padding-left: 10px;">Magic360 Images</label><br/>
        <div style="margin: 10px 0 10px 0;">
        <table id="magic360_images" cellspacing="0" cellpadding="0" class="table">
        <thead>
            <tr>
            <th>Position</th>
            <th>Image</th>
            <th>Delete <input type="checkbox" title="Toggle all checkboxes" onclick="checkAll(this);" /></th>
            </tr>
        </thead>
        <tbody>
        <?php
        $upSrc = __PS_BASE_URI__.'img/admin/up.gif';
        $downSrc = __PS_BASE_URI__.'img/admin/down.gif';
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $upSrc = __PS_BASE_URI__.'img/admin/arrow_up.png';
            $downSrc = __PS_BASE_URI__.'img/admin/arrow_down.png';
        }
        foreach ($images as $key => $data) {
            ?>
            <tr id="row-<?php echo $data['id_image']; ?>">
            <td class="center">
                <a<?php echo ($key == 0 ? ' style="display: none;"' : '') ?> id="arrowUp-<?php echo $data['id_image'] ?>" href="#" onclick="magicToolboxChangePosition(<?php echo $data['id_image'] ?>, 0);return false;"><img src="<?php echo $upSrc ?>" alt="Up" title="Up" /></a><br />
                <a<?php echo (($key == $imagesCount - 1) ? ' style="display: none;"' : '') ?> id="arrowDown-<?php echo $data['id_image'] ?>" href="#" onclick="magicToolboxChangePosition(<?php echo $data['id_image'] ?>, 1);return false;"><img src="<?php echo $downSrc ?>" alt="Down" title="Down" /></a>
                <input type="hidden" name="magic360-images-data[<?php echo $data['id_image'] ?>][position]" id="position-<?php echo $data['id_image'] ?>" value="<?php echo $data['position'] ?>" />
            </td>
            <?php $imageName = $pid.'-'.$data['id_image'].'-home'.$imageTypeSuffix.'.jpg' ?>
            <td><img src="<?php echo _PS_IMG_.'magic360/'.$imageName ?>" alt="<?php echo $imageName ?>" title="<?php echo $imageName ?>" style="width: 60px; height: 60px;" /></td>
            <td class="center">
                <input class="delete-checkbox" type="checkbox" name="magic360-images-data[<?php echo $data['id_image'] ?>][delete]" id="delete-<?php echo $data['id_image'] ?>" onclick="magicToolboxDeleteImage(<?php echo $data['id_image'] ?>);" value="0" alt="Delete" title="Delete" />
            </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        </table>
        </div>
        <div style="width: 106px;">
            <input class="button" type="submit" id="save-button-second" onclick="$('#action').val('applyChanges');" value="Apply changes" />
        </div>
        <hr style="width:100%;clear: both;">
        <div class="separation"></div>
        <?php
    }
    ?>
    <label style="text-align: left; padding-left: 10px;">Images to upload</label><br/><br/>
    <table id="upload_list"></table>
    <div class="file-inputs-container">
        <input class="invisible" type="file" id="upload-files" multiple="multiple" accept="image/*" />
        <input class="button browse-button" type="button" value="Browse Files" />
        <div class="cover"></div>
        <input class="button upload-button" type="submit" id="save-button-third" onclick="$('#action').val('uploadImages');" value="Upload Images" />
    </div>
    <!--<input class="button" type="submit" id="save-button-third" onclick="$('#action').val('uploadImages');" value="Upload Images" />-->
    <script type="text/javascript">
    //<![CDATA[
        var ms = new MultiSelector(document.getElementById('upload_list'));
        ms.addElement(document.getElementById('upload-files'));
    //]]>
    </script>
</form>
</body>
</html>
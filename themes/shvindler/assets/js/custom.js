/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */
$(document).ready(function () {



    var page = $('body');


    var btnMobileMenu = $('.header-mobile-btn.open-menu, .header-mobile-btn.close-menu');
    btnMobileMenu.click(function(){
        if(page.hasClass('show-menu')) {
            page.removeClass('show-menu');
        }
        else {
            page.addClass('show-menu');
        }
    })

    if(page.attr('id') === 'product'){
        var target = $('.tabs');
        var bar = $('.js-product-bar');
        var targetPos = target.offset().top;
        var winHeight = $(window).height();
        var scrollToElem = targetPos - winHeight;
        $(window).scroll(function () {
            var winScrollTop = $(this).scrollTop();
            if (winScrollTop > scrollToElem) {
                if (bar.hasClass('.product-bar--show')) {
                    return false;
                }
                else {
                    bar.addClass('product-bar--show')
                }
            } else {
                bar.removeClass('product-bar--show')
            }
        });
    }
    else if (page.attr('id') === 'my-account') {
        var hash = window.location.hash;
        var user = $('#_desktop_user_info');
        $('a[href="'+hash+'"]').trigger('click');

        user.find('a').click(function(){
            window.location.href = $(this).attr('href');
            window.location.reload();
        });
    }

    $('.filter-mobile-btn').click(function () {
        $('body').addClass('show-filter');
    });

    resizeHeightProduct($('#js-product-list'), '.product-item strong');
    resizeHeightProduct($('#js-product-list'), '.product-item__description');

    $("#quantity_wanted").change(function(){
        $(".product_quantity-totalprice").html(parseInt($(this).val()) * parseFloat($(".product_quantity-totalprice").data('price')));
        $(".product_quantity-tax").html(Math.round(parseInt($("#quantity_wanted").val()) * parseFloat($(".product_quantity-tax").data('price'))));
    });

    /*prestashop.on('updateCart', function (event) {
        if (!event.reason
            || !event.reason.linkAction
            || event.reason.linkAction !== 'add-to-cart'
        ) {
            return;
        }
        var refreshURL = $('.blockcart').data('refresh-url');
        var requestData = {};

        if (event && event.reason) {
            requestData = {
                id_product_attribute: event.reason.idProductAttribute,
                id_product: event.reason.idProduct,
                action: event.reason.linkAction
            };
        }

        $.post(refreshURL, requestData).then(function (resp) {
            if (window.buy_now === 1){
                window.buy_now = 0;
                window.location.href = $($(".add-to-buy-now")[0]).data("url");
                return false;
            }
            $("body").append(resp.preview);
            $(".nm-menu").addClass('nm-menu--active');
            var menuClose = $('.nm-menu-close');
            var count_cart = $(".header__cart .header__cart-wrapper .nm-menu-cart-count");
            count_cart.html(parseInt(count_cart[0].innerText)+1);
            menuClose.click(function () { 
                $('.nm-menu').removeClass('nm-menu--active');        
            });
        }).fail(function (resp) {
            prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: resp});
        });
    });*/

    prestashop.on('updateCart', function (event) {
        if (!event.reason
            || !event.reason.linkAction
            || (event.reason.linkAction !== 'add-to-cart'
            && event.reason.cartAjax !== 'update-cart')
        ) {
            return;
        }
        var refreshURL = $('.header__cart-desktop').data('refresh-url');
        var requestData = {};

        if (event && event.reason) {
            requestData = {
                id_product_attribute: event.reason.idProductAttribute,
                id_product: event.reason.idProduct,
                action: event.reason.linkAction
            };
        }

        $.post(refreshURL, requestData).then(function (resp) {
            if (window.buy_now === 1){
                window.buy_now = 0;
                window.location.href = '/order';
                return false;
            } else {

                if (parseInt($("select[name=qty]").val()) > 0){
                    count = parseInt($("select[name=qty]").val());
                }

                $(".js-cart-custom").html(resp.preview);
                $(".nm-menu").addClass('nm-menu--active');
                var menuClose = $('.nm-menu-close');
                var count_cart = $(".nm-menu-cart-count .count");

                var html = $(".nm-menu").data('count');
                count_cart.html(html);
                menuClose.click(function () {
                    $('.nm-menu').removeClass('nm-menu--active');
                });
                show_cart();
            }

        }).fail(function (resp) {
            prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: resp});
        });
    });

    
});

function resizeHeightProduct(parent, child) {
    var tallestcolumn = 0;
    var product = parent.find(child);

    product.each(function () {
        currentHeight = $(this).height();
        if (currentHeight > tallestcolumn) {
            tallestcolumn = currentHeight
        }
    });
    product.height(tallestcolumn)
}
{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{if $page.page_name != 'my-account'}<div class="box column-40">{/if}
    {if $page.page_name=='authentication'}
        <p class="h1-mini margin-bottom-15">{l s='Already have an account?' d='Shop.Theme.Customeraccount'} <a href="{$urls.pages.authentication}">{l s='Log in instead!' d='Shop.Theme.Customeraccount'}</a></p>
    {elseif $page.page_name != 'my-account'}
        <p class="h1-mini margin-bottom-15">{$page.meta.title}</p>
    {/if}
    {block name='customer_form'}
        {block name='customer_form_errors'}
            {include file='_partials/form-errors.tpl' errors=$errors['']}
        {/block}
        <form action="{block name='customer_form_actionurl'}{$action}{/block}" id="customer-form"
              class="js-customer-form" method="post">
            <div class="form-content">
                {block "form_fields"}
                    {foreach from=$formFields item="field"}
                        {block "form_field"}
                            {form_field field=$field}
                        {/block}
                    {/foreach}
                    {$hook_create_account_form nofilter}
                {/block}
            </div>

            {block name='customer_form_footer'}
                <div class="form-footer clearfix">
                    <input type="hidden" name="submitCreate" value="1">
                    {block "form_buttons"}
                        <button class="btn btn--black form-control-submit" data-link-action="save-customer"
                                type="submit">
                            <span>{l s='Save' d='Shop.Theme.Actions'}</span>
                        </button>
                    {/block}
                </div>
            {/block}

        </form>
    {/block}

{if $page.page_name != 'my-account'}</div>{/if}
{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_content'}
    {block name='product_tabs'}
        <div class="tabs acccount-container box">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a
                            class="nav-link active"
                            data-toggle="tab"
                            href="#account"
                            role="tab"
                            aria-controls="account"
                            aria-selected="true"
                    ><span>{l s='Information' d='Shop.Theme.Customeraccount'}</span></a>
                </li>
                <li class="nav-item">
                    <a
                            class="nav-link"
                            data-toggle="tab"
                            href="#address"
                            role="tab"
                            aria-controls="address"
                            aria-selected="true"
                    ><span>{l s='Addresses' d='Shop.Theme.Customeraccount'}</span></a>
                </li>
                <li class="nav-item">
                    <a
                            class="nav-link"
                            data-toggle="tab"
                            href="#orders"
                            role="tab"
                            aria-controls="orders"
                            aria-selected="true"
                    ><span>{l s='Order history and details' d='Shop.Theme.Customeraccount'}</span></a>
                </li>
                <li class="nav-item">
                    <a
                            id="wishlist-link"
                            class="nav-link"
                            data-toggle="tab"
                            href="#wishlist"
                            role="tab"
                            aria-controls="wishlist"
                            aria-selected="true"
                    <span>
              {l s='My wishlists' mod='advansedwishlist'}
            </span>
                    </a>
                </li>
            </ul>

            <div class="tab-content" id="tab-content">
                <div class="tab-pane fade in active" id="account" role="tabpanel">
                    {render file='customer/_partials/customer-form.tpl' ui=$customer_form}
                </div>
                <div class="tab-pane fade in" id="address" role="tabpanel">
                    <!--Address-->
                    {foreach $customer.addresses as $address}
                        {block name='customer_address'}
                            {include file='customer/_partials/block-address.tpl' address=$address}
                        {/block}
                    {/foreach}
                    <div class="addresses-footer">
                        <a href="{$urls.pages.address}" data-link-action="add-address">
                            <i class="material-icons">&#xE145;</i>
                            <span>{l s='Create new address' d='Shop.Theme.Actions'}</span>
                        </a>
                    </div>
                </div>
                <div class="tab-pane fade in" id="orders" role="tabpanel">

                    <h6 class="h1-mini">{l s='Here are the orders you\'ve placed since your account was created.' d='Shop.Theme.Customeraccount'}</h6>

                    {if $orders}
                        <table class="table table-orders table-striped table-bordered table-labeled hidden-sm-down">
                            <thead class="thead-default">
                            <tr>
                                <th>{l s='Order reference' d='Shop.Theme.Checkout'}</th>
                                <th>{l s='Date' d='Shop.Theme.Checkout'}</th>
                                <th>{l s='Total price' d='Shop.Theme.Checkout'}</th>
                                <th class="hidden-md-down">{l s='Payment' d='Shop.Theme.Checkout'}</th>
                                <th class="hidden-md-down">{l s='Status' d='Shop.Theme.Checkout'}</th>
                                <th colspan="2">{l s='Actions' d='Shop.Theme.Customeraccount'}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach from=$orders item=order}
                                <tr>
                                    <th scope="row">
                                        <a href="{$order.details.details_url}" data-link-action="view-order-details">
                                            {$order.details.reference}
                                        </a>
                                    </th>
                                    <td>{$order.details.order_date}</td>
                                    <td class="text-xs-right">{$order.totals.total.value}</td>
                                    <td class="hidden-md-down">{$order.details.payment}</td>
                                    <td>
              <span
                      class="label label-pill "
              >
                {$order.history.current.ostate_name}
              </span>
                                    </td>
                                    <td class="text-center order-actions">
                                        <a href="{$order.details.details_url}" data-link-action="view-order-details">
                                            {l s='Details' d='Shop.Theme.Customeraccount'}
                                        </a>
                                    </td>
                                    <td class="text-center order-actions">
                                        {if $order.details.reorder_url}
                                            <a href="{$order.details.reorder_url}">{l s='Reorder' d='Shop.Theme.Actions'}</a>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                        <div class="orders hidden-md-up">
                            {foreach from=$orders item=order}
                                <div class="order">
                                    <div class="row">
                                        <div>
                                            <a href="{$order.details.details_url}"><h3>{$order.details.reference}</h3>
                                            </a>
                                            <div class="date">{$order.details.order_date}</div>
                                            <div class="total">{$order.totals.total.value}</div>
                                            <div class="status">
                <span
                        class="label label-pill"
                >
                  {$order.history.current.ostate_name}
                </span>
                                            </div>
                                        </div>
                                        <div class="column-m-2 text-right">
                                            <div>
                                                <a href="{$order.details.details_url}"
                                                   data-link-action="view-order-details"
                                                   title="{l s='Details' d='Shop.Theme.Customeraccount'}">
                                                    <i class="material-icons">&#xE8B6;</i>
                                                </a>
                                            </div>
                                            {if $order.details.reorder_url}
                                                <div>
                                                    <a href="{$order.details.reorder_url}"
                                                       title="{l s='Reorder' d='Shop.Theme.Actions'}">
                                                        <i class="material-icons">&#xE863;</i>
                                                    </a>
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    {/if}
                </div>
                <div class="tab-pane fade in" id="wishlist" role="tabpanel">
                    {hook h='displayMyWishlist'}
                </div>
            </div>
        </div>
    {/block}

{/block}


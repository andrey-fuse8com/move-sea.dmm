{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='content'}
    <div class="container catalog-title" id="js-product-list-header">
        <div class="row row-center">
            {block name='product_list_header'}
                <h1 id="js-product-list-header">{$listing.label}</h1>
            {/block}
        </div>
    </div>
    {if $listing.products|count}
        <div class="container">

            <div class="row">
                <button class="btn btn--black filter-mobile-btn">
                    <span>{l s='Filter' d='Shop.Theme.Actions'}</span>
                </button>
                {block name='sort_by'}
                    {include file='catalog/_partials/sort-orders.tpl' sort_orders=$listing.sort_orders}
                {/block}
            </div>

        </div>
        <div class="container">
            <div class="row">
                <div class="catalog">
                    {if isset($product)}
                        {if (Configuration::get('ACCESS_PRICES_USERS_CATEGORIES') != "on" || (!$customer.is_guest && isset($access_prices[$customer.id_default_group][$product.id_category_default]) && $access_prices[$customer.id_default_group][$product.id_category_default]))}
                            <div class="catalog__left">
                                {hook h='displayLeftColumn'}
                            </div>
                        {/if}
                    {/if}
                    <div class="catalog__list">
                        {block name='product_list'}
                            {include file='catalog/_partials/products.tpl' listing=$listing}
                        {/block}
                    </div>
                </div>
            </div>
        </div>
        <div class="filter-mobile">
            <a href="{$urls.base_url}" class="mobile-logo"><img src="{$shop.logo}"></a>
            <div class="filter-mobile__content">
                <button class="close"><i class="icon icon--close-black"></i></button>
            </div>
        </div>
    {else}
        {include file='errors/not-found-catalog.tpl'}
    {/if}
{/block}

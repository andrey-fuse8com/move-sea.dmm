
{block name='product_miniature_item'}

    <div class="product-item js-product-miniature {if  ($product.id==$current_product)}product-item-active{/if}" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
        <div class="product-item__content">
            {block name='displayProductListFunctionalButtons'}
                {hook h='displayProductListFunctionalButtons' product=$product}
            {/block}
            {if $product.related_image}
                <a href="{$product.url}" class="product-item__thumb">
                    <img
                            src = "{$product.related_image}"
                    >
                    <span class="product-item__thumb__price">{$product.price}</span>
                </a>
            {else}

            {/if}

        </div>
    </div>
{/block}
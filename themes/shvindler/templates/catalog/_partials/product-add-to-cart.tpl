{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/AFL-3.0
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
    * @copyright 2007-2018 PrestaShop SA
    * @license https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
    * International Registered Trademark & Property of PrestaShop SA
    *}
    <div class="product-add-to-cart">
        {block name='product_availability'}
        <span id="product-availability">
            {if $product.show_availability && $product.availability_message}
            {if $product.availability == 'available'}
            <i class="material-icons rtl-no-flip product-available">&#xE5CA;</i>
            {elseif $product.availability == 'last_remaining_items'}
            <i class="material-icons product-last-items">&#xE002;</i>
            {else}
            <i class="material-icons product-unavailable">&#xE14B;</i>
            {/if}
            {$product.availability_message}
            {/if}
        </span>
        {/block}


        {if !$configuration.is_catalog}
        {block name='product_quantity'}
        <div class="product-quantity clearfix">
            <div class="product_quantity-container">
                <span class="control-label">{l s='Quantity' d='Shop.Theme.Catalog'}</span>
                <div class="qty product_quantity-qty ">
                    <select type="text" name="qty" id="quantity_wanted" value="{$product.quantity_wanted}"
                        class="input-group product__card-input" min="{$product.minimal_quantity}"
                        aria-label="{l s='Quantity' d='Shop.Theme.Actions'}">
                        <option value="1" selected>1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                    </select>
                </div>
                <p>{l s='Total:' d='Shop.Theme.Catalog'}
                    <span data-price="{$product.price_amount}"
                        class="product_quantity-totalprice">{$product.price_amount}</span><br>
                </p>
                <p class="tax_add_product">
                    {$cart.labels.tax_short}
                </p>

            </div>

            <div class="add">
                <button class="add-to-cart" data-button-action="add-to-cart" type="submit" {if
                    !$product.add_to_cart_url} disabled {/if}>
                    <i class="shopping-cart_icon">
                        <img src="/themes/shvindler/assets/images/cart.png" alt=""></i>
                    {l s='Add to cart' d='Shop.Theme.Actions'}
                </button>
                <button class="add-to-buy-now button-play" data-button-action="add-to-cart" type="submit" {if
                    !$product.add_to_cart_url} disabled {/if} data-url="{$urls.pages.order}"
                    onclick="window.buy_now = 1;">
                    <i class="shopping-cart_icon">
                        <img src="/themes/shvindler/assets/images/play.png" alt=""></i>
                    {l s='Buy now' d='Shop.Theme.Actions'}
                </button>
            </div>
        </div>
        {/block}


        {block name='product_minimal_quantity'}
        <p class="product-minimal-quantity">
            {if $product.minimal_quantity > 1}
            {l
            s='The minimum purchase order quantity for the product is %quantity%.'
            d='Shop.Theme.Checkout'
            sprintf=['%quantity%' => $product.minimal_quantity]
            }
            {/if}
        </p>
        {/block}
        {/if}
    </div>
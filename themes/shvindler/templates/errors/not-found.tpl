{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<section id="content" class="page-content page-not-found">
  {block name='page_content'}

    <div class="image-block-animated"><img src="/img/cms/csm_PowerGear-USB-C-45-2_3f566e6ab4.jpg" alt="" style="margin-top:-267.25px;"></div>

    <div class="content-block">
      <div class="container">
        <div class="row">
          <div class="column-40">
            <h2>{l s='Sorry for the inconvenience.' d='Shop.Theme.Global'}</h2>
          </div>
        </div>
      </div>
    </div>

  {/block}
</section>

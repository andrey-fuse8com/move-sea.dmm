{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<div class="header__bar hidden-sm-down">
    <div class="container">
        <div class="row row-end row-middle">
            {hook h='displayNav2'}
            {hook h='displayNav3'}
        </div>
    </div>
</div>
<div class="open-menu header-mobile-btn hidden-md-up">
    <span></span>
</div>
<div class="header__logo">
    <a href="{$urls.base_url}"><img src="{$shop.logo}" alt="{$shop.name}"></a>
</div>
<div class="header__menu">
    <div class="close-menu header-mobile-btn header-mobile-btn--active hidden-md-up"><span></span></div>
    <nav>
        {hook h='displayTop'}
    </nav>
    <div class="hidden-md-up">
        {hook h='displayNav2'}
        {hook h='displayNav3'}
        {hook h='displayContacts'}
    </div>
</div>

{hook h='displayNavFullWidth'}

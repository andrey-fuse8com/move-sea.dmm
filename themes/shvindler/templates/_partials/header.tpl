{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/AFL-3.0
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
    * @copyright 2007-2018 PrestaShop SA
    * @license https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
    * International Registered Trademark & Property of PrestaShop SA
    *}

    <div class="header__bar hidden-sm-down">
        <div class="container">
            <div class="row row-end row-middle">
                {hook h='displayNav2'}
                {hook h='displaySign'}
                <div class="header__cart">
                    <a href="#" onclick="show_cart(); return false;">
                        <div class="header__cart-wrapper">
                            <img src="/themes/shvindler/assets/images/shoppingcart_icon2.png" alt="">
                            <p>
                                {l s='Cart' d='Shope.Theme.Catalog'}
                            </p>
                            <span class="nm-menu-cart-count count nm-count-zero">
                                {if $cart.products_count > 9}
                                {l s="9+" d='Shope.Theme.Catalog'}
                                {else}
                                {l s=$cart.products_count d='Shope.Theme.Catalog'}
                                {/if}
                            </span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-top-wrap">
        <div class="open-menu header-mobile-btn hidden-md-up">
            <span>&nbsp;</span>
            <span>&nbsp;</span>
            <span>&nbsp;</span>
            <span>&nbsp;</span>
        </div>

        <div class="header__logo">
            <a href="{$urls.base_url}"><img src="{$shop.logo}" alt="{$shop.name}"></a>
        </div>
        <div class="mobile-cart" onclick="show_cart()">
            <img src="/themes/shvindler/assets/images/shopping-cart.svg" alt="">
            <span class="nm-menu-cart-count count nm-count-zero">
                {if $cart.products_count > 9}
                {l s="9+" d='Shope.Theme.Catalog'}
                {else}
                {l s=$cart.products_count d='Shope.Theme.Catalog'}
                {/if}
            </span>
        </div>
    </div>



    <div class="header__menu">
        <div class="close-menu" onclick="closeMenu()">
            <span></span>
            <span></span>
        </div>
        <nav>
            {hook h='displayTop'}
        </nav>
        <div class="hidden-md-up">
            {hook h='displayNav2'}
            {hook h='displayContacts'}
        </div>

    </div>
    {hook h='displayNavFullWidth'}
    <script>
        function show_cart() {
            if ($("body>ul").hasClass("nm-menu")) {
                $(".nm-menu").addClass('active nm-menu--active');
                var menuClose = $('.nm-menu-close');
                menuClose.click(function () { $('.nm-menu').removeClass('nm-menu--active'); }); return false;
            } else {
                $.ajax({
                    type: 'GET',
                    url: '/module/ps_shoppingcart/ajax',
                    success: function (response) {
                        $("body").append(response.preview);
                        $(".nm-menu").addClass('active nm-menu--active');
                        var menuClose = $('.nm-menu-close');
                        menuClose.click(function () { $('.nm-menu').removeClass('nm-menu--active'); }); return false;
                    }
                });
            }
        }
        function closeMenu() {
            $("body").removeClass("show-menu");
        }
        $(document).ready(function () {
            prestashop.on('updateCart', function (event) {
                if (!event.reason
                    || !event.reason.linkAction
                    || (event.reason.linkAction !== 'add-to-cart'
                        && event.reason.cartAjax !== 'update-cart')
                ) {
                    return;
                }
                var refreshURL = $('.header__cart-desktop').data('refresh-url');
                var requestData = {};

                if (event && event.reason) {
                    requestData = {
                        id_product_attribute: event.reason.idProductAttribute,
                        id_product: event.reason.idProduct,
                        action: event.reason.linkAction
                    };
                }

                $.post(refreshURL, requestData).then(function (resp) {
                    if (window.buy_now === 1) {
                        window.buy_now = 0;
                        window.location.href = '/order';
                        return false;
                    } else {

                        if (parseInt($("select[name=qty]").val()) > 0) {
                            count = parseInt($("select[name=qty]").val());
                        }

                        $(".js-cart-custom").html(resp.preview);
                        $(".nm-menu").addClass('nm-menu--active');
                        var menuClose = $('.nm-menu-close');
                        var count_cart = $(".nm-menu-cart-count .count");

                        var html = $(".nm-menu").data('count');
                        count_cart.html(html);
                        menuClose.click(function () {
                            $('.nm-menu').removeClass('nm-menu--active');
                        });
                        show_cart();
                    }

                }).fail(function (resp) {
                    prestashop.emit('handleError', { eventType: 'updateShoppingCart', resp: resp });
                });
            });
        })
    </script>
{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{extends file=$layout}

{block name='head_seo' prepend}
    <link rel="canonical" href="{$product.canonical_url}">
{/block}

{block name='head' append}
    <meta property="og:type" content="product">
    <meta property="og:url" content="{$urls.current_url}">
    <meta property="og:title" content="{$page.meta.title}">
    <meta property="og:site_name" content="{$shop.name}">
    <meta property="og:description" content="{$page.meta.description}">
    <meta property="og:image" content="{$product.cover.large.url}">
    <meta property="product:pretax_price:amount" content="{$product.price_tax_exc}">
    <meta property="product:pretax_price:currency" content="{$currency.iso_code}">
    <meta property="product:price:amount" content="{$product.price_amount}">
    <meta property="product:price:currency" content="{$currency.iso_code}">
    {if isset($product.weight) && ($product.weight != 0)}
        <meta property="product:weight:value" content="{$product.weight}">
        <meta property="product:weight:units" content="{$product.weight_unit}">
    {/if}
{/block}
{block name='content'}

    <div class="container">
        <div class="row row-center">
            <div class="product column-80" itemscope itemtype="https://schema.org/Product">
                <meta itemprop="url" content="{$product.url}">

                <div class="product__title">
                    <h1 class="h1-title" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
                    <div class="h1-title-left">
                        <div class="trail-icon">
                            <strong class="trail-icon__text">
                                {$product.flag}
                                {foreach from=$product.flags item=flag}
                                    {$flag.label}
                                {/foreach}
                            </strong>
                        </div>
                        <hr>
                        <span>{l s='Reference' d='Shop.Theme.Catalog'}:&nbsp; {$product.reference_to_display}</span>
                    </div>
                    <div class="h1-title-right">
                        <hr>
                    </div>
                </div>

                <div class="product__main">
                    <div class="product__thumb">
                        {include file='catalog/_partials/product-cover-thumbnails.tpl'}
                    </div>
                    <div class="product__actions">
                        <div class="product__price">
                            {include file='catalog/_partials/product-prices.tpl'}
                            <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                                <input type="hidden" name="token" value="{$static_token}">
                                <input type="hidden" name="id_product" value="{$product.id}"
                                       id="product_page_product_id">
                                <input type="hidden" name="id_customization" value="{$product.id_customization}"
                                       id="product_customization_id">
                                {block name='product_add_to_cart'}
                                    {include file='catalog/_partials/product-add-to-cart.tpl'}
                                {/block}
                                {block name='product_refresh'}{/block}
                            </form>
                            {block name='product_minimal_quantity'}
                                <p class="product-minimal-quantity">
                                    {if $product.minimal_quantity > 1}
                                        {l
                                        s='The minimum purchase order quantity for the product is %quantity%.'
                                        d='Shop.Theme.Checkout'
                                        sprintf=['%quantity%' => $product.minimal_quantity]
                                        }
                                    {/if}
                                </p>
                            {/block}
                        </div>
                        <div class="product__availability">
                            {if $product.show_availability && $product.availability_message}
                                {if $product.availability == 'available'}
                                    {if $product.quantity > 0}
                                        <div class="diamond-green"><i class="icon icon--check-white"></i></div>
                                    {else}
                                        <div class="diamond-yellow"><i class="icon icon--warning-black"></i></div>
                                    {/if}
                                {elseif $product.availability == 'last_remaining_items'}
                                    <div class="diamond-yellow"><i class="icon icon--warning-black"></i></div>
                                {else}
                                    <div class="diamond-yellow"><i class="icon icon--warning-black"></i></div>
                                {/if}
                                {$product.availability_message}
                            {/if}
                            {hook h='displayProductAdditionalInfo' product=$product category=$category}
                        </div>
                    </div>
                </div>
                <div class="product__description copy-milli">
                    <h3 itemprop="product-name">{l s='Description' d='Shop.Theme.Catalog'}</h3>
                    {$product.description_short nofilter}
                </div>

                {block name='product_tabs'}
                    <div class="tabs margin-top-50">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a
                                        class="nav-link active"
                                        data-toggle="tab"
                                        href="#description"
                                        role="tab"
                                        aria-controls="product-details"
                                        aria-selected="true"
                                ><span>{l s='Product Details' d='Shop.Theme.Catalog'}</span></a>
                            </li>
                            {if $product.grouped_features}
                                <li class="nav-item">
                                    <a
                                            class="nav-link"
                                            data-toggle="tab"
                                            href="#datasheet"
                                            role="tab"
                                            aria-controls="datasheet"
                                    ><span>{l s='Data sheet' d='Shop.Theme.Catalog'}</span></a>
                                </li>
                            {/if}
                            {if $accessories}
                                <li class="nav-item">
                                    <a
                                            class="nav-link"
                                            data-toggle="tab"
                                            href="#accessories"
                                            role="tab"
                                            aria-controls="attachments"
                                    ><span>{l s='Accessories' d='Shop.Theme.Catalog'}</span></a>
                                </li>
                            {/if}
                            {if isset($product_manufacturer->id)}
                                <li class="nav-item">
                                    <a
                                            class="nav-link"
                                            data-toggle="tab"
                                            href="#manufacture"
                                            role="tab"
                                            aria-controls="product-details"
                                    ><span>{l s='Licences' d='Shop.Theme.Catalog'}</span></a>
                                </li>
                            {/if}
                            {if $product.attachments}
                                <li class="nav-item">
                                    <a
                                            class="nav-link"
                                            data-toggle="tab"
                                            href="#attachments"
                                            role="tab"
                                            aria-controls="attachments"><span>{l s='Attachments' d='Shop.Theme.Catalog'}</span></a>
                                </li>
                            {/if}
                            {foreach from=$product.extraContent item=extra key=extraKey}
                                <li class="nav-item">
                                    <a
                                            class="nav-link"
                                            data-toggle="tab"
                                            href="#extra-{$extraKey}"
                                            role="tab"
                                            aria-controls="extra-{$extraKey}"><span>{$extra.title}</span></a>
                                </li>
                            {/foreach}
                        </ul>

                        <div class="tab-content" id="tab-content">

                            <div class="collapse-menu" data-target="#description"
                                 data-toggle="collapse" aria-expanded="true"><strong>{l s='Product Details' d='Shop.Theme.Catalog'}</strong>
                                <hr>
                                <div class="collapse-menu__diamond">
                                    <div class="btn-diamond">
                                        <i class="icon icon--plus-white"></i><i class="icon icon--minus-white"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade in active collapse"  aria-expanded="false" id="description" role="tabpanel">
                                <div class="description">
                                    {$product.description nofilter}
                                </div>
                            </div>
                            {if $product.grouped_features}

                                <div class="collapse-menu" data-target="#datasheet"
                                     data-toggle="collapse" aria-expanded="true"><strong>{l s='Data sheet' d='Shop.Theme.Catalog'}</strong>
                                    <hr>
                                    <div class="collapse-menu__diamond">
                                        <div class="btn-diamond">
                                            <i class="icon icon--plus-white"></i><i class="icon icon--minus-white"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade collapse"  aria-expanded="false" id="datasheet" role="tabpanel">
                                    <div class="product__features">
                                        <dl class="data-sheet">
                                            {foreach from=$product.grouped_features item=feature}
                                                <dt class="name">{$feature.name}</dt>
                                                <dd class="value">{$feature.value|escape:'htmlall'|nl2br nofilter}</dd>
                                            {/foreach}
                                        </dl>
                                    </div>
                                </div>
                            {/if}
                            {if $accessories}

                                <div class="collapse-menu" data-target="#accessories"
                                     data-toggle="collapse" aria-expanded="true"><strong>{l s='Accessories' d='Shop.Theme.Catalog'}</strong>
                                    <hr>
                                    <div class="collapse-menu__diamond">
                                        <div class="btn-diamond">
                                            <i class="icon icon--plus-white"></i><i class="icon icon--minus-white"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade collapse"  aria-expanded="false" id="accessories" role="tabpanel">
                                    <div class="product__accessories">
                                        <table class="table-row">
                                            <tbody>
                                            {foreach from=$accessories item="product_accessory"}
                                                {include file='catalog/_partials/miniatures/product-accessories.tpl' product=$product_accessory}
                                            {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            {/if}

                            {if isset($product_manufacturer->id)}

                                <div class="collapse-menu" data-target="#manufacture"
                                     data-toggle="collapse"><strong>{l s='Licences' d='Shop.Theme.Catalog'}</strong>
                                    <hr>
                                    <div class="collapse-menu__diamond">
                                        <div class="btn-diamond">
                                            <i class="icon icon--plus-white"></i><i class="icon icon--minus-white"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade collapse"  aria-expanded="false" id="manufacture" role="tabpanel">
                                    <div class="product-manufacturer">
                                        {if isset($manufacturer_image_url)}
                                            <a href="{$product_brand_url}">
                                                <img src="{$manufacturer_image_url}"
                                                     class="img img-thumbnail manufacturer-logo"
                                                     alt="{$product_manufacturer->name}">
                                            </a>
                                        {else}
                                            <label class="label">{l s='Brand' d='Shop.Theme.Catalog'}</label>
                                            <span>
                                                <a href="{$product_brand_url}">{$product_manufacturer->name}</a>
                                              </span>
                                        {/if}
                                    </div>
                                </div>
                            {/if}



                            {block name='product_attachments'}
                                {if $product.attachments}

                                    <div class="collapse-menu" data-target="#attachments"
                                         data-toggle="collapse"><strong>{l s='Attachments' d='Shop.Theme.Catalog'}</strong>
                                        <hr>
                                        <div class="collapse-menu__diamond">
                                            <div class="btn-diamond">
                                                <i class="icon icon--plus-white"></i><i class="icon icon--minus-white"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade collapse"  aria-expanded="false" id="attachments" role="tabpanel">
                                        <div class="product-attachments">
                                            <table class="table-row">
                                                <thead>
                                                <tr class="table-row__row">
                                                    <td>{l s='Document' d='Shop.Theme.Actions'}</td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {foreach from=$product.attachments item=attachment}
                                                    <tr class="table-row__row">
                                                        <td class="table-row__title"><strong>{$attachment.name}</strong>
                                                        </td>
                                                        <td class="table-row__button">
                                                            <a href="{url entity='attachment' params=['id_attachment' => $attachment.id_attachment]}">
                                                                <strong>{l s='Download' d='Shop.Theme.Actions'}</strong>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                {/foreach}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                {/if}
                            {/block}

                            {foreach from=$product.extraContent item=extra key=extraKey}

                            <div class="collapse-menu" data-target="extra-{$extraKey}"
                                 data-toggle="collapse"><strong>{$extra.title}</strong>
                                <hr>
                                <div class="collapse-menu__diamond">
                                    <div class="btn-diamond">
                                        <i class="icon icon--plus-white"></i><i class="icon icon--minus-white"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade  {$extra.attr.class} collapse"  aria-expanded="false"  id="extra-{$extraKey}"
                                 role="tabpanel" {foreach $extra.attr as $key => $val} {$key}="{$val}"{/foreach}>
                            {$extra.content nofilter}
                        </div>
                        {/foreach}
                    </div>
                {/block}


                {block name='product_footer'}
                    {hook h='displayAFF' type='others'}
                    {hook h='displayFooterProduct' product=$product category=$category}
                    {hook h='displayRecom'}
                {/block}

                {block name='product_images_modal'}
                    {include file='catalog/_partials/product-images-modal.tpl'}
                {/block}
            </div>

        </div>
    </div>
    <div class="product-bar js-product-bar">
        <div class="container">
            <div class="row">
                <div class="product-bar__image">
                    <img src="{$product.cover.bySize.product_page.url}" alt="{$product.cover.legend}">
                </div>
                <div class="column-40"></div>
                <div class="column-60 product-bar__text">
                    <div class="product-bar__price">
                        <div class="current-price">
                            <span itemprop="price" content="{$product.price_amount}">{$product.price}</span>
                        </div>
                    </div>
                    <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                        <input type="hidden" name="token" value="{$static_token}">
                        <input type="hidden" name="id_product" value="{$product.id}"
                               id="product_page_product_id">
                        <input type="hidden" name="id_customization" value="{$product.id_customization}"
                               id="product_customization_id">
                        {block name='product_add_to_cart'}
                            {include file='catalog/_partials/product-add-to-cart.tpl'}
                        {/block}
                        {block name='product_refresh'}{/block}
                    </form>
                </div>
            </div>
        </div>
    </div>
{/block}

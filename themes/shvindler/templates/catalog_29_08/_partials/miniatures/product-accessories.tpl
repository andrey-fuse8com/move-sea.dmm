{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='product_miniature_item'}
    <tr class="table-row__row js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
        <td class="table-row__empty">
            {block name='displayProductListFunctionalButtons'}
                {hook h='displayProductListFunctionalButtons' product=$product}
            {/block}
        </td>
        <td class="table-row__img"><a href="{$product.url}"><img src="{$product.cover.bySize.home_default.url}" alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"></a></td>
        <td class="table-row__title"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></td>
        <td class="table-row__price">{$product.price}</td>
        <td class="table-row__button">
        <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                <input type="hidden" name="token" value="{$static_token}">
                <input type="hidden" name="id_product" value="{$product.id_product}" id="product_page_product_id">
                <input
                        type="hidden"
                        name="qty"
                        value="1"
                        min="{$product.minimal_quantity}"
                >
                <button
                        class="add-to-cart"
                        data-button-action="add-to-cart"
                        type="submit"
                        {if !$product.add_to_cart_url}
                            disabled
                        {/if}
                >
           {if !$product.add_to_cart_url}
                    <span>{$product.availability_message}</span>
              {else}
                    <strong>{l s='Add to cart' d='Shop.Theme.Actions'}</strong>
            {/if}
                </button>
                {block name='product_refresh'}{/block}
        </form>
       </td>
    </tr>
{/block}

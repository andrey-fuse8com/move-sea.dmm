{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='product_miniature_item'}
    <div class="product-item js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
        <div class="product-item__content">
            {block name='displayProductListFunctionalButtons'}
                {hook h='displayProductListFunctionalButtons' product=$product}
            {/block}
            {if $product.cover}
                <a href="{$product.url}" class="product-item__thumb">
                    <img
                            src = "{$product.cover.bySize.home_default.url}"
                            alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                            data-full-size-image-url = "{$product.cover.large.url}"
                    >
                </a>
            {else}
                <a href="{$product.url}" class="product-item__thumb">
                    <img
                            src = "{$urls.no_picture_image.bySize.home_default.url}"
                    >
                </a>
            {/if}
            <div class="product-item__description">
                <a href="{$product.url}"><strong>{$product.name|truncate:30:'...'}</strong></a>
                <span>{l s='Reference' d='Shop.Theme.Catalog'}:&nbsp; {$product.reference_to_display}</span>
                <p>{$product.description|truncate:150:'...' nofilter}</p>
            </div>

            {$access_prices = json_decode(Configuration::get('ACCESS_PRICES_USERS_CATEGORIES_ACCESS_PRICES'), true)}
            {if (Configuration::get('ACCESS_PRICES_USERS_CATEGORIES') != "on" || (!$customer.is_guest && isset($access_prices[$customer.id_default_group][$product.id_category_default]) && $access_prices[$customer.id_default_group][$product.id_category_default]))}
                <div class="product-item__price">
                    <span>{$product.price}</span>
                </div>
            {/if}

            <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                <input type="hidden" name="token" value="{$static_token}">
                <input type="hidden" name="id_product" value="{$product.id_product}" id="product_page_product_id">
                <input
                        type="hidden"
                        name="qty"
                        value="1"
                        min="{$product.minimal_quantity}"
                >
                {if (Configuration::get('ACCESS_PRICES_USERS_CATEGORIES') != "on" || (!$customer.is_guest && isset($access_prices[$customer.id_default_group][$product.id_category_default]) && $access_prices[$customer.id_default_group][$product.id_category_default]))}

                <button
                        class="btn btn--black add-to-cart"
                        data-button-action="add-to-cart"
                        type="submit"
                        {if !$product.add_to_cart_url}
                            disabled
                        {/if}
                >


                    {l s='Add to cart' d='Shop.Theme.Actions'}
                </button>
                {/if}
                {block name='product_refresh'}{/block}
            </form>
        </div>
    </div>
{/block}

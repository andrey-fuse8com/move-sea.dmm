<div class="nm-cart-panel-item-thumbnail">
    <div class="nm-cart-panel-thumbnail-wrap"><a href="{$product.url}">
            <img width="350" height="350"
                 src="{$product.cover.bySize.cart_default.url}"
                 alt=""
                 class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                 sizes="(max-width: 300px) 100vw, 300px"
            >
        </a></div>
</div>
<div class="nm-cart-panel-item-details">
    <div class="product-remove">
        <a  class="remove-from-cart remove remove_from_cart_button"
            rel                         = "nofollow"
            href                        = "{$product.remove_from_cart_url}"
            data-link-action            = "delete-from-cart"
            data-id-product             = "{$product.id_product}"
            data-id-product-attribute   = "{$product.id_product_attribute}"
            data-id-customization       = "{$product.id_customization}"
        >
            <i class="fas fa-times"></i>
        </a>
    </div>
    <a href="{$product.url}" class="nm-cart-panel-product-title">{$product.name|truncate:80:'...'}</a>
</div>
    <div class="product-quantity">
        <div class="nm-quantity-wrap">
            <label>{l s='Quantity' d='Shop.Theme.Catalog'}</label>
            <div class="quantity">
                <a class="remove-from-cart remove remove_from_cart_button"
                rel="nofollow"
                href="{$product.down_quantity_url}"
                data-link-action="delete-from-cart"
                data-cart-ajax="update-cart"
                data-id-product="{$product.id_product}"
                data-id-product-attribute="{$product.id_product_attribute}"
                data-id-customization="{$product.id_customization}">–</a>
                <span>
                    {$product.quantity}
                </span>
                <a class="remove-from-cart remove remove_from_cart_button"
                rel="nofollow"
                href="{$product.up_quantity_url}"
                data-link-action="delete-from-cart"
                data-cart-ajax="update-cart"
                data-id-product="{$product.id_product}"
                data-id-product-attribute="{$product.id_product_attribute}"
                data-id-customization="{$product.id_customization}">+</a>
            </div>
            <div class="nm-cart-panel-item-price">
                <span class="amount">{$product.price}</span>
            </div>
        </div>
    </div>

    
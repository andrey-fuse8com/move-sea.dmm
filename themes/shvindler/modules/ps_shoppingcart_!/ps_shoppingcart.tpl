<div class="nm-menu" data-count="{$cart.products_count}">
  <div class="nm-menu-close">
  <span>
    {l s='Close' d='Shope.Theme.Catalog'}    
  </span>
  </div>
  <div class="blockcart" data-refresh-url="{$refresh_url}">
    <div class="js-cart-source">
      <div class="cart-dropdown-wrapper">
        <div class="nm-cart-panel-list-wrap">
          {if $cart.products}
          <ul class="cart_list product_list_widget">
            {foreach from=$cart.products item=product}
            <li class="woocommerce-mini-cart-item mini_cart_item product_cart_{$product.id_product}">{include
              'module:ps_shoppingcart/ps_shoppingcart-product-line.tpl' product=$product}
              <div class="mini_cart_item-delete">
                <a class="remove-from-cart remove remove_from_cart_button" rel="nofollow"
                  href="{$product.remove_from_cart_url}" data-link-action="delete-from-cart"
                  data-cart-ajax="update-cart"
                  data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}"
                  data-id-customization="{$product.id_customization}" onclick="
                                                  $('.product_cart_{$product.id_product}').hide();
                                                  var count_cart = $('.header__cart .header__cart-wrapper .nm-menu-cart-count');
                                                  count_cart.html(parseInt(count_cart[0].innerText)-{$product.quantity});
                                                  return false;">
                  <span></span>
                  <span></span>
                </a>
              </div>
            </li>
            {/foreach}
          </ul>
          {else}
          <li class="empty">{l s='No products in the cart.' d='Shop.Theme.AJAXCart'}</li>
          {/if}
        </div>
        <div class="nm-cart-panel-summary">
          <div class="nm-cart-panel-summary-inner">
            {if $cart.products}
            <div class="subtotal" style="margin-bottom: 0;">
              {foreach from=$cart.subtotals item="subtotal"}
              {if $subtotal.label == "Доставка"}
              <strong>{$subtotal.label}:</strong>
              <span class="nm-cart-panel-summary-subtotal">{$subtotal.amount} руб.</span>
              {/if}
              {/foreach}
            </div>
            <div class="total">
              <strong>{$cart.totals.total.label}</strong>
              <span class="nm-cart-panel-summary-subtotal">{$cart.totals.total.value}</span>
            </div>
            <div class="buttons">
              <a href="/content/8-products" class="button border wc-backward btnCont">{l s='Continue shopping' d='Shop.Theme.Actions'}</a>
              <a href="{$cart_url}" class="button border wc-forward">{l s='Show cart' d='Shop.Theme.AJAXCart'}</a>
              <a href="{$urls.pages.order}" class="btnOrder button checkout wc-forward"
                class="button checkout wc-forward">{l s='Proceed to checkout' d='Shop.Theme.Actions'}</a>
            </div>
            {else}
            <div class="buttons">
              <a href="/content/8-products" class="button border wc-backward btnCont">{l s='Continue shopping' d='Shop.Theme.Actions'}</a>
            </div>
            {/if}
          </div>
        </div>
      </div>
    </div>
  </div>
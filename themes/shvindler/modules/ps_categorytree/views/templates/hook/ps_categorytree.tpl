{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{if $category.level_depth == 2 || $category.level_depth == 3}
    {function name="categories" nodes=[] depth=0}
        {strip}
            {if $nodes|count}
                <ul class="list-pictures-blocks">
                    {foreach from=$nodes item=node}
                        <li data-depth="{$depth}">
                            <a href="{$node.link}">
                                <img src="/img/c/{$node.id}.jpg" alt="{$node.name}" class="img-category" />
                                <span class="title-category">
                                    <img src="/img/c/{$node.id}_thumb.jpg" />
                                </span>
                            </a>
                        </li>
                    {/foreach}
                </ul>
            {/if}
        {/strip}
    {/function}
    <div class="content-block">
        <div class="container">
            <div class="row">
                <div class="column-100">
                    {categories nodes=$categories.children}
                </div>
            </div>
        </div>
    </div>
{else}
    {function name="categories" nodes=[] depth=0}
        {strip}
            {if $nodes|count}
                <ul>
                    {foreach from=$nodes item=node}
                        <li>
                            <a href="{$node.link}">{$node.name}</a>
                            <div>
                                {categories nodes=$node.children depth=$depth+1}
                            </div>
                        </li>
                    {/foreach}
                </ul>
            {/if}
        {/strip}
    {/function}
    <div class="category-tree">
        {if !empty($category)}
            <h5><a href="{$category.id}-{$category.link_rewrite nofilter}">{$category.name}</a></h5>
        {else}
            <h5><a href="{$categories.link nofilter}">{$categories.name}</a></h5>
        {/if}
        {categories nodes=$categories.children}
    </div>
{/if}
{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Snegurka <site@web-esse.ru>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
            <div id="mywishlist" class="column-12 {if $advansedwishlistis17 == 1}mywishlist_17{else}mywishlist_16{/if}">
                <script>
                    var baseDir = '{$base_dir|addslashes}';
                    var static_token = '{$static_token|addslashes}';
                    var isLogged = true;
                    var advansedwishlist_controller_url = '{$advansedwishlist_controller_url nofilter}';
                    var mywishlist_url = '{$mywishlist_url nofilter}';
                    var url_cart = "{$link->getModuleLink('ps_shoppingcart', 'ajax', array(), true)|escape:'html':'UTF-8'}";
                    {if $advansedwishlistis17 != 1}
                    var ps_ws_version = 'advansedwishlistis16';
                    {else}
                    var ps_ws_version = 'advansedwishlistis17';
                    {/if}
                    var single_mode = '{$single_mode}';
                </script>
                {if $advansedwishlistis17 != 1}
                    {capture name=path}
                        <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
                            {l s='My account' mod='advansedwishlist'}
                        </a>
                        <span class="navigation-pipe">
            {$navigationPipe|escape:'htmlall':'UTF-8'}
        </span>
                        <span class="navigation_page">
            {l s='My wishlists' mod='advansedwishlist'}
        </span>
                    {/capture}
                    <h1 class="page-heading">{l s='My wishlists' mod='advansedwishlist'}</h1>
                {/if}
                {if $id_customer|intval neq 0}
                    {if !$single_mode}
                        <form method="post" class="std box" id="form_wishlist">
                            <h3 class="h1-mini">{l s='New wishlist' mod='advansedwishlist'}</h3>
                            <div class="form-group">
                                <input type="hidden" name="token" value="{$token|escape:'html':'UTF-8'}"/>
                                <input type="text" placeholder="{l s='Name' mod='advansedwishlist'}" id="name"
                                       name="name" class="inputTxt form-control column-12"
                                       value="{if isset($smarty.post.name) and $errors|@count > 0}{$smarty.post.name|escape:'html':'UTF-8'}{/if}"/>
                            </div>
                            <div class="form-footer">
                                <button id="submitWishlist" class="btn btn--black form-control-submit" type="submit"
                                        name="submitWishlist">
                                    <span>{l s='Save' mod='advansedwishlist'}</span>
                                </button>
                            </div>
                        </form>
                    {/if}
                    {if $wishlists}
                        <div id="block-history" class="block-center box margin-top-10">
                            <table class="table table-bordered">
                                <thead>
                                <tr class="row row-center">
                                    <th class="column-m-2 column-2">{l s='Name' mod='advansedwishlist'}</th>
                                    <th class="column-m-1 column-1">{l s='Qty' mod='advansedwishlist'}</th>
                                    <th class="column-m-1 column-1">{l s='Viewed' mod='advansedwishlist'}</th>
                                    <th class="column-m-2 column-2">{l s='Created' mod='advansedwishlist'}</th>
                                    {if !$single_mode}
                                        <th class="column-m-2 column-2">{l s='Direct Link' mod='advansedwishlist'}</th>
                                        <th class="column-m-2 column-2">{l s='Default' mod='advansedwishlist'}</th>
                                        <th class="column-m-2 column-2">{l s='Delete' mod='advansedwishlist'}</th>
                                    {/if}
                                </tr>
                                </thead>
                                <tbody>
                                {section name=i loop=$wishlists}
                                    <tr id="wishlist_{$wishlists[i].id_wishlist|intval}"  class="row row-center">
                                        <td class="column-m-2 column-2">
                                            <a href="#"
                                               onclick="javascript:event.preventDefault();WishlistManage('block-order-detail', '{$wishlists[i].id_wishlist|intval}');">
                                                {$wishlists[i].name|truncate:30:'...'|escape:'htmlall':'UTF-8'}
                                            </a>
                                        </td>
                                        <td class="column-m-1 column-1">
                                            {assign var=n value=0}
                                            {foreach from=$nbProducts item=nb name=i}
                                                {if $nb.id_wishlist eq $wishlists[i].id_wishlist}
                                                    {assign var=n value=$nb.nbProducts|intval}
                                                {/if}
                                            {/foreach}
                                            {if $n}
                                                {$n|intval}
                                            {else}
                                                0
                                            {/if}
                                        </td>
                                        <td class="column-m-1 column-1">{$wishlists[i].counter|intval}</td>
                                        <td class="column-m-2 column-2">{$wishlists[i].date_add|date_format:"%Y-%m-%d"|escape:'htmlall':'UTF-8'}</td>
                                        {if !$single_mode}
                                            <td class="column-m-2 column-2">
                                                <a href="#"
                                                   onclick="javascript:event.preventDefault();WishlistManage('block-order-detail', '{$wishlists[i].id_wishlist|intval}');">
                                                    {l s='View' mod='advansedwishlist'}
                                                </a>
                                            </td>
                                            <td class="column-m-2 column-2 wishlist_default">
                                                {if isset($wishlists[i].default) && $wishlists[i].default == 1}
                                                    <p class="is_wish_list_default">
                                                        {if $wl_custom_font}
                                                            <span class="jms-heart-1"></span>
                                                        {else}
                                                            {if $advansedwishlistis17 == 1}
                                                                <i class="material-icons">assignment_turned_in</i>
                                                            {else}
                                                                <i class="icon icon-check-square"></i>
                                                            {/if}
                                                        {/if}
                                                    </p>
                                                {else}
                                                    <a href="#"
                                                       onclick="javascript:event.preventDefault();(WishlistDefault('wishlist_{$wishlists[i].id_wishlist|intval}', '{$wishlists[i].id_wishlist|intval}'));">
                                                        {if $wl_custom_font}
                                                            <span class="jms-compare-1"></span>
                                                        {else}
                                                            {if $advansedwishlistis17 == 1}
                                                                <i class="material-icons">check_box_outline_blank</i>
                                                            {else}
                                                                <i class="icon icon-square"></i>
                                                            {/if}
                                                        {/if}
                                                    </a>
                                                {/if}
                                            </td>
                                            <td class="column-m-2 column-2 wishlist_delete">
                                                <a class="icon" href="#"
                                                   onclick="javascript:event.preventDefault();return (WishlistDelete('wishlist_{$wishlists[i].id_wishlist|intval}', '{$wishlists[i].id_wishlist|intval}', '{l s='Do you really want to delete this wishlist ?' mod='advansedwishlist' js=1}'));">
                                                    {if $wl_custom_font}
                                                        <span class="jms-arrows-remove-1"></span>
                                                    {else}
                                                        {if $advansedwishlistis17 == 1}
                                                            <i class="material-icons">delete</i>
                                                        {else}
                                                            <span class="lnr lnr-circle-minus"></span>
                                                        {/if}
                                                    {/if}

                                                </a>
                                            </td>
                                        {/if}
                                    </tr>
                                {/section}
                                </tbody>
                            </table>
                        </div>
                        <div id="block-order-detail">&nbsp;</div>
                    {else}
                        {if $single_mode}
                            {l s='No products in the list' mod='advansedwishlist'}
                        {/if}
                    {/if}
                {/if}
            </div>


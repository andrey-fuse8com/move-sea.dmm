{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="content-block">
    <div class="container">
        <div class="row">
            <div class="column-40">
                <h1>
                    <span class="bold"> {l s='SUBSCRIBE TO' d='Shop.Theme.Global'}</span> {l s='NEWSLETTER' d='Shop.Theme.Global'}
                </h1>
            </div>
            <div class="column-60 newslatter-block">
                <p>{l s='Get our latest news and special sales' d='Shop.Theme.Global'}. </p>
                {if $conditions}
                    <p>{$conditions}</p>
                {/if}
                {if isset($id_module)}
                    {hook h='displayGDPRConsent' id_module=$id_module}
                {/if}
                <form action="{$urls.pages.index}#footer" method="post">
                    <div class="column-50">
                        <input
                                name="email"
                                type="email"
                                class="form-control"
                                value="{$value}"
                                placeholder="{l s='Your email address' d='Shop.Forms.Labels'}"
                                aria-labelledby="block-newsletter-label"
                        >
                        <input type="hidden" name="action" value="0">
                    </div>
                    <div class="column-50">
                        <button name="submitNewsletter" class="btn btn--black" type="submit">
                            {l s='Subscribe' d='Shop.Theme.Actions'}
                        </button>
                    </div>
                </form>
                {if $msg}
                    <p class="column-50 alert {if $nw_error}alert-danger{else}alert-success{/if}">
                        {$msg}
                    </p>
                {/if}
            </div>
        </div>
    </div>
</div>

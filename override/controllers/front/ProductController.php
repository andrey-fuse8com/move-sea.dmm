<?php
/**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */


class ProductController extends ProductControllerCore
{
    private function tryToGetAvailableIdProductAttribute($checkedIdProductAttribute)
    {
        if (!Configuration::get('PS_DISP_UNAVAILABLE_ATTR')) {
            $availableProductAttributes = array_filter(
                $this->product->getAttributeCombinations(),
                function ($elem) {
                    return $elem['quantity'] > 0;
                }
            );

            $availableProductAttribute = array_filter(
                $availableProductAttributes,
                function ($elem) use ($checkedIdProductAttribute) {
                    return $elem['id_product_attribute'] == $checkedIdProductAttribute;
                }
            );

            if (empty($availableProductAttribute) && count($availableProductAttributes)) {
                return (int) array_shift($availableProductAttributes)['id_product_attribute'];
            }
        }

        return $checkedIdProductAttribute;
    }

    private function getIdProductAttributeByRequest()
    {
        $requestedIdProductAttribute = (int) Tools::getValue('id_product_attribute');

        return $this->tryToGetAvailableIdProductAttribute($requestedIdProductAttribute);
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = FrontController::getBreadcrumbLinks();

        $categoryDefault = new Category($this->product->id_category_default, $this->context->language->id);

        if (!$categoryDefault->is_root_category) {
            $data = Db::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'mapl` WHERE product_id='.$categoryDefault->id);
        }

        foreach ($categoryDefault->getAllParents() as $category) {

            if ($category->id_parent != 0 && !$category->is_root_category && $category->id_parent==2) {

                $breadcrumb['links'][] = $this->getCategoryPath($category);
            }
        }
        if(!empty($data)){
            $pageurl= $this->context->link->getCMSLink($data['page_id']);
            $page=new CMS($data['page_id']);
            $breadcrumb['links'][] = array('title' =>$page->meta_title[$this->context->language->id], 'url' => $pageurl);
        }else{
            if (!$categoryDefault->is_root_category) {
                $breadcrumb['links'][] = $this->getCategoryPath($categoryDefault);
            }
        }

        $breadcrumb['links'][] = array(
            'title' => $this->product->name,
            'url' => $this->context->link->getProductLink($this->product, null, null, null, null, null, (int) $this->getIdProductAttributeByRequest()),
        );

        return $breadcrumb;
    }
}
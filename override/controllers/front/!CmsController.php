<?php
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;


//include_once _PS_MODULE_DIR_ . 'galslidereverywhere/galslidereverywhere.php';

require_once _PS_MODULE_DIR_ . 'ps_customtext/classes/CustomText.php';
class CmsController extends CmsControllerCore
{

    public function initContent()
    {
        /*parent::initContent();
        $post['content']=htmlentities($this->cms->content, null, 'utf-8');
        $post['content'] = str_replace("&nbsp;", "", $post['content']);
        $content = html_entity_decode($post['content']);
        $img_link="";
        $maincontent['text']=$content;
        $img_path="../../themes/carreratoys/carfeatures/images/";
        preg_match_all('/\{mainpagecategories\:[(0-9\,)]+\}/i', $content, $matches);
        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode(":", $v);
                $shortcode_id = str_replace('}', '', $explode[1]);
                $this->context->smarty->assign('category', $this->displayCat($shortcode_id));
                $replacement = $this->context->smarty->fetch( 'catalog/_partials/miniatures/categories.tpl');
                $content = str_replace('{mainpagecategories:' . $shortcode_id . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }


        preg_match_all('/\{getoverview\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';
        if (!empty($matches[0])) {

            foreach ($matches[0] as $k => $v) {
                $explode = explode(":", $v);
                $shortcode_id = str_replace('}', '', $explode[1]);
                $shortcode_ids = explode(",", $shortcode_id);
                $title = '';
                if (!empty($explode[2])) {
                    $title = str_replace('}', '', $explode[2]);
                }
                //**********************************************************************************************************Генерация товара

                for ($i = 0; $i < count($shortcode_ids); $i++) {
                    if (stristr($shortcode_ids[$i], 'title')) {
                        $exp = explode("|", $shortcode_ids[$i]);
                        $maincategory = $this->displayCat($exp[0]);
                        $maincategories[$i] = $maincategory[0];
                        $subcategory = $this->getSubCategories($exp[0]);
                        $subcategories[$i] = $subcategory;
                    } else
                        $maincategory = $this->displayCat($shortcode_ids[$i]);
                    $maincategories[$i] = $maincategory[0];
                    $subcategory = $this->getSubCategories($shortcode_ids[$i]);
                    $subcategories[$i] = $subcategory;

                }
                //**************************************************************************************************************************

                $this->context->smarty->assign('maincategories', $maincategories);
                $this->context->smarty->assign('title', $title);
                $this->context->smarty->assign('subcategories', $subcategories);
                $replacement .= $this->context->smarty->fetch("catalog/_partials/miniatures/overview.tpl");
                $content = str_replace($matches[0], $replacement, $content);
            }
            $maincontent['text'] = $content;
        }


        //****************ПОИСК ШОРТКОДОВ getproduct
        preg_match_all('/\{getproduct\:[(0-9\,)]+\}/i', $content, $matches);
        $replacement = '';
        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode(":", $v);
                $shortcode_id = str_replace('}', '', $explode[1]);
                //**********************************************************************************************************Генерация товара
                $products = $this->getProducts();
                $productout = array($products[3], $products[2], $products[1], $products[4], $products[5], $products[6]);
                $products = $productout;

                $this->context->smarty->assign('product', $products[1]);
                $replacement = $this->context->smarty->fetch("catalog/_partials/miniatures/product-home.tpl");
                //**************************************************************************************************************************
                $content = str_replace('{getproduct:' . $shortcode_id . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }

        //****************ПОИСК ШОРТКОДОВ partbanner
        preg_match_all('/\{getpartbanner\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';

        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode(":", $v);
                $shortcode = str_replace('}', '', $explode[1]);
                //**********************************************************************************************************Генерация баннера
                $parts = explode("|", $shortcode);
                $attrs1 = explode(",", $parts[0]);
                $attrs2 = explode(",", $parts[1]);
                $this->context->smarty->assign('attrs1', $attrs1);
                $this->context->smarty->assign('attrs2', $attrs2);
                $replacement = htmlspecialchars_decode($this->context->smarty->fetch("catalog/_partials/miniatures/getpartbanner.tpl"));
                //**************************************************************************************************************************
                $content = str_replace('{getpartbanner:' . $shortcode . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }

        preg_match_all('/\{getcenterpartbanner\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';
        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode(":", $v);
                $shortcode = str_replace('}', '', $explode[1]);
                //**********************************************************************************************************Генерация баннера
                $parts = explode(",", $shortcode);
                $this->context->smarty->assign('parts', $parts);
                $replacement = $this->context->smarty->fetch("catalog/_partials/miniatures/getcenterpartbanner.tpl");
                //**************************************************************************************************************************
                $content = str_replace($matches[0], $replacement, $content);
            }
            $maincontent['text'] = $content;
        }
        preg_match_all('/\{getanimation\}/i', $content, $matches);
        $replacement = '';
        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {

                //$this->context->smarty->assign('parts', $parts);
                $replacement = $this->context->smarty->fetch("catalog/_partials/miniatures/animation.tpl");

                //**************************************************************************************************************************
                $content = str_replace($matches[0], $replacement, $content);
            }
            $maincontent['text'] = $content;
        }


        preg_match_all('/\{getbestsales\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';
        if (!empty($matches[0])) {
            $products = $this->getProducts();
            $productsout = array();
            foreach ($matches[0] as $k => $v) {
                $explode = explode(":", $v);
                $shortcode_id = str_replace('}', '', $explode[1]);
                $shortcode_ids = explode(",", $shortcode_id);
                $title = '';
                if (!empty($explode[2])) {
                    $title = str_replace('}', '', $explode[2]);
                }
                //**********************************************************************************************************Генерация товара
                $module = new FrontController();
                $productsSale = ProductSale::getBestSales($this->context->language->id, $module->p - 1, $module->n, $module->orderBy, $module->orderWay);
                foreach ($productsSale as $k => $v) {
                    array_push($productsout, $products[$v['id_product']]);
                }
                //**************************************************************************************************************************
                $this->context->smarty->assign('products', $productsout);
                $this->context->smarty->assign('title', $title);
                $this->context->smarty->assign('shortcode', 'getbestsales');
                $replacement .= $this->context->smarty->fetch("catalog/_partials/miniatures/product_shortcode.tpl");
                $content = str_replace($matches[0], $replacement, $content);
            }
            $maincontent['text'] = $content;
        }



        preg_match_all('/\{getproductsrow\:[\s\S]+?\}/i', $content, $matches);


        if (!empty($matches[0])) {
            $products = $this->getProducts();
            $productsout = array();

            foreach ($matches[0] as $k => $v) {
                $replacement = '';
                $title='';
                $text='';
                $shortcode_id='';
                $explode = explode("getproductsrow:", $v);
                $shortcode_id = str_replace('}', '', $explode[1]);
                if (!empty($shortcode_id)){
                    $title=explode(';',$shortcode_id)[1];
                    $text=explode(';',$shortcode_id)[2];
                    $i=0;
                    foreach(explode(',',explode(';',$shortcode_id)[0]) as $item){
                        $productsout[$i++]=$products[$item];
                    }
                }
                //**************************************************************************************************************************
                $this->context->smarty->assign('text', $text);
                $this->context->smarty->assign('products', $productsout);
                $this->context->smarty->assign('title', $title);
                $this->context->smarty->assign('shortcode', 'getproductsrow');
                $replacement .= $this->context->smarty->fetch("catalog/_partials/miniatures/product_shortcode.tpl");
                $content = str_replace($v, $replacement, $content);
                $maincontent['text'] = $content;
            }

        }


        preg_match_all('/\{get2products\:[\s\S]+?\}/i', $content, $matches);


        if (!empty($matches[0])) {
            $products = $this->getProducts();
            $productsout = array();

            foreach ($matches[0] as $k => $v) {
                $replacement = '';
                $title='';
                $text='';
                $shortcode_id='';
                $explode = explode("get2products:", $v);
                $shortcode_id = str_replace('}', '', $explode[1]);
                if (!empty($shortcode_id)){
                    $title=explode(';',$shortcode_id)[0];
                    $i=0;
                    foreach(explode(',',explode(';',$shortcode_id)[1]) as $item){
                        $productsout[$i++]=$products[$item];
                    }
                }
                //**************************************************************************************************************************
                $this->context->smarty->assign('products', $productsout);
                $this->context->smarty->assign('title', $title);
                $this->context->smarty->assign('shortcode', 'get2products');
                $replacement .= $this->context->smarty->fetch("catalog/_partials/miniatures/2products.tpl");
                $content = str_replace($v, $replacement, $content);
                $maincontent['text'] = $content;
            }

        }


        preg_match_all('/\[gs\_var\_[(a-zA-Z0-9,-; ]+\]/i', $content, $matches);
        $replacement = '';
        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode("_", $v);
                $shortcode = str_replace(']', '', $explode[2]);
                //**********************************************************************************************************Генерация баннера
                //$this->context->smarty->assign('parts', $parts);
                $replacement = Hook::exec('displayObjSlider', array('var' => array(
                    '[gs_var_' . $shortcode . ']'
                ), 'mod' => 'galslidereverywhere'));
                //**************************************************************************************************************************
                $content = str_replace($v, $replacement, $content);
            }
            $maincontent['text'] = $content;
        }
        preg_match_all('/\{composite\_banner\_[(a-zA-Z0-9,-; ]+\}/i', $content, $matches);
        $replacement = '';
        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode("_", $v);
                $shortcode = str_replace('}', '', $explode[2]);
                //**********************************************************************************************************Генерация баннера
                //$this->context->smarty->assign('parts', $parts);
                $replacement = Hook::exec('displayObjBanner', array('var' => array($shortcode), 'mod' => 'composite_banner'));
                //**************************************************************************************************************************
                $content = str_replace($v, $replacement, $content);
            }
            $maincontent['text'] = $content;
        }

        preg_match_all('/\{getvideo\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';

        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode("getvideo:", $v);
                $shortcode = str_replace('}', '', $explode[1]);

                //**********************************************************************************************************Генерация баннера
                $parts = explode(";", $shortcode);
                $output['video']=$parts[0];
                $output['pic']=$parts[1];
                $output['size']=$parts[2];
                $this->context->smarty->assign('content', $output);
                $this->context->smarty->assign('img_path', $img_path);
                $replacement = htmlspecialchars_decode($this->context->smarty->fetch("catalog/_partials/miniatures/video.tpl"));
                //**************************************************************************************************************************
                $content = str_replace('{getvideo:' . $shortcode . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }

        preg_match_all('/\{getbigtopbanner\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';

        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode("getbigtopbanner:", $v);
                $shortcode = str_replace('}', '', $explode[1]);
                //**********************************************************************************************************Генерация баннера
                $parts = explode(";", $shortcode);
                $output['background']=$parts[0];
                $output['car']=$parts[1];
                $output['smallcar']=$parts[2];
                $output['title']=$parts[3];
                $output['text']=$parts[4];
                $this->context->smarty->assign('content', $output);
                $this->context->smarty->assign('img_path', $img_path);
                $replacement = htmlspecialchars_decode($this->context->smarty->fetch("catalog/_partials/miniatures/getbigtopbanner.tpl"));
                //**************************************************************************************************************************
                $content = str_replace('{getbigtopbanner:' . $shortcode . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }

        preg_match_all('/\{getfooterbanner\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';

        if (!empty($matches[0])) {
            $output=[];
            $categories = Category::getCategories((int)$this->context->language->id, true, false);
            foreach ($matches[0] as $k => $v) {
                $explode = explode("getfooterbanner:", $v);
                $shortcode = str_replace('}', '', $explode[1]);
                //**********************************************************************************************************Генерация баннера
                $parts = explode(",", $shortcode);
                foreach($parts as $key=>$id){
                    $output[$key]['id']=$id;
                    $list=scandir ( 'img/c');
                    $output[$key]['img']='';
                    foreach($list as $img){
                        if (stristr($img,$id.'-0_thumb')){
                            $output[$key]['img0']='../img/c/'.$img;
                        }
                        if (stristr($img,$id.'-1_thumb')){
                            $output[$key]['img1']='../img/c/'.$img;
                        }
                    }
                    foreach($categories as $category){
                        if ($category['id_category']==$id){
                            $output[$key]['name']=$category['name'];
                            $output[$key]['link_rewrite']=$category['link_rewrite'];
                        }
                    }
                }
                $this->context->smarty->assign('content', $output);
                $this->context->smarty->assign('img_path', $img_path);
                $replacement = htmlspecialchars_decode($this->context->smarty->fetch("catalog/_partials/miniatures/getfooterbanner.tpl"));
                //**************************************************************************************************************************
                $content = str_replace('{getfooterbanner:' . $shortcode . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }

        preg_match_all('/\{getscreensizebanner\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';

        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode("getscreensizebanner:", $v);
                $shortcode = str_replace('}', '', $explode[1]);
                //**********************************************************************************************************Генерация баннера
                $parts = explode(";", $shortcode);
                $output['img']=$parts[0];
                $output['url']=$parts[1];
                $output['title']=$parts[2];
                $output['text']=$parts[3];
                $this->context->smarty->assign('content', $output);
                $this->context->smarty->assign('img_path', $img_path);
                $replacement = htmlspecialchars_decode($this->context->smarty->fetch("catalog/_partials/miniatures/getscreensizebanner.tpl"));
                //**************************************************************************************************************************
                $content = str_replace('{getscreensizebanner:' . $shortcode . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }

        preg_match_all('/\{getpicslider\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';
        $output=[];
        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $explode = explode("getpicslider:", $v);
                $shortcode = str_replace('}', '', $explode[1]);
                //**********************************************************************************************************Генерация баннера
                $parts = explode(";", $shortcode);
                foreach($parts as $key=>$part){
                    $output[$key]=$part;
                }
                $this->context->smarty->assign('content', $output);
                $this->context->smarty->assign('img_path', $img_path);
                $replacement = htmlspecialchars_decode($this->context->smarty->fetch("catalog/_partials/miniatures/getpicslider.tpl"));
                //**************************************************************************************************************************
                $content = str_replace('{getpicslider:' . $shortcode . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }



        preg_match_all('/\{get3div\:[\s\S]+?\}/i', $content, $matches);
        $replacement = '';

        if (!empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $output=[];
                $text='';
                $title='';
                $explode = explode("get3div:", $v);
                $shortcode = str_replace('}', '', $explode[1]);
                //**********************************************************************************************************Генерация баннера
                $parts = explode("|", $shortcode);
                foreach($parts as $key=>$part) {

                    if ($key==0){
                        $title = explode(";", $part)[0];
                        $text = explode(";", $part)[1];
                        $back = explode(";", $part)[2];
                    }else{
                        if (explode(";", $part)[0]=='pic'){
                            $output[$key]['type']='pic';
                            $output[$key]['img']=explode(";", $part)[1];
                        }else{
                            $output[$key]['type']='vid';
                            $output[$key]['img']=explode(";", $part)[1];
                            $output[$key]['vid']=explode(";", $part)[2];
                        }
                    }

                }
                $this->context->smarty->assign('background', $back);
                $this->context->smarty->assign('text', $text);
                $this->context->smarty->assign('title', $title);
                $this->context->smarty->assign('content', $output);
                $this->context->smarty->assign('img_path', $img_path);
                $replacement = htmlspecialchars_decode($this->context->smarty->fetch("catalog/_partials/miniatures/get3div.tpl"));
                //**************************************************************************************************************************
                $content = str_replace('{get3div:' . $shortcode . "}", $replacement, $content);
            }
            $maincontent['text'] = $content;
        }

                $this->cms->content=$maincontent['text'];
                $this->context->smarty->assign(array(
                    'cms' => $this->objectPresenter->present($this->cms),
                ));
                $this->setTemplate('cms/page', array(
                    'entity' => 'cms',
                    'id' => $this->cms->id
                ));
            }



    public function displayCat($category_id)
    {
        $id_lang = $this->context->language->id;
        if (!empty(stripos($category_id, ','))) {
            $ids = explode(",", $category_id);
            for ($i = 0; $i < count($ids); $i++) {
                $query = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'category_lang` WHERE id_category=' . (int)$ids[$i] . ' and `id_lang`=\'' . pSQL($id_lang) . '\'');
                $result[$i] = $query[0];
                //$result[$i][0]['description'] = strip_tags($result[$i]['description']);
            }

        } else {
            $query = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'category_lang` WHERE id_category=' . (int)$category_id . ' and `id_lang`=\'' . pSQL($id_lang) . '\'');
            $result[0] = $query[0];
        }
        return $result;
    }

    protected function getProducts()
    {

        $customer = $this->context->customer->id;

        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'product` WHERE  id_product = id_product';

        $products = Db::getInstance()->executeS($sql);

        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );

        $products_for_template = [];

        foreach ($products as $rawProduct) {
            $products_for_template[] = $presenter->present(
                $presentationSettings,
                $assembler->assembleProduct($rawProduct),
                $this->context->language
            );
        }

        return $products_for_template;
    }

    public static function getSubCategories($id_category, $id_lang = null)
    {
        $context = Context::getContext();
        $id_lang = is_null($id_lang) ? $context->language->id : $id_lang;

        $sql_groups_where = '';
        $sql_groups_join = '';
        if (Group::isFeatureActive()) {
            $sql_groups_join = 'LEFT JOIN `' . _DB_PREFIX_ . 'category_group` cg ON (cg.`id_category` = c.`id_category`)';
            $groups = FrontController::getCurrentCustomerGroups();
            $sql_groups_where = 'AND cg.`id_group` ' . (count($groups) ? 'IN (' . implode(',', $groups) . ')' : '=' . (int)Group::getCurrent()->id);
        }

        $query = 'SELECT c.*, cl.id_lang, cl.name, cl.description, cl.link_rewrite, cl.meta_title, cl.meta_keywords, cl.meta_description
            FROM `' . _DB_PREFIX_ . 'category` c
            ' . Shop::addSqlAssociation('category', 'c') . '
            LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (c.`id_category` = cl.`id_category` AND `id_lang` = ' . (int)$id_lang . ' ' . Shop::addSqlRestrictionOnLang('cl') . ')
            ' . $sql_groups_join . '
            WHERE `id_parent` = ' . (int)$id_category . '
            AND c.`active` = 1
            ' . $sql_groups_where . '
            GROUP BY c.`id_category`
            ORDER BY `level_depth` ASC, category_shop.`position` ASC';

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

        if (!$result) {
            return false;
        }

        foreach ($result as &$row) {
            $row['url'] = $context->link->getCategoryLink(
                $row['id_category'],
                $row['link_rewrite']
            );
        }

        return $result;*/
    }
}
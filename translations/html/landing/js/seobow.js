$('.seabow__video-image').click(function (e) { 
  $('.seabow__popap-video').css('display', 'block');  
});


$(document).mouseup(function (e) { 
  var popup = $('.seabow__popap-video');
 if (e.target!=popup[0]&&popup.has(e.target).length === 0){
    if (typeof(player.stopVideo) !== "undefined"){
        player.stopVideo();
        $(".seabow__popap-video").css('display', 'none');
     }
 }
});

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player_seabow', {
        height: '780',
        width: '1200',
        videoId: 'PA2aCY0o7dY',
        playerVars: {
            'autoplay': 1,
            'controls': 0,
            'showinfo': 1,
            'loop': 1
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

function onPlayerReady(event) {
    event.target.playVideo();
    

}

var done = false;

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
        setTimeout(stopVideo, 6000);
        done = true;
    }
    if (event.data === YT.PlayerState.ENDED) {
        player.playVideo();
    }
}

var mySwiper = new Swiper ('#trident-one-swiper', {
  pagination: {
    el: '#trident-one-swiper .swiper-pagination',
    clickable :true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  autoplay: {
    dalay: 3000,
  },
  on: {
    init: function(){		     
      window.setTimeout(function(){
        $("#trident-one-swiper .swiper-pagination-top span").each(function(index){
      $(this).html('<img src="/modules/menu_and_product_landings/views/img/trident-pc-seven-t'+(index+1)+'.png"><i></i>');
    });
      },3000)         
    }
  }
})

var posterTvGrid = new posterTvGrid('posterTvGrid',{className: "posterTvGrid"},[
  {"img":"images\/trident-pc-eight-t1.png","title":"Lorem ipsum dolor sit amet"},
  {"img":"images\/trident-pc-eight-t2.png","title":"Lorem ipsum dolor sit amet"},
  {"img":"images\/trident-pc-eight-t3.png","title":"Lorem ipsum dolor sit amet"},
  {"img":"images\/trident-pc-eight-t4.png","title":"Lorem ipsum dolor sit amet"},

]
);

"use strict";
/*
* Plugin: an-progress-bar
* Version: 1.0.1
* Description: A plugin that fills bars with a percentage you set.
* Author: Hasan Misbah
* Copyright 2018, Hasan Misbah
* Free to use and abuse under the MIT license.
* http://www.opensource.org/licenses/mit-license.php
*/
!function(i) {
    i.fn.skillbar = function(t) {
        var e = i.extend({
            speed: 1e3,
            bg: ""
        }, t)
          , n = e.bg
          , d = i(this).find(".filled")
          , s = i(this).find(".title");
        return n ? (d.css({
            "background-color": n
        }),
        s.css({
            "background-color": "rgba(0,0,0,0.5)"
        })) : this.each(function(t) {
            i(this).find(".filled").animate({
                width: i(this).find(".filled").data("width")
            }, e.speed)
        }),
        this
    }
}(jQuery);
//# sourceMappingURL=an-skill-bar.js.map

$(window).scroll(function() {
  var gun = $(document).scrollTop();
      var top1 = $('.trident-pc-nine').offset().top;       
      var height1 = top1-gun;
      if (height1 < 0) {
        $(document).ready(function () {
      $('.skillbar').skillbar({
          speed: 2000,
      });
    });
      }
    }); 

    $(function () {
      init();
  })

  function init() {
      this.dragging = false;
      this.$dragBtn = $('.slider-btn');
      this.$dragImg = $('.slider-item2');
      this.$leftBtn = $('.slider-btn .slider-btn-arrow.left');
      this.$rightBtn = $('.slider-btn .slider-btn-arrow.right');
      this.maxOffsetX = $('.slider').width();

      this.isClick = false;
      this.initPos = 0;

      this.sliderListener();

      const timer1 = setTimeout(() => {
          this.$dragImg.css({
              width: '48%',
              transition: 'width 1500ms cubic-bezier(0.39, 0.575, 0.565, 1)'
          });
          this.$dragBtn.css({
              left: '48%',
              transition: 'left 1500ms cubic-bezier(0.39, 0.575, 0.565, 1)'
          });
          clearTimeout(timer1);
      }, 1000);

      const timer2 = setTimeout(() => {
          this.$dragImg.css({
              transition: 'width 50ms cubic-bezier(0.39, 0.575, 0.565, 1)'
          });
          this.$dragBtn.css({
              transition: 'left 50ms cubic-bezier(0.39, 0.575, 0.565, 1)'
          });
          clearTimeout(timer2);
      }, 2000);
  }

  function sliderListener() {
      $(document).on('mousedown touchstart', '.slider-btn', (e) => {
          this.startDrag(e);
      });

      $(document).on('mousemove touchmove', '.slider', (e) => {
          this.isClick = false;
          this.drag(e);
          // return false;
      });

      $(document).on('click', '.slider', (e) => {
          this.isClick = true;
          this.startDrag();
          this.drag(e);
          this.stopDrag();
      });

      $(document).on('mouseup touchend', () => {
          this.stopDrag();
          // return false;
      });
  }

  function drag(e) {
      const that = this;
      if (!that.dragging) {
          return;
      }

      let offsetX = 0;

      if (e.originalEvent && e.originalEvent.touches) {
          offsetX = e.originalEvent.touches[0].pageX;
      } else {
          offsetX = e.offsetX;
      }

      if ('ontouchstart' in document) {
          if (window.browserScreen === 'mobile') {
              offsetX -= 25;
          } else if (window.browserScreen === 'padv') {
              offsetX -= 60;
          } else if (window.browserScreen === 'padh' || window.browserScreen === 'padProv') {
              offsetX -= 125;
          } else if (window.browserScreen === 'padProh') {
              offsetX -= 185;
          }
      }

      if (offsetX < 0) {
          offsetX = 0;
      } else if (offsetX > that.maxOffsetX) {
          offsetX = that.maxOffsetX;
      }

      if (offsetX < this.initPos) {
          that.$leftBtn.removeClass('active');
          that.$rightBtn.addClass('active');
      } else {
          that.$rightBtn.removeClass('active');
          that.$leftBtn.addClass('active');
      }

      this.initPos = offsetX;

      if (that.isClick) {
          that.$dragImg.css('transition', 'width 0.5s cubic-bezier(0.39, 0.575, 0.565, 1)');
          that.$dragBtn.css('transition', 'left 0.5s cubic-bezier(0.39, 0.575, 0.565, 1)');
      } else {
          that.$dragImg.css('transition', '');
          that.$dragBtn.css('transition', '');
      }

      requestAnimationFrame(() => {
          let percent = offsetX / $('.slider').width();

          if (percent > 0.9) {
              percent = 0.9;
          }
          if (percent < 0.1) {
              percent = 0.1;
          }
          that.$dragImg.css('width', `${percent * 100}%`);
          that.$dragBtn.css('left', `${percent * 100}%`);
      });
  }

  function startDrag() {
      $('.slider').addClass('dragging');
      this.dragging = true;
  }

  function stopDrag() {
      if (!this.dragging) {
          return;
      }
      this.dragging = false;

      $('.slider').removeClass('dragging');
      $('.slider-btn .slider-btn-arrow').removeClass('active');
  }